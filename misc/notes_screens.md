## obrazovky

#### login

- přihlášení k existujícímu účtu (firebase)
- po přihlášení naviguje na seznam skupin
- umožňuje navigovat na registraci

#### registrace

- vytvoření nového účtu
- po úspěšné registraci naviguje na seznam skupin (časem by mohlo být na intro, ale to je out of scope MVP)

#### seznam skupin (main)

- "hlavní" obrazovka aplikace
- zobrazuje seznam skupin s
    - jejich jménem,
    - aktuálním dlužným stavem uživatele v dané skupině (v promární měně skupiny).
- zobrazuje uživateli součty
    - "dluhů" vůči skupinám, kde je v mínusu
    - "collectible" částek ze skupin, kde je v plusu
    - obě tyto částky jsou počítány vůči skupinám jakožto celkům
    - v hlavní měně uživatele (lze změnit v nastavení účtu)
    - rozdíl těchto částek
- naviguje na
    - detail skupiny (proklik přes kartu skupiny)
    - logout (v menu; dočasně levý horní roh)
    - nastavení (v menu)
    - přidání skupiny (floating action button)
        - zobrazí dialog, zda přidat existující skupinu (zadá její ID), nebo
        - založit novou -> přesměruje na zakládání skupiny

#### detail skupiny

- zobrazuje
    - aktuální dlužné stavy účastníků vůči skupině
        - v měně skupiny
        - bere se stav vůči skupině jako celku (tzn když A dlužilo B 500 Kč a zaplatilo za C 200 Kč, "dluží" A skupině 300Kč)
        - seřazené od nejvíce dlužících
    - historii transakcí od nejnovější, s
        - jménem (je-li vyplněno, jinak jen "purchase" / "transfer")
        - částkou v měně, v jaké se uskutečnila
        - seznam účastníků, jichž se týká
            - ideálně 2 seznamy - platící a dlužící
            - každopádně ale zvýrazňuje transakce, kterých se účastnil uživatelův účastník
        - zvýrazňuje transakce s komentářem uživatele
            - potenciálně i s komentáři jiných - ale méně výrazně
- naviguje na
    - přidání transakce (floating action button)
    - detail transakce (proklik na jejich seznamu)
    - vyhledávání v rámci skupiny (floating button / menu)
    - editaci skupiny (menu / button vpravo nahoře)
    - možnost zkopírovat ID skupiny (menu)
    - možnost opustit skupinu (menu)
    - zpět na seznam skupin (button vlevo nahoře)

#### editace / vytvoření skupiny

- pro editaci se k přepisu zobrazují již vyplněné hodnoty, pro vytváření jsou prázdné/defaultní
- pokud nebude u skupiny menu, pak u existující:
    - možnost vykopírovat ID
    - možnost opustit skupinu
- možno nastavit
    - jméno skupiny
    - primární měnu
    - přidat účastníka
    - optional
        - vypnout/zapnout možnost připojení přes ID skupiny
        - editovat nick účastníka
        - udělování (a odebírání?) admin práv
        - sloučení účastníků
        - smazání účastníka
            - který je bez transakcí, nebo
            - který má nulový stav (vyžaduje defaultního ghost účastníka)
            - libovolného (taktéž vyžaduje defaultního ghost účastníka)
        - odebrat přístup uživateli
- mnoho těchto nastavení (mimo editace vlastního nicku?) by bylo dobré omezit na adminy skupiny
- umožňuje změny propsat/zahodit; pak naviguje zpět na detail skupiny

#### detail transakce

- zobrazuje všechny vyplněné parametry transakce
- umožňuje smazání transakce (ideálně v okně třeba 5 minut po vytvoření, a pak jen adminem)
- naviguje
    - na přidání/úpravu/smazání vlastního komentáře
    - na editaci dané transakce
    - zpět na detail skupiny

#### editace / vytvoření transakce

- umožňuje vyplnit/změnit atributy (v rámci povolených úprav - data musí zůstat konzistentní)
    - nejspíš mimo komentářů - budou zvlášť
- umožňuje změny propsat/zahodit; pak naviguje zpět na detail transakce

#### editace / vytvoření / smazání komentáře u transakce

- umožňuje pouze nad vlastními komentáři uživatele
- pro MVP stačí smazání komentáře při editaci textu na prázdný string

#### nastavení

- potenciálně rozdělitelné na nastavení app a účtu, pokud pro app implementujeme více nastavení
- nastavení účtu
    - změna defaultní měny
    - změna hesla
    - změna mailu










