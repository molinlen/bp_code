# Seznam úkolů souvisejících s kódem

Pracovní soubor pro seznam(y) úkolů souvisejících s kódem.

Aktuálně projekt čeká na posun do další fáze (freeze pro možnost odevzdání a ohodnocení BP).


 
#### nice to have

Pokud vybyde čas, bylo by fajn zfouknout aspoň část.

- revize todos v kódu
- překlad do češtiny - bylo by fajn stihnout
- overview screen menu: join vs create group: review icons
- více druhů splittingu
- smysluplnou část textů umožnit selectovat
- zavírátko vlevo na app baru?
- overview error/loading - rozpadnout po sekcích
- purchase detail/edit: promyšlenější ordering participants... možná podíl + nickname?
- language-considering formátování částek
- language-considering formátování měn
- edit participanta
- edit času transakce
- přepsat UseCase metody na `invoke`
- search v rámci skupiny
- návrh vyrovnání dluhů
- fotky účtenek
- settings
- global search
- pokročilé splitování nákupů
  - více možností dělení
  - otestovat ukládání a načítání z firestore
- najít vhodné řešení pro ukládání + updatování tabulky převodů měn
- password change
- password recovery
- e-mail change
- registrace:
  - detekovat malformed e-mail
  - detekovat opakovaně použitý e-mail
- možnost použití bez přístupu k internetu
  - alespoň read
  - ideálně i write
- možnost práce bez přihlášení (https://firebase.google.com/docs/auth/android/anonymous-auth)
- možnost přihlášení přes Google
- catchy jméno pro appku
- lepší distribuce



###### splněné nice to have

Naštěstí již za námi:

- [X] keyboard actions/options
- [X] detail uživatele (ač zatím bez pokročilé editace)
- [X] loading overlay u zpracovávání požadavku (join group, add participant, save changes & navigate elsewhere)
- [X] zavírat menu tlačítkem zpět https://betterprogramming.pub/back-press-handling-in-android-jetpack-compose-42d9ed402d40
- [X] purchase edit: neměnit pořadí sharers
- [X] purchase edit: (un)select all sharers button
- [X] password show option
- [X] pozvrzovací dialog před destruktivní akcí (opuštění skupiny, mazání transakce)
- [X] base screen layout vyměnit za něco standarizovanějšího (např. Scaffold)
- [X] reagovat v text inputu na tap mimo prvek: schovat klávesnici na tap mimo text-field
- [X] evidence "jen vlastních nákupů" ve skupině o 1 člověku

