package cz.cvut.fit.molinlen.android.debttracker.data.model

import java.math.BigDecimal
import java.time.OffsetDateTime

sealed interface Transaction {
    val transactionId: String
    val fullAmount: Amount
    val currency: CurrencyCode
    val time: OffsetDateTime
    val fromWhom: List<Participant>
    val toWho: List<Participant>
    val comments: List<Comment>
    val note: String?

    fun hasCommentByUser(userId: String?) = comments.any { it.writerUserId == userId }
}

sealed interface Purchase : Transaction {
    val name: String?
    val payers: List<Payer>
    val splitting: TransactionSplittingStrategy

    override val fromWhom: List<Participant>
        get() = payers.map { it.participant }
    override val toWho: List<Participant>
        get() = splitting.getParticipants().toSet().toList()
    override val currency: CurrencyCode
        get() = payers.first().amount.currency
    override val fullAmount: Amount
        get() = Amount(
            value = payers.foldRight(BigDecimal.ZERO) { payer, x -> x.plus(payer.amount.value) },
            currency = currency
        )
}

data class PurchaseDetail(
    override val transactionId: String,
    override val name: String?,
    override val payers: List<Payer>,
    override val splitting: TransactionSplittingStrategy,
    override val comments: List<Comment>,
    override val time: OffsetDateTime,
    override val note: String?,
) : Purchase

sealed interface Transfer : Transaction {
    val from: Participant
    val to: Participant
    val amount: Amount

    // In the future, we might want other that 1:1 payments. For now though, they suffice.
    override val fromWhom: List<Participant>
        get() = listOf(from)
    override val toWho: List<Participant>
        get() = listOf(to)
    override val fullAmount: Amount
        get() = amount
    override val currency: CurrencyCode
        get() = amount.currency
}

data class TransferDetail(
    override val transactionId: String,
    override val from: Participant,
    override val to: Participant,
    override val amount: Amount,
    override val time: OffsetDateTime,
    override val comments: List<Comment>,
    override val note: String?,
) : Transfer

data class Payer(
    val participant: Participant,
    val amount: Amount,
)

interface Comment {
    val writerUserId: String
    //todo displayable indentifier... email?
    val content: String
    val time: OffsetDateTime
    val edited: Boolean
}

data class CommentDetail(
    override val writerUserId: String,
    override val content: String,
    override val time: OffsetDateTime,
    override val edited: Boolean,
) : Comment

