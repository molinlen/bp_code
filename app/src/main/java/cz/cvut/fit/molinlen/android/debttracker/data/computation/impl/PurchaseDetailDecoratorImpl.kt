package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import arrow.core.Either
import arrow.core.flatMap
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitter
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitterImpl
import cz.cvut.fit.molinlen.android.debttracker.data.computation.PurchaseDetailDecorator
import cz.cvut.fit.molinlen.android.debttracker.data.model.Amount
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransactionSplittingPortion

class PurchaseDetailDecoratorImpl(
    private val amountSplitter: AmountSplitter = AmountSplitterImpl,
) : PurchaseDetailDecorator {

    override fun decoratePortionSplitting(
        splittingPortion: TransactionSplittingPortion,
        totalAmount: Amount,
    ): ProblemOr<TransactionSplittingPortion> {
        val splittingResult = amountSplitter.splitAmountToPortions(
            amount = totalAmount,
            portions = splittingPortion.sharers.map { it.shareValue },
        )

        val result = splittingResult.flatMap { portionsData ->
            Either.catch {
                splittingPortion.copy(
                    sharers = splittingPortion.sharers.map { share ->
                        share.copy(
                            shareValueComputed = portionsData.portionResults[share.shareValue]?.value!!,
                        )
                    },
                )
            }.mapLeft { Problem("Failed to map SplittingPortion with portionsData", null, it) }
        }

        return result
    }
}
