package cz.cvut.fit.molinlen.android.debttracker.ui.components

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import java.time.LocalDate
import java.time.OffsetDateTime
import java.util.*

fun Modifier.pickedText(): Modifier {
    return this.padding(top = 5.dp, bottom = 5.dp, start = 15.dp)
}

fun Modifier.pickerIcon(): Modifier {
    return this.padding(top = 8.dp, bottom = 8.dp, end = 8.dp)
}

@Composable
fun DropDownPicker(
    menuExpanded: MutableState<Boolean>,
    pickedText: String,
    contentDescription: String,
    modifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    textWeight: FontWeight = FontWeight.Normal,
    isError: Boolean = false,
    supportingText: String? = null,
    onlyShowSupportingTextForError: Boolean = true,
    errorStrong: Boolean = false,
    menuContent: @Composable () -> Unit,
) {
    PickedItemRow(
        modifier = modifier,
        horizontalArrangement = Arrangement.SpaceBetween,
        isError = isError,
        errorStrong = errorStrong,
        supportingText = supportingText,
        onlyShowSupportingTextForError = onlyShowSupportingTextForError,
        onClick = {
            menuExpanded.value = menuExpanded.value.not()
        },
    ) {
        Text(
            text = pickedText,
            fontWeight = textWeight,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = textModifier
                .pickedText()
                .weight(1f),
        )
        Box {
            Icon(
                imageVector = Icons.Default.ArrowDropDown,
                contentDescription = contentDescription,
                modifier = Modifier.pickerIcon(),
            )
            DropdownMenu(
                expanded = menuExpanded.value,
                onDismissRequest = { menuExpanded.value = false },
            ) {
                menuContent()
            }
        }
    }
}

@Composable
private fun PickedItemRow(
    modifier: Modifier = Modifier,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.SpaceBetween,
    isError: Boolean = false,
    errorStrong: Boolean = false,
    supportingText: String? = null,
    onlyShowSupportingTextForError: Boolean = true,
    onClick: () -> Unit,
    content: @Composable (RowScope.() -> Unit),
) {
    Column(modifier = modifier) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = horizontalArrangement,
            modifier = Modifier
                .padding(vertical = 6.dp)
                .border(
                    width = 2.dp,
                    color = when {
                        isError && errorStrong -> MaterialTheme.colors.onError
                        isError -> MaterialTheme.colors.error
                        else -> AccentColors.surfaceAccentMedium
                    })
                .alsoIf(isError && errorStrong) { it.background(MaterialTheme.colors.error) } //todo how much should error be accented?
                .fillMaxWidth()
                .clickable(onClick = onClick)
                .defaultMinSize(minHeight = 56.dp)
        ) {
            content()
        }
        supportingText?.let {
            TextFieldSupportingText(
                isError = isError,
                onlyShowSupportingTextForError = onlyShowSupportingTextForError,
                supportingText = it,
                modifier = Modifier.tweakForGeneralSupportingText(),
            )
        }
    }
}

@Composable
fun DatePicker(
    datePickerExpanded: MutableState<Boolean>,
    date: LocalDate,
    supportingText: String? = null,
    onlyShowSupportingTextForError: Boolean = true,
) {
    PickedItemRow(
        supportingText = supportingText,
        onlyShowSupportingTextForError = onlyShowSupportingTextForError,
        onClick = {
            datePickerExpanded.value = true
        },
        content = {
            Text(
                text = date.toPrettyString(),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .pickedText()
                    .weight(1f),
            )
            Icon(
                imageVector = Icons.Default.DateRange,
                contentDescription = stringResource(R.string.pick_date),
                modifier = Modifier.pickerIcon(),
            )
        }
    )
}

@Composable
fun DatePicker(
    datePickerExpanded: MutableState<Boolean>,
    time: OffsetDateTime,
    supportingText: String? = null,
    onlyShowSupportingTextForError: Boolean = true,
) = DatePicker(
    datePickerExpanded = datePickerExpanded,
    date = time.toLocalDate(),
    supportingText = supportingText,
    onlyShowSupportingTextForError = onlyShowSupportingTextForError,
)

@Composable
fun datePickerDialog(
    datePickerExpanded: MutableState<Boolean>,
    onUpdateDate: (year: Int, month: Int, dayOfMonth: Int) -> Unit,
): DatePickerDialog {
    val calendar = Calendar.getInstance()
    val dialog = DatePickerDialog(
        LocalContext.current,
        if (MaterialTheme.colors.isLight) R.style.light_alert_dialog else R.style.dark_alert_dialog,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            // DatePickerDialog has month in 0-11 range for Calendar compatibility, but DebtTracker primarily uses OffsetDateTime/LocalDate, which use 1-12 range
            onUpdateDate(year, month + 1, dayOfMonth)
            datePickerExpanded.value = false
        },
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH),
    )
    dialog.setOnCancelListener { datePickerExpanded.value = false }
    dialog.setOnDismissListener { datePickerExpanded.value = false }
    return dialog
}

@Composable
fun DatePickerDialogShow(
    time: OffsetDateTime,
    dialog: DatePickerDialog,
) {
    val calendar = Calendar.getInstance()
    // conversion ensures that month is passed as 0-11 to DatePickerDialog, even though OffsetDateTime/LocalDate have it as 1-12
    calendar.time = Date.from(time.toInstant())
    dialog.updateDate(
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH),
    )
    dialog.show()
}

@Composable
fun BasicLeftCheckboxWithText(
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    text: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    checkboxModifier: Modifier = Modifier,
    needsToBeChecked: Boolean = false,
) {
    Row(modifier = modifier) {
        BasicCheckbox(
            checked = checked,
            modifier = checkboxModifier,
            colors = if (needsToBeChecked) mandatoryCheckboxColors() else basicCheckboxColors(),
            onCheckedChange = onCheckedChange,
        )
        text()
    }
}

@Composable
fun BasicCheckbox(
    checked: Boolean,
    modifier: Modifier = Modifier,
    colors: CheckboxColors = basicCheckboxColors(),
    onCheckedChange: (Boolean) -> Unit,
) {
    Checkbox(
        checked = checked,
        onCheckedChange = onCheckedChange,
        modifier = modifier,
        colors = colors,
    )
}

@Composable
fun BasicTriStateCheckbox(
    state: ToggleableState,
    modifier: Modifier = Modifier,
    colors: CheckboxColors = basicCheckboxColors(),
    onClick: () -> Unit,
) {
    TriStateCheckbox(
        state = state,
        onClick = onClick,
        modifier = modifier,
        colors = colors,
    )
}

@Composable
fun basicCheckboxColors() = CheckboxDefaults.colors(
    checkedColor = MaterialTheme.colors.primaryVariant,
)

@Composable
fun alternateCheckboxColors() = CheckboxDefaults.colors(
    checkedColor = MaterialTheme.colors.secondaryVariant,
)

@Composable
fun mandatoryCheckboxColors() = CheckboxDefaults.colors(
    checkedColor = MaterialTheme.colors.primaryVariant,
    uncheckedColor = AccentColors.redContent,
)
