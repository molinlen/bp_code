package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.PurchaseRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.PurchaseRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface DeletePurchaseUseCase {
    suspend fun deletePurchase(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<Unit>>
}

class DeletePurchaseUseCaseImpl(
    private val purchaseRepository: PurchaseRepository = if (USE_FAKE_DATA) PurchaseRepositoryFake() else PurchaseRepositoryImpl(),
) : DeletePurchaseUseCase {
    override suspend fun deletePurchase(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<Unit>> =
        purchaseRepository.deletePurchase(userId = userId, groupId = groupId, purchaseId = purchaseId)
}
