package cz.cvut.fit.molinlen.android.debttracker.navigation

object DebtTrackerNavArguments {
    const val GROUP_ID_KEY = "group_id"
    const val PURCHASE_ID_KEY = "purchase_id"
    const val TRANSFER_ID_KEY = "transfer_id"
    const val EMAIL_KEY = "email"
    const val PASSWORD_KEY = "password"
}
