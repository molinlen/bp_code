package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.TransferRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.TransferRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface GetTransferDetailUseCase {
    suspend fun getTransferDetail(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<TransferDetail>>
}

class GetTransferDetailUseCaseImpl(
    private val transferRepository: TransferRepository = if (USE_FAKE_DATA) TransferRepositoryFake() else TransferRepositoryImpl(),
) : GetTransferDetailUseCase {
    override suspend fun getTransferDetail(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<TransferDetail>> {
        return transferRepository.getTransferDetail(userId = userId, groupId = groupId, transferId = transferId)
    }
}
