package cz.cvut.fit.molinlen.android.debttracker.data.model

interface User {
    val userId: String
    val primaryCurrency: CurrencyCode?
//    val groups: List<UsersGroup>
    // ... ?
}

data class UserBasic(
    override val userId: String,
    override val primaryCurrency: CurrencyCode?,
) : User

data class UserUpdateData(
    val primaryCurrency: CurrencyCode,
)

data class UserUpdateInputData(
    val primaryCurrency: CurrencyCode?,
)
