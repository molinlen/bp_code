package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import android.util.Log
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.computation.PurchaseDetailDecorator
import cz.cvut.fit.molinlen.android.debttracker.data.computation.impl.PurchaseDetailDecoratorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.PurchaseDetail
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransactionSplittingExplicit
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransactionSplittingPortion
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.PurchaseRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.PurchaseRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface GetPurchaseDetailUseCase {
    suspend fun getPurchaseDetail(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<PurchaseDetail>>
}

class GetPurchaseDetailUseCaseImpl(
    private val purchaseRepository: PurchaseRepository = if (USE_FAKE_DATA) PurchaseRepositoryFake() else PurchaseRepositoryImpl(),
    private val purchaseDetailDecorator: PurchaseDetailDecorator = PurchaseDetailDecoratorImpl(),
) : GetPurchaseDetailUseCase {
    override suspend fun getPurchaseDetail(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<PurchaseDetail>> = flow {
        purchaseRepository.getPurchaseDetail(userId = userId, groupId = groupId, purchaseId = purchaseId)
            .collect { fetched ->
                emit(fetched)
                fetched.map { fetchedPurchase ->
                    when (fetchedPurchase.splitting) {
                        is TransactionSplittingExplicit -> {} // no decoration needed
                        is TransactionSplittingPortion -> {
                            purchaseDetailDecorator.decoratePortionSplitting(
                                splittingPortion = fetchedPurchase.splitting,
                                totalAmount = fetchedPurchase.fullAmount,
                            ).fold(
                                { problem ->
                                    Log.e(APP_TAG,
                                        "$ERROR_PREFIX failed to decorate portion splitting [$groupId] [${fetchedPurchase.transactionId}] [${fetchedPurchase.splitting}] - [$problem]")
                                }, { decorated ->
                                    emit(fetchedPurchase.copy(splitting = decorated).right())
                                }
                            )
                        }
                    }
                }
            }
    }
}
