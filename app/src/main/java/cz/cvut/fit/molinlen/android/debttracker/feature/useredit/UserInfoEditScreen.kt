package cz.cvut.fit.molinlen.android.debttracker.feature.useredit

import androidx.compose.foundation.layout.*
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.EditMode
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun UserInfoEditScreen(
    navigateOnExit: () -> Unit,
) {
    val viewModel: UserInfoEditViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData()
    }

    UserInfoEditScreenImpl(
        canSave = viewModel.canSave(),
        onUpdateCurrency = viewModel::updateCurrency,
        onSave = { viewModel.save(navigateOnExit) },
        onDiscard = navigateOnExit,
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@Composable
fun UserInfoEditScreenImpl(
    canSave: Boolean,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    onClose: () -> Unit = onDiscard,
    viewState: UserInfoEditViewState,
) {
    val currencyPickerExpanded = remember { mutableStateOf(false) }
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = stringResource(R.string.edit_account),
                leftIcon = { HeaderCloseIcon(onClick = onClose) },
                backgroundColor = ScreenHeaderColors.editScreenColor,
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> UserInfoEditScreenContent(
                currencyPickerExpanded = currencyPickerExpanded,
                viewState = viewState,
                onUpdateCurrency = onUpdateCurrency,
                onSave = onSave,
                onDiscard = onDiscard,
                canSave = canSave,
                paddingValues = paddingValues,
            )
        }
    }
}

@Composable
private fun UserInfoEditScreenContent(
    currencyPickerExpanded: MutableState<Boolean>,
    viewState: UserInfoEditViewState,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    canSave: Boolean,
    paddingValues: PaddingValues,
) {
    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.screenContentModifier(paddingValues),
    ) {
        Spacer(modifier = Modifier) // an extremely lazy way to get the following components to center and bottom respectively
        CurrencyPicker(
            currencyPickerExpanded = currencyPickerExpanded,
            pickedCurrency = viewState.userData?.primaryCurrency,
            onUpdateCurrency = onUpdateCurrency,
        )
        viewState.saveError?.let { problem ->
            ErrorCard(
                text = buildErrorMessage(
                    errorMessage = stringResource(R.string.transaction_save_error_message),
                    errorDetails = problem.description,
                    errorCause = problem.cause,
                ),
            )
        }
        SaveAndDiscardButtons(
            mode = EditMode.EDIT,
            onSave = onSave,
            onDiscard = onDiscard,
            saveEnabled = canSave,
        )
    }
}

@Composable
private fun CurrencyPicker(
    currencyPickerExpanded: MutableState<Boolean>,
    pickedCurrency: CurrencyCode?,
    onUpdateCurrency: (CurrencyCode) -> Unit,
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = stringResource(R.string.primary_currency_prefix),
            modifier = Modifier
                .padding(end = 20.dp)
                .tweakForLabeledPickerRowLeadingText(),
        )
        DropDownPicker(
            menuExpanded = currencyPickerExpanded,
            pickedText = pickedCurrency?.name ?: "...",
            contentDescription = stringResource(R.string.pick_currency),
            supportingText = stringResource(R.string.field_is_mandatory),
            isError = pickedCurrency == null,
        ) {
            CurrencyCode.values().map {
                DropdownMenuItem(
                    onClick = {
                        onUpdateCurrency(it)
                        currencyPickerExpanded.value = false
                    }
                ) {
                    Text(it.name)
                }
            }
        }
    }
}
