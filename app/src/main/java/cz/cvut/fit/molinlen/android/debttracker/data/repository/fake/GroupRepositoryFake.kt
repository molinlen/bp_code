package cz.cvut.fit.molinlen.android.debttracker.data.repository.fake

import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.amy
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.catherine
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.danny
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.john
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.schubert
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.tom
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.user
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.random.Random

fun randomId() = java.util.UUID.randomUUID().toString()

class GroupRepositoryFake : GroupRepository {
    override suspend fun createNewGroup(userId: String, groupData: GroupInfoBasic): Flow<ProblemOr<String>> {
        return flowOf(if (Random.nextBoolean()) randomId().right() else Problem("Testing problem ;)").left())
    }

    override suspend fun updateGroupInfo(groupInfo: GroupInfoBasic): Flow<ProblemOr<Unit>> {
        return flowOf(if (Random.nextBoolean()) Unit.right() else Problem("Testing problem ;)").left())
    }

    override suspend fun joinExistingGroup(
        userId: String,
        groupId: String,
        asParticipantId: String?,
    ): Flow<ProblemOr<GroupInfoBasic>> = flow {
        if (Random.nextBoolean())
            getGroup(userId, groupId).collect { result ->
                val mapped = result.map {
                    GroupInfoBasic(groupId = it.group.groupId,
                        name = it.group.name,
                        defaultCurrency = it.group.defaultCurrency)
                }
                emit(mapped)
            }
        else emit(Problem("Testing problem ;)").left())
    }

    override suspend fun leaveGroup(userId: String, groupId: String): Flow<ProblemOr<Unit>> {
        return flowOf(if (Random.nextBoolean()) Unit.right() else Problem("Testing problem ;)").left())
    }

    override suspend fun addGroupParticipants(
        groupId: String,
        newParticipants: List<ParticipantBasic>,
    ): Flow<ProblemOr<Unit>> = flowOf(Unit.right())

    override suspend fun getGroupList(userId: String): Flow<ProblemOr<List<UsersGroupBasic>>> = flow {
        getGroup(userId, randomId()).collect { one ->
            emit(one.map { listOf(it) })
        }
    }

    override suspend fun getUsersGroupParticipants(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupParticipants>> {
        return flowOf(
            UsersGroupParticipants(
                groupParticipants = setOf(
                    amy, schubert, john, tom, danny, user, catherine
                ),
                userParticipantId = user.participantId,
                groupCurrency = CurrencyCode.CZK,
            ).right()
        )
    }

    override suspend fun getUsersGroupParticipantId(userId: String, groupId: String): Flow<ProblemOr<String?>> {
        return flowOf(user.participantId.right())
    }

    override suspend fun getBasicGroupInfo(userId: String, groupId: String): Flow<ProblemOr<UsersGroupInfoBasic>> {
        return flow {
            getGroup(userId, groupId).collect { usersGroupOrProblem ->
                val info = usersGroupOrProblem.map {
                    UsersGroupInfoBasic(
                        group = GroupInfoBasic(
                            groupId = it.group.groupId,
                            name = it.group.name,
                            defaultCurrency = it.group.defaultCurrency,
                        ),
                        userParticipantId = it.userParticipantId,
                    )
                }
                emit(info)
            }
        }
    }

    override suspend fun getGroup(userId: String, groupId: String): Flow<ProblemOr<UsersGroupBasic>> {
        return flowOf(UsersGroupBasic(
            userParticipantId = user.participantId,
            group = GroupBasic(
                groupId = randomId(),
                name = "Office lunches",
                defaultCurrency = CurrencyCode.CZK,
                participants = setOf(user, tom, amy, schubert, catherine, john, danny),
                transactions = listOf(
                    PurchaseDetail(
                        transactionId = randomId(),
                        name = "Pizza",
                        payers = listOf(Payer(
                            participant = john,
                            amount = amountFromDouble(1046.0)
                        )),
                        comments = emptyList(),
                        splitting = TransactionSplittingExplicit(
                            sharers = listOf(
                                TransactionShareExplicit(
                                    participant = john,
                                    shareValue = BigDecimal.valueOf(200)
                                ),
                                TransactionShareExplicit(
                                    participant = amy,
                                    shareValue = BigDecimal.valueOf(300)
                                ),
                                TransactionShareExplicit(
                                    participant = tom,
                                    shareValue = BigDecimal.valueOf(246)
                                ),
                                TransactionShareExplicit(
                                    participant = user,
                                    shareValue = BigDecimal.valueOf(300)
                                ),
                            )
                        ),
                        time = OffsetDateTime.of(2022, 3, 17, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
//                    time = OffsetDateTime.now(),
                        note = null,
                    ),
                    PurchaseDetail(
                        transactionId = randomId(),
                        name = "Pasta",
                        payers = listOf(Payer(
                            participant = danny,
                            amount = amountFromDouble(900.0)
                        )),
                        comments = emptyList(),
                        splitting = TransactionSplittingExplicit(
                            sharers = listOf(
                                TransactionShareExplicit(
                                    participant = danny,
                                    shareValue = BigDecimal.valueOf(300)
                                ),
                                TransactionShareExplicit(
                                    participant = tom,
                                    shareValue = BigDecimal.valueOf(350)
                                ),
                                TransactionShareExplicit(
                                    participant = schubert,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                            )
                        ),
                        time = OffsetDateTime.of(2022, 3, 16, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = null,
                    ),
                    PurchaseDetail(
                        transactionId = randomId(),
                        name = "McDontal's",
                        payers = listOf(
                            Payer(
                                participant = user,
                                amount = amountFromDouble(400.0)
                            ),
                            Payer(
                                participant = amy,
                                amount = amountFromDouble(383.0)
                            ),
                        ),
                        comments = emptyList(),
                        splitting = TransactionSplittingExplicit(
                            sharers = listOf(
                                TransactionShareExplicit(
                                    participant = amy,
                                    shareValue = BigDecimal.valueOf(200)
                                ),
                                TransactionShareExplicit(
                                    participant = user,
                                    shareValue = BigDecimal.valueOf(200)
                                ),
                                TransactionShareExplicit(
                                    participant = catherine,
                                    shareValue = BigDecimal.valueOf(200)
                                ),
                                TransactionShareExplicit(
                                    participant = schubert,
                                    shareValue = BigDecimal.valueOf(183)
                                ),
                            )
                        ),
                        time = OffsetDateTime.of(2022, 3, 14, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = null,
                    ),
                    TransferDetail(
                        transactionId = randomId(),
                        from = tom,
                        to = catherine,
                        amount = amountFromDouble(300.0),
                        comments = emptyList(),
                        time = OffsetDateTime.of(2022, 3, 11, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = null,
                    ),
                    PurchaseDetail(
                        transactionId = randomId(),
                        name = "Kirk's Forest Crispies",
                        payers = listOf(
                            Payer(
                                participant = catherine,
                                amount = amountFromDouble(400.0)
                            ),
                            Payer(
                                participant = danny,
                                amount = amountFromDouble(400.0)
                            ),
                        ),
                        comments = emptyList(),
                        splitting = TransactionSplittingExplicit(
                            sharers = listOf(
                                TransactionShareExplicit(
                                    participant = danny,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = catherine,
                                    shareValue = BigDecimal.valueOf(300)
                                ),
                                TransactionShareExplicit(
                                    participant = tom,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                            )
                        ),
                        time = OffsetDateTime.of(2022, 3, 10, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = "We should go there again",
                    ),
                    TransferDetail(
                        transactionId = randomId(),
                        from = user,
                        to = john,
                        amount = amountFromDouble(450.0),
                        comments = emptyList(),
                        time = OffsetDateTime.of(2022, 3, 9, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = null,
                    ),
                    PurchaseDetail(
                        transactionId = randomId(),
                        name = "Dumpling Apocalypse",
                        payers = listOf(
                            Payer(
                                participant = amy,
                                amount = amountFromDouble(800.0)
                            ),
                            Payer(
                                participant = danny,
                                amount = amountFromDouble(700.0)
                            ),
                        ),
                        comments = emptyList(),
                        splitting = TransactionSplittingExplicit(
                            sharers = listOf(
                                TransactionShareExplicit(
                                    participant = danny,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = catherine,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = tom,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = user,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = amy,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                                TransactionShareExplicit(
                                    participant = john,
                                    shareValue = BigDecimal.valueOf(250)
                                ),
                            )
                        ),
                        time = OffsetDateTime.of(2022, 3, 3, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                        note = null,
                    ),
                ))
        ).right())
    }
}
