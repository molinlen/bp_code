package cz.cvut.fit.molinlen.android.debttracker.feature.registration

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.left
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.RegisterNewUserUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.RegisterNewUserUseCaseImpl
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.ValidateRegistrationInputUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.ValidateRegistrationInputUseCaseImpl
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputProblem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class RegistrationViewModel(
    private val validateRegistrationUseCase: ValidateRegistrationInputUseCase = ValidateRegistrationInputUseCaseImpl(),
    private val registerNewUserUseCase: RegisterNewUserUseCase = RegisterNewUserUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(RegistrationViewState())
    val viewState = _viewState.asStateFlow()

    val minPasswordLength get() = registerNewUserUseCase.minPasswordLength

    private fun validateState() {
        viewModelScope.launch {
            val newProblems =
                validateRegistrationUseCase.validateRegistrationInput(viewState.value.toRegistrationInput())
            _viewState.update { state ->
                state.copy(
                    inputProblems = newProblems.fold(
                        { problems -> problems }, { EnumSet.noneOf(RegistrationInputProblem::class.java) }))
            }
        }
    }

    fun updateEmail(email: String) {
        _viewState.update {
            it.copy(email = email, registrationResult = null)
        }
        validateState()
    }

    fun updatePassword(password: String) {
        _viewState.update {
            it.copy(password = password, registrationResult = null)
        }
        validateState()
    }

    fun updateRepeatedPassword(repeatedPassword: String) {
        _viewState.update {
            it.copy(repeatedPassword = repeatedPassword, registrationResult = null)
        }
        validateState()
    }

    fun updateCheckboxChecked(checked: Boolean) {
        _viewState.update {
            it.copy(checkboxChecked = checked, registrationResult = null)
        }
        validateState()
    }

    fun updateEmailAndPassword(email: String, password: String) {
        _viewState.update {
            it.copy(
                email = email,
                password = password,
            )
        }
        validateState()
    }

    fun register(navigateAfterwards: () -> Unit) {
        _viewState.update { it.copy(processing = true, registrationResult = null) }
        viewModelScope.launch {
            registerNewUserUseCase.registerNewUser(viewState.value.toRegistrationInput()).collect { result ->
                Log.d(APP_TAG, "Got registration attempt result: [$result]")
                _viewState.update {
                    it.copy(registrationResult = result)
                }
                result.map {
                    withContext(Dispatchers.Main) {
                        navigateAfterwards()
                    }
                }
                _viewState.update {
                    it.copy(
                        processing = false,
                        registrationResult = result.fold({ problem -> problem.left() }, { null }),
                    )
                }
            }
        }
    }
}
