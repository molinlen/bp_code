package cz.cvut.fit.molinlen.android.debttracker.feature.groupInfo

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Group
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun GroupInfoScreen(
    groupId: String,
    navigateAfterBackClick: () -> Unit,
    navigateAfterEditGroupClick: () -> Unit,
    navigateAfterLeaveGroupClick: () -> Unit,
) {
    val viewModel: GroupInfoViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId)
    }
    val context = LocalContext.current

    GroupInfoScreenImpl(
        onBackClick = navigateAfterBackClick,
        onEditGroupClick = navigateAfterEditGroupClick,
        onLeaveGroupConfirm = { viewModel.leaveGroup(navigateAfterLeaveGroupClick) },
        onCopyToClipboardClick = { viewModel.copyGroupIdToClipboard(context = context) },
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@Composable
fun GroupInfoScreenImpl(
    onBackClick: () -> Unit,
    onEditGroupClick: () -> Unit,
    onLeaveGroupConfirm: () -> Unit,
    onCopyToClipboardClick: () -> Unit,
    viewState: GroupInfoViewState,
) {
    val leaveGroupDialogVisible = remember { mutableStateOf(false) }
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = stringResource(R.string.group_info),
                leftIcon = { HeaderBackIcon(onClick = onBackClick) },
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        if (leaveGroupDialogVisible.value) {
            DestructiveActionConfirmationDialog(
                visible = leaveGroupDialogVisible,
                questionText = stringResource(R.string.group_leave_dialog_question),
                confirmButtonText = stringResource(R.string.yes),
                onConfirmClick = onLeaveGroupConfirm,
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.screenContentModifier(paddingValues),
        ) {
            when {
                viewState.loading -> LoadingScreen(modifier = Modifier.weight(1f))
                viewState.error != null -> ErrorScreen(error = viewState.error, modifier = Modifier.weight(1f))
                else -> Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    viewState.usersGroup?.let { usersGroup ->
                        val group = usersGroup.group
                        GroupValuesTexts(group = group, onCopyToClipboardClick = onCopyToClipboardClick)
                    }
                }
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 10.dp),
            ) {
                EditGroupButton(onEditGroupClick = onEditGroupClick)
                LeaveGroupButton(onLeaveGroupClick = { leaveGroupDialogVisible.value = true })
            }
        }
    }
}

@Composable
private fun GroupValuesTexts(
    group: Group,
    onCopyToClipboardClick: () -> Unit,
) {
    val textModifier = Modifier.padding(vertical = 20.dp)
    Spacer(modifier = Modifier.padding(vertical = 10.dp))
    Text(
        text = buildAnnotatedString {
            append(stringResource(R.string.name_label))
            withStyle(SpanStyle(
                fontWeight = FontWeight.Bold,
                fontSize = MaterialTheme.typography.h6.fontSize,
            )) {
                append(group.name)
            }
        },
        modifier = textModifier,
        maxLines = 3,
        overflow = TextOverflow.Ellipsis,
    )
    Text(
        text = buildAnnotatedString {
            append(stringResource(R.string.currency_label))
            withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                append(group.defaultCurrency.name)
            }
        },
        modifier = textModifier,
    )
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth(),
    ) {
        BasicSelectionContainer {
            Text(text = stringResource(R.string.id_label) + group.groupId, modifier = textModifier)
        }
        CopyIdToClipboardButton(onClick = onCopyToClipboardClick)
    }
    Text(
        text = stringResource(R.string.group_id_copy_lead),
        style = MaterialTheme.typography.caption,
        fontStyle = FontStyle.Italic,
        modifier = Modifier.padding(horizontal = 25.dp),
    )
}

@Composable
private fun CopyIdToClipboardButton(
    onClick: () -> Unit,
) {
    IconButton(onClick = onClick) {
        Icon(
            painter = painterResource(R.drawable.content_copy),
            contentDescription = stringResource(R.string.copy_group_id_to_clipboard),
        )
    }
}

@Composable
private fun EditGroupButton(onEditGroupClick: () -> Unit) {
    BasicRectangleButton(
        text = stringResource(R.string.edit_group),
        iconImageVector = Icons.Default.Edit,
        onClick = onEditGroupClick,
    )
}

@Composable
private fun LeaveGroupButton(onLeaveGroupClick: () -> Unit) {
    BasicRectangleButton(
        text = stringResource(R.string.leave_group),
        iconImageVector = Icons.Default.Delete,
        onClick = onLeaveGroupClick,
        buttonColors = errorButtonColors(),
    )
}
