package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.validation.*
import java.util.*

interface ValidateGroupInputUseCase {
    suspend fun validateGroupInput(groupInput: GroupInputData): Either<EnumSet<GroupInfoInputProblem>, GroupInfoBasic>
}

class ValidateGroupInputUseCaseImpl(
    private val groupInputValidator: GroupInputValidator = GroupInputValidatorImpl(),
) : ValidateGroupInputUseCase {
    override suspend fun validateGroupInput(
        groupInput: GroupInputData,
    ): Either<EnumSet<GroupInfoInputProblem>, GroupInfoBasic> {
        return groupInputValidator.validateGroupInput(groupInput)
    }
}
