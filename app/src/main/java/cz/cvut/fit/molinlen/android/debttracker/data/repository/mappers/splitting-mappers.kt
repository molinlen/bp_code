package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.left
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.*
import java.math.BigDecimal

fun TransactionSplittingStrategy.toEntity() = TransactionSplittingStrategyEntity(
    type = this.getType(),
    data = when (this) {
        is TransactionSplittingExplicit -> this.toEntity()
        is TransactionSplittingPortion -> this.toEntity()
    }
)

fun TransactionSplittingExplicit.toEntity() = TransactionSplittingExplicitEntityData(
    shares = sharers.map { it.toEntity() },
)

fun TransactionSplittingPortion.toEntity() = TransactionSplittingPortionEntityData(
    shares = sharers.map { it.toEntity() },
)

fun TransactionShareExplicit.toEntity() = TransactionParticipantShareExplicitEntityData(
    participantId = participant.participantId,
    shareValue = shareValue.toPlainString(),
)

fun TransactionSharePortion.toEntity() = TransactionParticipantSharePortionEntityData(
    participantId = participant.participantId,
    shareValue = shareValue,
)

fun TransactionSplittingStrategyEntity.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransactionSplittingStrategy> {
    return when (data) {
        is TransactionSplittingExplicitEntityData ->
            (data as TransactionSplittingExplicitEntityData).toModelWithParticipants(participants)
        is TransactionSplittingPortionEntityData ->
            (data as TransactionSplittingPortionEntityData).toModelWithParticipants(participants)
        null -> Problem("Missing TransactionSplittingStrategyEntity data").left()
    }
}

fun TransactionSplittingExplicitEntityData.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransactionSplittingExplicit> {
    return either.eager { shares.map { it.toModelWithParticipants(participants).bind() } }
        .map { TransactionSplittingExplicit(it) }
}

fun TransactionSplittingPortionEntityData.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransactionSplittingPortion> {
    return either.eager { shares.map { it.toModelWithParticipants(participants).bind() } }
        .map { TransactionSplittingPortion(it) }
}

fun TransactionParticipantShareExplicitEntityData.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransactionShareExplicit> {
    return Either.catch {
        TransactionShareExplicit(
            participant = participants[participantId]!!,
            shareValue = BigDecimal(shareValue),
        )
    }.mapConversionThrowable("ShareExplicitEntity")
}

fun TransactionParticipantSharePortionEntityData.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransactionSharePortion> {
    return Either.catch {
        TransactionSharePortion(
            participant = participants[participantId]!!,
            shareValue = shareValue!!,
        )
    }.mapConversionThrowable("SharePortionEntity")
}
