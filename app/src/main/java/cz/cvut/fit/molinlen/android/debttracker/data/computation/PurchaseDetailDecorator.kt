package cz.cvut.fit.molinlen.android.debttracker.data.computation

import cz.cvut.fit.molinlen.android.debttracker.data.model.*

interface PurchaseDetailDecorator {
    fun decoratePortionSplitting(
        splittingPortion: TransactionSplittingPortion,
        totalAmount: Amount,
    ): ProblemOr<TransactionSplittingPortion>
}
