package cz.cvut.fit.molinlen.android.debttracker.data.computation.fake

import android.util.Log
import arrow.core.flatMap
import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.computation.GroupStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.USE_FAKE_DATA
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.time.ZoneOffset

class GroupStatusCalculatorFake : GroupStatusCalculator {

    override fun computeGroupPreview(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupPreview> {
        return computeGroupPreviews(emptyList()).flatMap {
            it.firstOrNull()?.right() ?: Problem("No preview found")
                .left()
        }
    }

    override fun computeGroupPreviews(usersGroups: List<UsersGroupBasic>): ProblemOr<List<UsersGroupPreview>> {
        return if (USE_FAKE_DATA) {
            previewList.right()
        } else {
            Log.d(APP_TAG, "groups: $usersGroups")
            usersGroups.map {
                UsersGroupPreview(
                    userParticipantId = it.userParticipantId,
                    group = GroupPreview(
                        groupId = it.group.groupId,
                        name = it.group.name,
                        defaultCurrency = it.group.defaultCurrency,
                        currentOwnStatus = amountZero(it.group.defaultCurrency),
                    )
                )
            }.right()
        }
    }

    override fun computeGroupStatus(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupDetail> {
        return if (USE_FAKE_DATA) {
            usersGroupDetail.right()
        } else {
            UsersGroupDetail(
                userParticipantId = usersGroup.userParticipantId,
                group = GroupDetail(
                    groupId = usersGroup.group.groupId,
                    name = usersGroup.group.name,
                    defaultCurrency = usersGroup.group.defaultCurrency,
                    transactions = usersGroup.group.transactions,
                    participants = usersGroup.group.participants.map {
                        ParticipantWithStatus(
                            participantId = it.participantId,
                            nickname = it.nickname,
                            currentStatus = amountZero(usersGroup.group.defaultCurrency),
                        )
                    }.toSet(),
                )
            ).right()
        }
    }

    private val user = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "James",
        currentStatus = amountFromDouble(217.0),
    )
    private val tom = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Tom",
        currentStatus = amountFromDouble(-297.0),
    )
    private val amy = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Amy",
        currentStatus = amountFromDouble(-181.0),
    )
    private val schubert = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Schubert",
        currentStatus = amountFromDouble(-135.0),
    )
    private val catherine = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Catherine",
        currentStatus = amountFromDouble(-121.0),
    )
    private val john = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "John",
        currentStatus = amountFromDouble(175.0),
    )
    private val danny = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Danny",
        currentStatus = amountFromDouble(342.0),
    )
    private val usersGroupDetail = UsersGroupDetail(
        userParticipantId = user.participantId,
        group = GroupDetail(
            groupId = randomId(),
            name = "Office lunches",
            defaultCurrency = CurrencyCode.CZK,
            participants = setOf(user, tom, amy, schubert, catherine, john, danny),
            transactions = listOf(
                PurchaseDetail(
                    transactionId = randomId(),
                    name = "Pizza",
                    payers = listOf(
                        Payer(
                            participant = john,
                            amount = amountFromDouble(1046.0)
                        )
                    ),
                    comments = emptyList(),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(
                                participant = john,
                                shareValue = BigDecimal.valueOf(200)
                            ),
                            TransactionShareExplicit(
                                participant = amy,
                                shareValue = BigDecimal.valueOf(300)
                            ),
                            TransactionShareExplicit(
                                participant = tom,
                                shareValue = BigDecimal.valueOf(246)
                            ),
                            TransactionShareExplicit(
                                participant = user,
                                shareValue = BigDecimal.valueOf(300)
                            ),
                        )
                    ),
                    time = OffsetDateTime.of(2022, 3, 17, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
//                    time = OffsetDateTime.now(),
                    note = null,
                ),
                PurchaseDetail(
                    transactionId = randomId(),
                    name = "Pasta",
                    payers = listOf(
                        Payer(
                            participant = danny,
                            amount = amountFromDouble(900.0)
                        )
                    ),
                    comments = listOf(
                        CommentDetail(
                            writerUserId = "4IePFt8v0eNqtioh1GyGK2RJj123",
                            content = "Hi from molinlen",
                            time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                            edited = true,
                        ),
                    ),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(
                                participant = danny,
                                shareValue = BigDecimal.valueOf(300)
                            ),
                            TransactionShareExplicit(
                                participant = tom,
                                shareValue = BigDecimal.valueOf(350)
                            ),
                            TransactionShareExplicit(
                                participant = schubert,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                        )
                    ),
                    time = OffsetDateTime.of(2022, 3, 16, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = null,
                ),
                PurchaseDetail(
                    transactionId = randomId(),
                    name = "McDontal's",
                    payers = listOf(
                        Payer(
                            participant = user,
                            amount = amountFromDouble(400.0)
                        ),
                        Payer(
                            participant = amy,
                            amount = amountFromDouble(383.0)
                        ),
                    ),
                    comments = listOf(
                        CommentDetail(
                            writerUserId = "9IePFt8v0eNqtioh1GyGK2RJj129",
                            content = "Ciao ciao",
                            time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                            edited = true,
                        ),
                    ),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(
                                participant = amy,
                                shareValue = BigDecimal.valueOf(200)
                            ),
                            TransactionShareExplicit(
                                participant = user,
                                shareValue = BigDecimal.valueOf(200)
                            ),
                            TransactionShareExplicit(
                                participant = catherine,
                                shareValue = BigDecimal.valueOf(200)
                            ),
                            TransactionShareExplicit(
                                participant = schubert,
                                shareValue = BigDecimal.valueOf(183)
                            ),
                        )
                    ),
                    time = OffsetDateTime.of(2022, 3, 14, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = null,
                ),
                TransferDetail(
                    transactionId = randomId(),
                    from = tom,
                    to = catherine,
                    amount = amountFromDouble(300.0),
                    comments = emptyList(),
                    time = OffsetDateTime.of(2022, 3, 11, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = null,
                ),
                PurchaseDetail(
                    transactionId = randomId(),
                    name = "Kirk's Forest Crispies",
                    payers = listOf(
                        Payer(
                            participant = catherine,
                            amount = amountFromDouble(400.0)
                        ),
                        Payer(
                            participant = danny,
                            amount = amountFromDouble(400.0)
                        ),
                    ),
                    comments = emptyList(),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(
                                participant = danny,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = catherine,
                                shareValue = BigDecimal.valueOf(300)
                            ),
                            TransactionShareExplicit(
                                participant = tom,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                        )
                    ),
                    time = OffsetDateTime.of(2022, 3, 10, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = "We should go there again",
                ),
                TransferDetail(
                    transactionId = randomId(),
                    from = user,
                    to = john,
                    amount = amountFromDouble(450.0),
                    comments = listOf(
                        CommentDetail(
                            writerUserId = "4IePFt8v0eNqtioh1GyGK2RJj123",
                            content = "Hello there",
                            time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                            edited = true,
                        ),
                    ),
                    time = OffsetDateTime.of(2022, 3, 9, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = null,
                ),
                PurchaseDetail(
                    transactionId = randomId(),
                    name = "Dumpling Apocalypse",
                    payers = listOf(
                        Payer(
                            participant = amy,
                            amount = amountFromDouble(800.0)
                        ),
                        Payer(
                            participant = danny,
                            amount = amountFromDouble(700.0)
                        ),
                    ),
                    comments = emptyList(),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(
                                participant = danny,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = catherine,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = tom,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = user,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = amy,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                            TransactionShareExplicit(
                                participant = john,
                                shareValue = BigDecimal.valueOf(250)
                            ),
                        )
                    ),
                    time = OffsetDateTime.of(2022, 3, 3, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
                    note = null,
                ),
            )
        )
    )

    private val previewList = listOf(
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Office lunches",
                currentOwnStatus = amountFromDouble(217.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Grandma",
                currentOwnStatus = amountFromDouble(130.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Coffee exchange",
                currentOwnStatus = amountFromDouble(-20.5, CurrencyCode.EUR),
                defaultCurrency = CurrencyCode.EUR,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Hiking with Tim and Tony",
                currentOwnStatus = amountFromDouble(3.15, CurrencyCode.USD),
                defaultCurrency = CurrencyCode.USD,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Judo beers",
                currentOwnStatus = amountFromDouble(-190.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Thelma & Louise",
                currentOwnStatus = amountFromDouble(-10.0, CurrencyCode.USD),
                defaultCurrency = CurrencyCode.USD,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Groceries",
                currentOwnStatus = amountFromDouble(1513.62, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Used books club",
                currentOwnStatus = amountFromDouble(43.15, CurrencyCode.USD),
                defaultCurrency = CurrencyCode.USD,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Ingrid",
                currentOwnStatus = amountFromDouble(-102.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Old office beers",
                currentOwnStatus = amountFromDouble(97.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Squirrel spotting gear purchases",
                currentOwnStatus = amountFromDouble(11.3, CurrencyCode.EUR),
                defaultCurrency = CurrencyCode.EUR,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "XYZ Summer Rock Fest",
                currentOwnStatus = amountFromDouble(0.0, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Your Mom",
                currentOwnStatus = amountFromDouble(313.7, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
        UsersGroupPreview(
            userParticipantId = randomId(),
            group = GroupPreview(
                name = "Abisudhfabfuuisygb Bunnies",
                currentOwnStatus = amountFromDouble(567.89, CurrencyCode.CZK),
                defaultCurrency = CurrencyCode.CZK,
            ),
        ),
    )
}
