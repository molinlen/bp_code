package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import com.google.firebase.Timestamp

data class CommentEntityData(
    var content: String? = null,
    var time: Timestamp? = null,
    var edited: Boolean? = null,
)

data class CommentEntity(
    val writerUserId: String? = null,
    val data: CommentEntityData? = null,
)
