package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import com.google.firebase.Timestamp
import cz.cvut.fit.molinlen.android.debttracker.data.model.SplittingType

data class PurchaseEntityData(
    var name: String? = null,
    var payers: List<PayerEntity> = emptyList(),
    var splitting: TransactionSplittingStrategyEntity? = null,
    var time: Timestamp? = null,
    var note: String? = null,
    var comments: List<CommentEntity> = emptyList(),
)

data class PurchaseEntityReadData(
    var name: String? = null,
    var payers: List<PayerEntity> = emptyList(),
    var splitting: TransactionSplittingStrategyReadEntity? = null,
    var time: Timestamp? = null,
    var note: String? = null,
    var comments: List<CommentEntity> = emptyList(),
) {
    data class TransactionSplittingStrategyReadEntity(
        var type: SplittingType? = null,
        var data: Map<String, Any> = emptyMap(),
    )
}

data class PurchaseEntityUpdateData(
    val name: String?,
    val payers: List<PayerEntity>,
    val splitting: TransactionSplittingStrategyEntity?,
    var time: Timestamp? = null,
    val note: String?,
)

data class PurchaseEntity(
    var transactionId: String? = null,
    var purchase: PurchaseEntityData? = null,
)

data class PayerEntity(
    var participantId: String? = null,
    var amount: AmountEntityData? = null,
)

data class TransactionSplittingStrategyEntity(
    var type: SplittingType? = null,
    var data: TransactionSplittingStrategyEntityData? = null,
)


sealed interface TransactionSplittingStrategyEntityData

data class TransactionSplittingExplicitEntityData(
    var shares: List<TransactionParticipantShareExplicitEntityData> = emptyList(),
) : TransactionSplittingStrategyEntityData

data class TransactionSplittingPortionEntityData(
    var shares: List<TransactionParticipantSharePortionEntityData> = emptyList(),
) : TransactionSplittingStrategyEntityData

//data class TransactionSplittingPercentEntityData(
//    var shares: List<TransactionParticipantSharePercentEntityData> = emptyList(),
//) : TransactionSplittingStrategyEntityData
//
//data class TransactionSplittingExplicitAndPercentEntityData(
//    var sharesExplicit: List<TransactionParticipantShareExplicitEntityData> = emptyList(),
//    var sharesPercent: List<TransactionParticipantSharePercentEntityData> = emptyList(),
//) : TransactionSplittingStrategyEntityData


interface TransactionParticipantShareEntityData {
    var participantId: String?
}

data class TransactionParticipantShareExplicitEntityData(
    override var participantId: String? = null,
    var shareValue: String? = null,
) : TransactionParticipantShareEntityData

data class TransactionParticipantSharePortionEntityData(
    override var participantId: String? = null,
    var shareValue: Int? = null,
) : TransactionParticipantShareEntityData

//data class TransactionParticipantSharePercentEntityData(
//    override var participantId: String? = null,
//    var shareValue: Int? = null,
//) : TransactionParticipantShareEntityData

