package cz.cvut.fit.molinlen.android.debttracker.data.validation

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInfo
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInput
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputValidator.Companion.MIN_PASSWORD_LENGTH
import java.util.*

interface RegistrationInputValidator {
    suspend fun validateRegistrationInput(registrationInput: RegistrationInput): Either<EnumSet<RegistrationInputProblem>, RegistrationInfo>

    companion object {
        const val MIN_PASSWORD_LENGTH = 6
    }
}

class RegistrationInputValidatorImpl : RegistrationInputValidator {
    override suspend fun validateRegistrationInput(
        registrationInput: RegistrationInput,
    ): Either<EnumSet<RegistrationInputProblem>, RegistrationInfo> {
        val fails = listOfNotNull(
            validateEmailFormat(registrationInput),
            validateIsSamePassword(registrationInput),
            validatePasswordLength(registrationInput),
            validateTermsAcceptation(registrationInput),
        )

        return when {
            fails.isNotEmpty() -> EnumSet.copyOf(fails).left()
            else -> RegistrationInfo(
                email = registrationInput.email,
                password = registrationInput.passwordFirst,
                termsAccepted = registrationInput.termsAccepted,
            ).right()
        }
    }

    //todo validate against firebase auth if possible

    //todo validate previous email registration

    private fun validateEmailFormat(registrationInput: RegistrationInput): RegistrationInputProblem? {
        //todo validate email format
        return if (registrationInput.email.isBlank()) {
            RegistrationInputProblem.INVALID_EMAIL
        } else {
            null
        }
    }

    private fun validateIsSamePassword(registrationInput: RegistrationInput): RegistrationInputProblem? {
        with(registrationInput) {
            return if (passwordFirst != passwordRepeated || passwordRepeated.isBlank()) {
                RegistrationInputProblem.PASSWORDS_NOT_MATCH
            } else {
                null
            }
        }
    }

    private fun validatePasswordLength(registrationInput: RegistrationInput): RegistrationInputProblem? {
        return if (registrationInput.passwordFirst.length < MIN_PASSWORD_LENGTH) {
            RegistrationInputProblem.PASSWORD_TOO_SHORT
        } else {
            null
        }
    }

    private fun validateTermsAcceptation(registrationInput: RegistrationInput): RegistrationInputProblem? {
        return if (registrationInput.termsAccepted.not()) {
            RegistrationInputProblem.TERMS_NOT_ACCEPTED
        } else {
            null
        }
    }
}
