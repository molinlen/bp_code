package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import com.google.firebase.Timestamp
import java.time.OffsetDateTime
import java.time.ZoneId
import java.util.*

fun Timestamp.toOffsetDateTime(): OffsetDateTime {
    return OffsetDateTime.ofInstant(this.toDate().toInstant(), ZoneId.systemDefault())
}

fun OffsetDateTime.toTimestamp(): Timestamp {
    return Timestamp(Date.from(this.toInstant()))
}
