package cz.cvut.fit.molinlen.android.debttracker.data.model

import java.math.BigDecimal

enum class SplittingType {
    EXPLICIT,
//    PERCENT,
    PORTION,
//    EXPLICIT_AND_PERCENT,
//    EXPLICIT_AND_PORTION,
    ;
}

//todo penny rounding !!!

sealed interface TransactionSplittingStrategyType {
    fun getType(): SplittingType
}

sealed interface TransactionSplittingParticipants {
    fun getParticipants(): List<Participant>
}

sealed interface TransactionSplittingStrategy : TransactionSplittingStrategyType, TransactionSplittingParticipants

data class TransactionSplittingExplicit(
    val sharers: List<TransactionShareExplicit>,
) : TransactionSplittingStrategy {
    override fun getType(): SplittingType = SplittingType.EXPLICIT
    override fun getParticipants(): List<Participant> = sharers.map { it.participant }
}

data class TransactionSplittingPortion(
    val sharers: List<TransactionSharePortion>,
) : TransactionSplittingStrategy {
    override fun getType(): SplittingType = SplittingType.PORTION
    override fun getParticipants(): List<Participant> = sharers.map { it.participant }
}

//data class TransactionSplittingPercent(
//    val sharers: List<TransactionSharePercent>,
//) : TransactionSplittingStrategy {
//    override fun getType(): SplittingType = SplittingType.PERCENT
//    override fun getParticipants(): List<Participant> = sharers.map { it.participant }
//}
//data class TransactionSplittingExplicitAndPercent(
//    val sharersExplicit: List<TransactionShareExplicit>,
//    val sharersPercent: List<TransactionSharePercent>,
//) : TransactionSplittingStrategy {
//    override fun getType(): SplittingType = SplittingType.EXPLICIT_AND_PERCENT
//    override fun getParticipants(): List<Participant> =
//        sharersExplicit.map { it.participant } + sharersPercent.map { it.participant }
//}

sealed interface TransactionShare {
    val participant: Participant
}

data class TransactionShareExplicit(
    override val participant: Participant,
    val shareValue: BigDecimal,
) : TransactionShare

data class TransactionSharePortion(
    override val participant: Participant,
    val shareValue: Int,
    val shareValueComputed: BigDecimal? = null,
) : TransactionShare

//data class TransactionSharePercent(
//    override val participant: Participant,
//    val shareValue: Int,
//) : TransactionShare
