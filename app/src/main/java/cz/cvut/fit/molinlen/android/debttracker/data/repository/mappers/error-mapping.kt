package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem

fun <R> Either<Throwable, R>.mapConversionThrowable(where: String) = this.mapLeft { throwable ->
    when (throwable) {
        is NullPointerException -> Problem("Missing mandatory value in $where", cause = throwable)
        is NoSuchElementException -> Problem("Could not assign value - not found in $where", cause = throwable)
        is Exception -> Problem("Failed conversion from entity to model in $where", cause = throwable)
        else -> Problem("Got unexpected throwable converting entity to model in $where", cause = throwable)
    }
}
