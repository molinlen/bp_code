package cz.cvut.fit.molinlen.android.debttracker.feature.userdetail

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors

@Composable
fun UserDetailScreen(
    navigateAfterDataEditClick: () -> Unit,
    navigateAfterBackClick: () -> Unit,
) {
    val viewModel: UserDetailViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData()
    }

    UserDetailScreenImpl(
        userEmail = viewModel.userEmail,
        onDataEditClick = navigateAfterDataEditClick,
        onBackClick = navigateAfterBackClick,
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@Composable
fun UserDetailScreenImpl(
    userEmail: String?,
    onDataEditClick: () -> Unit,
    onBackClick: () -> Unit,
    viewState: UserDetailViewState,
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = stringResource(R.string.account_info),
                leftIcon = { HeaderBackIcon(onClick = onBackClick) },
            )
        },
    ) { paddingValues ->
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> Column(
                modifier = Modifier.screenContentModifier(paddingValues),
            ) {
                UserDetailScreenContent(
                    userEmail = userEmail,
                    userBasic = viewState.userBasic,
                    onDataEditClick = onDataEditClick,
                )
            }
        }
    }
}

@Composable
private fun UserDetailScreenContent(
    userEmail: String?,
    userBasic: UserBasic?,
    onDataEditClick: () -> Unit,
) {
    val textModifier = Modifier.padding(vertical = 25.dp)
    Spacer(modifier = Modifier.padding(bottom = 25.dp))
    userEmail?.let { email ->
        Text(
            text = prefixAndSuffixString(prefix = stringResource(R.string.email_label), suffix = email),
            modifier = textModifier,
        )
    }
    userBasic?.let { userInfo ->
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Text(
                text = prefixAndSuffixString(
                    prefix = stringResource(R.string.primary_currency_label),
                    suffix = userInfo.primaryCurrency?.name ?: stringResource(R.string.none),
                    isError = userInfo.primaryCurrency == null,
                ),
                modifier = textModifier,
            )
            IconButton(onClick = onDataEditClick) {
                Icon(
                    imageVector = Icons.Default.Edit,
                    contentDescription = stringResource(R.string.edit_currency),
                )
            }
        }
    }
}

@Composable
private fun prefixAndSuffixString(
    prefix: String,
    suffix: String,
    isError: Boolean = false,
): AnnotatedString = buildAnnotatedString {
    append(prefix)
    withStyle(SpanStyle(
        fontWeight = FontWeight.Bold,
        color = if (isError) AccentColors.redContent else Color.Unspecified,
    )) {
        append(suffix)
    }
}
