package cz.cvut.fit.molinlen.android.debttracker.feature.groupparticipants

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.ParticipantBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.nullIfBlank
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class GroupParticipantsViewModel(
    private val getGroupParticipantsUseCase: GetGroupParticipantsUseCase = GetGroupParticipantsUseCaseImpl(),
    private val createGroupParticipantsUseCase: CreateGroupParticipantsUseCase = CreateGroupParticipantsUseCaseImpl(),
    private val updateUserGroupLinkUseCase: UpdateUserGroupLinkUseCase = UpdateUserGroupLinkUseCaseImpl(),
) : ViewModel() {
    private lateinit var groupId: String

    private val _viewState = MutableStateFlow(GroupParticipantsViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private val auth = Firebase.auth

    fun loadData(groupId: String) {
        this.groupId = groupId
        viewModelScope.launch {
            auth.currentUser
                ?.let { user ->
                    getGroupParticipantsUseCase.getUsersGroupParticipants(userId = user.uid, groupId = groupId)
                        .collect { fetched ->
                            _viewState.update {
                                fetched.fold(
                                    { problem ->
                                        GroupParticipantsViewState(error = problem)
                                    }, { usersGroupParticipants ->
                                        it.copy(groupParticipants = usersGroupParticipants, loading = false)
                                    }
                                )
                            }
                        }
                }
                ?: run {
                    _viewState.update { GroupParticipantsViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in group participants without currentUser present")
                }
        }
    }

    fun updateFutureParticipant(nickname: String) {
        Log.d(APP_TAG, "Updating nickname A [$nickname]")
        _viewState.update { it.copy(currentlyAddingParticipantNickname = nickname, participantAdditionError = null) }
    }

    fun canAddCurrentParticipant(): Boolean {
        return addableParticipantNickname(viewState.value.currentlyAddingParticipantNickname) != null
    }

    private fun addableParticipantNickname(inputNickname: String): String? {
        return inputNickname.trim().nullIfBlank()?.let { newNickname ->
            viewState.value.groupParticipants?.let { group ->
                when (newNickname) {
                    !in group.groupParticipants.map { it.nickname }.toSet() -> newNickname
                    else -> null
                }
            }
        }
    }

    fun addParticipant() {
        viewModelScope.launch {
            addableParticipantNickname(viewState.value.currentlyAddingParticipantNickname)?.let { nickname ->
                _viewState.update { it.copy(processing = true, participantAdditionError = null) }
                createGroupParticipantsUseCase.addGroupParticipant(
                    groupId = groupId,
                    participant = ParticipantBasic(participantId = "", nickname = nickname),
                ).collect { result ->
                    result.map { updateFutureParticipant("") }
                    _viewState.update {
                        it.copy(
                            processing = false,
                            participantAdditionError = result.fold({ problem -> problem }, { null }),
                        )
                    }
                }
            }
        }
    }

    fun pairUserToParticipant(participant: Participant?) {
        auth.currentUser?.uid?.let { userId ->
            viewModelScope.launch {
                updateUserGroupLinkUseCase.updateUserGroupLink(userId, groupId, participant?.participantId)
                    .collect { result ->
                        result.mapLeft {
                            Log.w(APP_TAG,
                                "Failed to link user [$userId] to participant [${participant?.participantId}]: [$it] ")
                        }
                    }
            }
        }
    }
}
