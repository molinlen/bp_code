package cz.cvut.fit.molinlen.android.debttracker.feature.groupedit

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.EditMode
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.validation.GroupInfoInputProblem
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun GroupEditScreen(
    groupId: String?,
    navigateAfterSave: (groupId: String) -> Unit,
    navigateAfterDiscard: () -> Unit,
    navigateAfterClose: () -> Unit = navigateAfterDiscard,
) {
    val viewModel: GroupEditViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId)
    }

    GroupEditScreenImpl(
        mode = EditMode.fromId(groupId),
        onSave = { viewModel.save(navigateAfterSave) },
        onDiscard = navigateAfterDiscard,
        onClose = navigateAfterClose,
        updateGroupName = { viewModel.updateGroupName(it) },
        updateGroupCurrency = { viewModel.updateGroupCurrency(it) },
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@Composable
fun GroupEditScreenImpl(
    mode: EditMode,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    onClose: () -> Unit,
    updateGroupName: (String) -> Unit,
    updateGroupCurrency: (CurrencyCode) -> Unit,
    viewState: GroupEditViewState,
) {
    val currencyPickerExpanded = remember { mutableStateOf(false) }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = when (mode) {
                    EditMode.CREATE -> stringResource(R.string.create_group)
                    EditMode.EDIT -> stringResource(R.string.edit_group)
                },
                leftIcon = { HeaderCloseIcon(onClick = onClose) },
                backgroundColor = ScreenHeaderColors.editScreenColor,
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> viewState.groupInput?.let { groupInput ->
                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .screenContentModifier(paddingValues)
                        .clearFocusOnTap()
                ) {
                    Column {
                        Spacer(modifier = Modifier.padding(vertical = 10.dp))
                        BasicTextField(
                            label = stringResource(R.string.name),
                            value = groupInput.name,
                            onValueChange = updateGroupName,
                            isError = viewState.inputProblems.contains(GroupInfoInputProblem.GROUP_NAME_BLANK),
                            keyboardOptions = KeyboardOptions(
                                capitalization = KeyboardCapitalization.Words,
                                imeAction = ImeAction.Done,
                            ),
                            supportingText = stringResource(R.string.field_cant_be_blank),
                        )
                        GroupCurrencyPicker(
                            currencyPickerExpanded = currencyPickerExpanded,
                            currentCurrency = groupInput.defaultCurrency,
                            updateGroupCurrency = updateGroupCurrency,
                        )
                    }
                    SaveAndDiscardButtons(
                        saveText = if (mode == EditMode.CREATE) stringResource(R.string.create) else stringResource(R.string.save_changes),
                        onSave = onSave,
                        onDiscard = onDiscard,
                        saveEnabled = viewState.inputProblems.isEmpty(),
                    )
                }
            }
        }
    }
}

@Composable
private fun GroupCurrencyPicker(
    currencyPickerExpanded: MutableState<Boolean>,
    currentCurrency: CurrencyCode?,
    updateGroupCurrency: (CurrencyCode) -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(vertical = 20.dp),
    ) {
        Text(
            text = stringResource(R.string.primary_currency_prefix),
            modifier = Modifier
                .padding(end = 10.dp)
                .tweakForLabeledPickerRowLeadingText(),
        )
        DropDownPicker(
            menuExpanded = currencyPickerExpanded,
            pickedText = currentCurrency?.name ?: "...",
            contentDescription = stringResource(R.string.pick_currency),
            isError = currentCurrency == null,
            supportingText = stringResource(R.string.field_is_mandatory),
            modifier = Modifier.fillMaxWidth(0.6f)
        ) {
            CurrencyCode.values().map {
                DropdownMenuItem(
                    onClick = {
                        updateGroupCurrency(it)
                        currencyPickerExpanded.value = false
                    }
                ) {
                    Text(it.name)
                }
            }
        }
    }
}
