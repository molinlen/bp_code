package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic
import cz.cvut.fit.molinlen.android.debttracker.data.repository.UserRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.UserRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.UserRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface GetUserBasicInfoUseCase {
    suspend fun getBasicUserInfo(
        userId: String,
    ): Flow<ProblemOr<UserBasic>>
}

class GetUserBasicInfoUseCaseImpl(
    private val userRepository: UserRepository = if (USE_FAKE_DATA) UserRepositoryFake() else UserRepositoryImpl(),
) : GetUserBasicInfoUseCase {
    override suspend fun getBasicUserInfo(
        userId: String,
    ): Flow<ProblemOr<UserBasic>> = userRepository.getUserInfo(userId = userId)
}
