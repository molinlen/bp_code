package cz.cvut.fit.molinlen.android.debttracker.feature.login

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.LoginUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.LoginUseCaseImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(
    private val loginUseCase: LoginUseCase = LoginUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(LoginViewState())
    val viewState = _viewState.asStateFlow()

    fun updateEmail(email: String) {
        _viewState.update { //todo validation
            it.copy(email = email, loginResult = null)
        }
    }

    fun updatePassword(password: String) {
        _viewState.update {
            it.copy(password = password, loginResult = null)
        }
    }

    fun logIn(navigateAfterwards: () -> Unit) {
        _viewState.update { it.copy(processing = true) }
        viewModelScope.launch {
            loginUseCase.login(viewState.value.toLoginInfo()).collectLatest { result ->
                Log.d(APP_TAG, "Got login result: [$result]")
                _viewState.update {
                    it.copy(loginResult = result)
                }
                result.map {
                    withContext(Dispatchers.Main) { navigateAfterwards() }
                }
                _viewState.update { it.copy(processing = false) }
            }
        }
    }
}
