package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.ParticipantBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.ParticipantEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.ParticipantEntityData

fun Participant.toEntity() = ParticipantEntity(
    participantId = participantId,
    participant = this.toEntityData()
)

fun Participant.toEntityData() = ParticipantEntityData(
    nickname = nickname,
)

fun ParticipantEntity.toModel(): ProblemOr<ParticipantBasic> = Either.catch {
    ParticipantBasic(
        participantId = participantId,
        nickname = participant.nickname!!,
    )
}.mapConversionThrowable("ParticipantEntity [$participantId]")

fun <P : Participant> Collection<P>.associatedByParticipantIds(): Map<String, P> = this.associateBy { it.participantId }
