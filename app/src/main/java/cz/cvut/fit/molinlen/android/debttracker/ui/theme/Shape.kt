package cz.cvut.fit.molinlen.android.debttracker.ui.theme

import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(4.dp),
    large = RoundedCornerShape(0.dp)
)

object SpecificShapes {
    val bottomSheet = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp)
    val dialog = RoundedCornerShape(8.dp)
    val tab = TabBorder(8.dp)
}

/** Basically a RoundedCornerShape border without the bottom line **/
class TabBorder(private val roundedCorners: Dp) : Shape {
    override fun createOutline(size: Size, layoutDirection: LayoutDirection, density: Density): Outline {
        val wantedCornerSize = 2 * CornerSize(roundedCorners).toPx(size, density)
        val usedCornerSize = when {
            size.minDimension < wantedCornerSize -> size.minDimension // at most half of smaller side
            else -> wantedCornerSize
        }
        return Outline.Generic(
            path = drawTabBorderPath(size = size, cornerSize = usedCornerSize)
        )
    }

    private fun drawTabBorderPath(size: Size, cornerSize: Float): Path {
        return Path().apply {
            moveTo(0f, size.height)
            lineTo(0f, cornerSize)
            arcTo(
                rect = Rect(
                    left = 0f,
                    top = 0f,
                    right = cornerSize,
                    bottom = cornerSize
                ),
                startAngleDegrees = 180.0f,
                sweepAngleDegrees = 90.0f,
                forceMoveTo = false,
            )
            lineTo(size.width - cornerSize, 0f)
            arcTo(
                rect = Rect(
                    left = size.width - cornerSize,
                    top = 0f,
                    right = size.width,
                    bottom = cornerSize
                ),
                startAngleDegrees = 270.0f,
                sweepAngleDegrees = 90.0f,
                forceMoveTo = false,
            )
            lineTo(size.width, size.height)
        }
    }
}
