package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.SpecificShapes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

val screenContentPadding = 15.dp

fun Modifier.screenContentModifier(screenPaddingValues: PaddingValues = PaddingValues()): Modifier = this
    .fillMaxSize()
    .padding(screenPaddingValues)
    .padding(horizontal = screenContentPadding)

@Composable
fun ContentTopSpacer(topPadding: Dp = 6.dp) {
    Spacer(modifier = Modifier.padding(top = topPadding))
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun rememberBottomSheetMenuState(initialValue: ModalBottomSheetValue = ModalBottomSheetValue.Hidden) =
    rememberModalBottomSheetState(initialValue)

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomSheetMenu(
    sheetState: ModalBottomSheetState,
    coroutineScope: CoroutineScope,
    modifier: Modifier = Modifier,
    menuContent: @Composable () -> Unit,
    content: @Composable () -> Unit,
) {
    BackHandler(enabled = sheetState.isVisible) {
        coroutineScope.launch {
            sheetState.hide()
        }
    }
    LaunchedEffect(Unit) {
        ensureHiddenMenuState(coroutineScope, sheetState)
    }

    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetContent = {
            menuContent()
        },
        sheetBackgroundColor = AccentColors.surfaceAccentMild,
        sheetShape = SpecificShapes.bottomSheet,
        modifier = modifier,
    ) {
        content()
    }
}

@OptIn(ExperimentalMaterialApi::class)
fun toggleMenuState(
    coroutineScope: CoroutineScope,
    menuScaffoldState: ModalBottomSheetState,
) {
    coroutineScope.launch {
        if (menuScaffoldState.isVisible) {
            menuScaffoldState.hide()
        } else {
            menuScaffoldState.show()
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
fun ensureHiddenMenuState(
    coroutineScope: CoroutineScope,
    menuScaffoldState: ModalBottomSheetState,
) {
    if (menuScaffoldState.isVisible) {
        coroutineScope.launch {
            menuScaffoldState.hide()
        }
    }
}

@Composable
fun BottomSheetMenuContentColumn(
    modifier: Modifier = Modifier,
    verticalArrangement: Arrangement.Vertical = Arrangement.Center,
    horizontalAlignment: Alignment.Horizontal = Alignment.CenterHorizontally,
    content: @Composable () -> Unit,
) {
    Column(
        verticalArrangement = verticalArrangement,
        horizontalAlignment = horizontalAlignment,
        modifier = modifier
            .fillMaxWidth()
            .padding(vertical = 15.dp),
    ) {
        content()
    }
}

@Composable
fun DestructiveActionConfirmationDialog(
    visible: MutableState<Boolean>,
    questionText: String,
    confirmButtonText: String,
    onConfirmClick: () -> Unit,
) {
    Dialog(onDismissRequest = { visible.value = false }) {
        Surface(shape = SpecificShapes.dialog) {
            Column(modifier = Modifier.padding(15.dp)) {
                Text(
                    text = questionText,
                    modifier = Modifier.padding(top = 5.dp, bottom = 20.dp),
                )
                Row(horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxWidth()) {
                    TextButton(onClick = { visible.value = false }, colors = alternativeTextButtonColors()) {
                        Text(text = stringResource(R.string.cancel))
                    }
                    TextButton(onClick = onConfirmClick, colors = errorTextButtonColors()) {
                        Text(text = confirmButtonText)
                    }
                }
            }
        }
    }
}
