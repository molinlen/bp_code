package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UserEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UserEntityUpdateData

fun UserUpdateInputData.toUpdateData(): ProblemOr<UserUpdateData> {
    return Either.catch {
        UserUpdateData(
            primaryCurrency = primaryCurrency!!,
        )
    }.mapConversionThrowable("conversion of UserUpdateInput")
}

fun User.toInputData(): UserUpdateInputData {
    return UserUpdateInputData(
        primaryCurrency = primaryCurrency,
    )
}

fun UserUpdateData.toEntityUpdateData(): UserEntityUpdateData = UserEntityUpdateData(
    primaryCurrency = primaryCurrency,
)

fun UserEntity.toModel(): ProblemOr<UserBasic> {
    return Either.catch {
        UserBasic(
            userId = userId!!,
            primaryCurrency = data?.primaryCurrency,
        )
    }.mapConversionThrowable("UserEntity [$userId]")
}
