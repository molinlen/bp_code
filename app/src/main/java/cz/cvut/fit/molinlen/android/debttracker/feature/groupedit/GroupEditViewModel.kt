package cz.cvut.fit.molinlen.android.debttracker.feature.groupedit

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import cz.cvut.fit.molinlen.android.debttracker.data.validation.GroupInfoInputProblem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class GroupEditViewModel(
    private val getGroupUseCase: GetGroupBasicInfoUseCase = GetGroupBasicInfoUseCaseImpl(),
    private val validationUseCase: ValidateGroupInputUseCase = ValidateGroupInputUseCaseImpl(),
    private val createGroupUseCase: CreateGroupUseCase = CreateGroupUseCaseImpl(),
    private val updateGroupUseCase: UpdateGroupUseCase = UpdateGroupUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(GroupEditViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private var groupId: String? = null

    private val auth = Firebase.auth

    fun loadData(groupId: String?) {
        this.groupId = groupId
        auth.currentUser
            ?.let { user ->
                groupId
                    ?.let { groupId ->
                        getExistingGroupForStart(userId = user.uid, groupId = groupId)
                    }
                    ?: run {
                        _viewState.update { GroupEditViewState(groupInput = emptyGroupInput()) }
                        validateState()
                    }
            }
            ?: run {
                _viewState.update { GroupEditViewState(errorMessage = "No user signed in") }
                Log.e(APP_TAG, "$ERROR_PREFIX in group edit without currentUser present")
            }
    }

    fun updateGroupName(name: String) {
        _viewState.update { it.copy(groupInput = it.groupInput?.copy(name = name)) }
        validateState()
    }

    fun updateGroupCurrency(currency: CurrencyCode) {
        _viewState.update { it.copy(groupInput = it.groupInput?.copy(defaultCurrency = currency)) }
        validateState()
    }

    fun save(navigateAfterwards: (groupId: String) -> Unit) {
        viewModelScope.launch {
            auth.currentUser?.uid?.let { userId ->
                viewState.value.groupInput?.let { groupInput ->
                    validationUseCase.validateGroupInput(groupInput).map { groupInfo ->
                        _viewState.update { it.copy(processing = true) }
                        groupId
                            ?.let { groupId ->
                                updateGroupUseCase.updateGroupInfo(groupInfo.copy(groupId = groupId))
                                    .collect { result ->
                                        result.map { withContext(Dispatchers.Main) { navigateAfterwards(groupId) } }
                                        _viewState.update { it.copy(processing = false) }
                                    }
                            }
                            ?: let {
                                createGroupUseCase.createGroup(userId, groupInfo).collect { result ->
                                    result.map { withContext(Dispatchers.Main) { navigateAfterwards(it) } }
                                    _viewState.update { it.copy(processing = false) }
                                }
                            }
                    }
                }
            }
        }
    }

    private fun validateState() {
        viewState.value.groupInput?.let { groupInput ->
            viewModelScope.launch {
                val newProblems = validationUseCase.validateGroupInput(groupInput)
                _viewState.update {
                    it.copy(
                        inputProblems = newProblems.fold(
                            { problems -> problems }, { EnumSet.noneOf(GroupInfoInputProblem::class.java) }))
                }
            }
        }
    }

    private fun getExistingGroupForStart(userId: String, groupId: String) = viewModelScope.launch {
        getGroupUseCase.getBasicGroupInfo(userId, groupId)
            .collect { existing ->
                if (viewState.value.groupInput == null) {
                    existing.fold({ problem ->
                        _viewState.update { GroupEditViewState(error = problem) }
                    }, { usersGroupInfo ->
                        _viewState.update { it.copy(groupInput = fillFromGroup(usersGroupInfo.group), loading = false) }
                        validateState()
                    })
                }
            }
    }
}
