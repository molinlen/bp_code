package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import android.util.Log
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface LogoutUseCase {
    suspend fun logout(): Flow<ProblemOr<Unit>>
}

class LogoutUseCaseImpl : LogoutUseCase {
    private val auth = Firebase.auth
    override suspend fun logout(): Flow<ProblemOr<Unit>> {
        return flow {
            Log.d(APP_TAG, "Logging out")
            Either.catch {
                if (auth.currentUser != null) {
                    auth.signOut()
                    Log.d(APP_TAG, "Logged out")
                }
                emit(Unit.right())
            }.mapLeft {
                Log.e(APP_TAG, "$ERROR_PREFIX logout : thrown failure", it)
                emit(Problem("Logout failed", it.message, it).left())
            }
        }
    }
}
