package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.transferdetail

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail

data class TransferDetailViewState(
    val transfer: TransferDetail? = null,
    val userParticipantId: String? = null,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
