package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupInfoBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface CreateGroupUseCase {
    suspend fun createGroup(
        userId: String,
        groupData: GroupInfoBasic,
    ): Flow<ProblemOr<String>>
}

class CreateGroupUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : CreateGroupUseCase {
    override suspend fun createGroup(
        userId: String,
        groupData: GroupInfoBasic,
    ): Flow<ProblemOr<String>> = groupRepository.createNewGroup(userId, groupData)
}
