package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.TransferRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.TransferRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface CreateTransferUseCase {
    suspend fun createTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<String>>
}

class CreateTransferUseCaseImpl(
    private val transferRepository: TransferRepository = if (USE_FAKE_DATA) TransferRepositoryFake() else TransferRepositoryImpl(),
) : CreateTransferUseCase {
    override suspend fun createTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<String>> =
        transferRepository.createTransfer(userId = userId, groupId = groupId, transfer = transfer)
}
