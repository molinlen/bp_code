package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.PurchaseDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.PurchaseRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.PurchaseRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface UpdatePurchaseUseCase {
    suspend fun updatePurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<Unit>>
}

class UpdatePurchaseUseCaseImpl(
    private val purchaseRepository: PurchaseRepository = if (USE_FAKE_DATA) PurchaseRepositoryFake() else PurchaseRepositoryImpl(),
) : UpdatePurchaseUseCase {
    override suspend fun updatePurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<Unit>> =
        purchaseRepository.updatePurchase(userId = userId, groupId = groupId, purchase = purchase)
}
