package cz.cvut.fit.molinlen.android.debttracker.ui.theme

import androidx.compose.ui.graphics.Color

val md_theme_light_primary = Color(0xFF106E09)
val md_theme_light_primaryContainer = Color(0xFFD5ECC3)
val md_theme_light_onPrimaryContainer = Color(0xFF002200)
val md_theme_light_secondary = Color(0xFF386568) // "tertiary"
val md_theme_light_secondaryContainer = Color(0xFFBCEBEE)
val md_theme_light_onSecondaryContainer = Color(0xFF002022)
val md_theme_light_errorContainer = Color(0xFFFFDAD6)
val md_theme_light_onErrorContainer = Color(0xFF410002)
val md_theme_light_background = Color(0xFFFCFDF6)
val md_theme_light_onBackground = Color(0xFF1A1C18)

val light_surface_accent_mild = Color(0xFFF1F1F1)
val light_surface_accent_medium = Color(0xFFE7E7E7)
val light_surface_accent_strong = Color(0xFFDBDBDB)
val light_red_content = Color(0xFF80000A)
val light_green_content = Color(0xFF106E09)

val md_theme_dark_primary = Color(0xFF81DB6E)
val md_theme_dark_primaryContainer = Color(0xFF004D00)
val md_theme_dark_onPrimaryContainer = Color(0xFFD5ECC3)
val md_theme_dark_secondary = Color(0xFFA0CFD2) // "tertiary"
val md_theme_dark_secondaryContainer = Color(0xFF1E4D50)
val md_theme_dark_onSecondaryContainer = Color(0xFFBCEBEE)
val md_theme_dark_errorContainer = Color(0xFF93000A)
val md_theme_dark_onErrorContainer = Color(0xFFFFDAD6)
val md_theme_dark_background = Color(0xFF1A1C18)
val md_theme_dark_onBackground = Color(0xFFE2E3DC)

val dark_surface_accent_mild = Color(0xFF292929)
val dark_surface_accent_medium = Color(0xFF313131)
val dark_surface_accent_strong = Color(0xFF393939)
val dark_red_content = Color(0xFFFFB4AB)
val dark_green_content = Color(0xFF96D784)


//val seed = Color(0xFF006600)
