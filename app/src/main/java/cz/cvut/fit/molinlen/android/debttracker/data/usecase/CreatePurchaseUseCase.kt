package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.PurchaseDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.PurchaseRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.PurchaseRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface CreatePurchaseUseCase {
    suspend fun createPurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<String>>
}

class CreatePurchaseUseCaseImpl(
    private val purchaseRepository: PurchaseRepository = if (USE_FAKE_DATA) PurchaseRepositoryFake() else PurchaseRepositoryImpl(),
) : CreatePurchaseUseCase {
    override suspend fun createPurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail
    ): Flow<ProblemOr<String>> =
        purchaseRepository.createPurchase(userId = userId, groupId = groupId, purchase = purchase)
}
