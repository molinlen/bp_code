package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cz.cvut.fit.molinlen.android.debttracker.EditMode
import cz.cvut.fit.molinlen.android.debttracker.R

@Composable
fun MenuItem(
    label: String,
    painter: Painter,
    contentColorBase: Color = MaterialTheme.colors.onBackground,
    enabled: Boolean = true,
    onClick: () -> Unit,
) {
    MenuItem(
        label = label,
        iconImageVector = null,
        iconPainter = painter,
        enabled = enabled,
        contentColorBase = contentColorBase,
        onClick = onClick,
    )
}

@Composable
fun MenuItem(
    label: String,
    imageVector: ImageVector,
    contentColorBase: Color = MaterialTheme.colors.onBackground,
    enabled: Boolean = true,
    onClick: () -> Unit,
) {
    MenuItem(
        label = label,
        iconImageVector = imageVector,
        iconPainter = null,
        enabled = enabled,
        contentColorBase = contentColorBase,
        onClick = onClick,
    )
}

@Composable
private fun MenuItem(
    label: String,
    iconImageVector: ImageVector?,
    iconPainter: Painter?,
    contentColorBase: Color,
    enabled: Boolean,
    onClick: () -> Unit,
) {
    CompositionLocalProvider(
        LocalContentColor provides contentColorBase,
        LocalContentAlpha provides if (enabled) ContentAlpha.high else ContentAlpha.disabled,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 2.dp, horizontal = screenContentPadding)
                .alsoIf(enabled) { it.clickable { onClick() } },
            verticalAlignment = Alignment.CenterVertically,
        ) {
            iconImageVector?.let { // no contentDescription - label should be descriptive enough
                Icon(imageVector = it, contentDescription = null, modifier = Modifier.padding(all = 10.dp))
            }
            iconPainter?.let {
                Icon(painter = it, contentDescription = null, modifier = Modifier.padding(all = 10.dp))
            }
            Text(text = label)
        }
    }
}

@Composable
fun BasicRectangleButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    iconImageVector: ImageVector? = null,
    iconPainter: Painter? = null,
    enabled: Boolean = true,
    buttonColors: ButtonColors = standardButtonColors(),
) {
    Button(onClick = onClick, modifier = modifier.padding(all = 5.dp), enabled = enabled, colors = buttonColors) {
        RectangleButtonContent(
            iconImageVector = iconImageVector,
            iconPainter = iconPainter,
            text = text,
            modifier = textModifier,
        )
    }
}

@Composable
fun BasicRectangleTextButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    textModifier: Modifier = Modifier,
    iconImageVector: ImageVector? = null,
    iconPainter: Painter? = null,
    enabled: Boolean = true,
    buttonColors: ButtonColors = standardTextButtonColors(),
) {
    TextButton(onClick = onClick, modifier = modifier.padding(all = 5.dp), enabled = enabled, colors = buttonColors) {
        RectangleButtonContent(
            iconImageVector = iconImageVector,
            iconPainter = iconPainter,
            text = text,
            modifier = textModifier,
        )
    }
}

@Composable
private fun RectangleButtonContent(
    iconImageVector: ImageVector?,
    iconPainter: Painter?,
    text: String,
    modifier: Modifier,
) {
    iconImageVector?.let {
        Icon(
            imageVector = it,
            contentDescription = null,
            modifier = Modifier.padding(end = 5.dp),
        )
    }
    iconPainter?.let {
        Icon(
            painter = it,
            contentDescription = null,
            modifier = Modifier.padding(end = 5.dp),
        )
    }
    Text(text = text, textAlign = TextAlign.Center, modifier = modifier.padding(5.dp))
}

@Composable
fun BasicFloatingActionButton(
    icon: ImageVector,
    onClick: () -> Unit,
    contentDescription: String,
    modifier: Modifier = Modifier,
) {
    FloatingActionButton(
        onClick = onClick,
        shape = CircleShape,
        modifier = modifier,
    ) {
        Icon(
            imageVector = icon,
            contentDescription = contentDescription,
        )
    }
}

@Composable
fun standardButtonColors(): ButtonColors = ButtonDefaults.buttonColors()

@Composable
fun errorButtonColors(): ButtonColors = ButtonDefaults.buttonColors(
    backgroundColor = MaterialTheme.colors.error,
    contentColor = MaterialTheme.colors.onError,
)

@Composable
fun standardTextButtonColors(): ButtonColors = ButtonDefaults.buttonColors(
    backgroundColor = MaterialTheme.colors.primary.copy(alpha = 0.6f),
    contentColor = MaterialTheme.colors.onPrimary,
)

@Composable
fun alternativeTextButtonColors(): ButtonColors = ButtonDefaults.buttonColors(
    backgroundColor = MaterialTheme.colors.secondary.copy(alpha = 0.6f),
    contentColor = MaterialTheme.colors.onSecondary,
)

@Composable
fun errorTextButtonColors(): ButtonColors = ButtonDefaults.buttonColors(
    backgroundColor = MaterialTheme.colors.error.copy(alpha = 0.6f),
    contentColor = MaterialTheme.colors.onError,
)

@Composable
fun HeaderMenuIcon(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    HeaderIcon(
        imageVector = Icons.Default.Menu,
        contentDescription = stringResource(R.string.menu),
        onClick = onClick,
        modifier = modifier,
    )
}

@Composable
fun HeaderBackIcon(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    HeaderIcon(
        imageVector = Icons.Default.ArrowBack,
        contentDescription = stringResource(R.string.back),
        onClick = onClick,
        modifier = modifier,
    )
}

@Composable
fun HeaderCloseIcon(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    HeaderIcon(
        imageVector = Icons.Default.Close,
        contentDescription = stringResource(R.string.close),
        onClick = onClick,
        modifier = modifier,
    )
}

@Composable
fun HeaderIcon(
    imageVector: ImageVector,
    contentDescription: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.high) {
        Icon(
            imageVector = imageVector,
            contentDescription = contentDescription,
            modifier = modifier
                .clickable(
                    interactionSource = remember { MutableInteractionSource() },
                    indication = rememberRipple(bounded = false),
                    onClick = onClick,
                )
                .padding(horizontal = 5.dp)
        )
    }
}

@Composable
fun SaveAndDiscardButtons(
    mode: EditMode,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    modifier: Modifier = Modifier,
    buttonModifier: Modifier = Modifier,
    saveEnabled: Boolean = true,
    saveText: String? = null,
    discardText: String = stringResource(R.string.discard),
) {
    SaveAndDiscardButtons(
        saveText = saveText ?: when (mode) {
            EditMode.CREATE -> stringResource(R.string.create)
            EditMode.EDIT -> stringResource(R.string.save_changes)
        },
        discardText = discardText,
        onSave = onSave,
        onDiscard = onDiscard,
        saveEnabled = saveEnabled,
        modifier = modifier,
        buttonModifier = buttonModifier,
    )
}

@Composable
fun SaveAndDiscardButtons(
    saveText: String,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    modifier: Modifier = Modifier,
    buttonModifier: Modifier = Modifier,
    saveEnabled: Boolean = true,
    discardText: String = stringResource(R.string.discard),
) {
    Row(
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .padding(top = 20.dp, bottom = 12.dp)
            .fillMaxWidth(),
    ) {
        BasicRectangleButton(
            text = discardText,
            onClick = onDiscard,
            buttonColors = errorButtonColors(),
            modifier = buttonModifier.weight(1f),
        )
        BasicRectangleButton(
            text = saveText,
            onClick = onSave,
            enabled = saveEnabled,
            modifier = buttonModifier.weight(1f),
        )
    }
}
