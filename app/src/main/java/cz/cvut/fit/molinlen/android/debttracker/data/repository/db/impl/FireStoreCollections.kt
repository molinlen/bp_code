package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

object FireStoreCollections {
    const val USERS = "users"

    object UserSubCollections {
        const val GROUPS = "users_groups"
    }

    const val GROUPS = "groups"

    object GroupSubCollections {
        const val PURCHASES = "purchases"
        const val TRANSFERS = "transfers"
    }
}
