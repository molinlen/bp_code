package cz.cvut.fit.molinlen.android.debttracker.data.computation.fake

fun randomId() = java.util.UUID.randomUUID().toString()
