package cz.cvut.fit.molinlen.android.debttracker

const val APP_TAG = "DEBT_TRACKER"

const val ERROR_PREFIX = "ERROR:"

enum class EditMode {
    CREATE,
    EDIT,
    ;

    companion object {
        fun fromId(id: String?): EditMode {
            return if (id == null) {
                CREATE
            } else {
                EDIT
            }
        }
    }
}

