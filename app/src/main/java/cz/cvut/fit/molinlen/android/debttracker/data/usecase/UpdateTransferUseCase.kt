package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.TransferRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.TransferRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface UpdateTransferUseCase {
    suspend fun updateTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<Unit>>
}

class UpdateTransferUseCaseImpl(
    private val transferRepository: TransferRepository = if (USE_FAKE_DATA) TransferRepositoryFake() else TransferRepositoryImpl(),
) : UpdateTransferUseCase {
    override suspend fun updateTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<Unit>> =
        transferRepository.updateTransfer(userId = userId, groupId = groupId, transfer = transfer)
}
