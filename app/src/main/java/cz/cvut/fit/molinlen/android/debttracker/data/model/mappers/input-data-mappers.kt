package cz.cvut.fit.molinlen.android.debttracker.data.model.mappers

import arrow.core.Either
import arrow.core.left
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.mapConversionThrowable
import java.math.BigDecimal

fun TransferDetailInputData.toTransfer(): ProblemOr<TransferDetail> {
    return when {
        amount.value <= BigDecimal.ZERO -> Problem(
            description = "Must have valid amount greater than zero.",
            details = "TransferDetailInputData.toTransfer - amount value [${amount.value}]").left()
        else -> Either.catch {
            TransferDetail(
                transactionId = transactionId ?: "",
                from = from!!,
                to = to!!,
                amount = amount,
                time = time,
                comments = emptyList(),
                note = note?.trim()?.nullIfBlank(),
            )
        }.mapConversionThrowable("conversion from transfer input")
    }
}

fun PurchaseDetailInputData.toPurchase(): ProblemOr<PurchaseDetail> {
    val neededToSplit = neededToSplit(
        between = splitting.shares.count { it.selected && it.explicitAmountValue > BigDecimal.ZERO }
    )
    return when {
        totalAmount.value <= BigDecimal.ZERO -> Problem(
            description = "Must have valid amount greater than zero.",
            details = "PurchaseDetailInputData.toPurchase - amount value [${totalAmount.value}]"
        ).left()
        neededToSplit != null -> Problem(
            description = "Must have valid splitting.",
            details = "PurchaseDetailInputData.toPurchase - needed to split [${neededToSplit}]"
        ).left()
        else -> Either.catch {
            val splittingStrategy = when (splitting.selectedType) {
                SplittingType.EXPLICIT -> TransactionSplittingExplicit(
                    sharers = splitting.shares
                        .filter { it.selected && it.explicitAmountValue > BigDecimal.ZERO }
                        .map {
                            TransactionShareExplicit(participant = it.participant, shareValue = it.explicitAmountValue)
                        }
                )
                SplittingType.PORTION -> TransactionSplittingPortion(
                    sharers = splitting.shares
                        .filter { it.selected && it.portionValue > 0 }
                        .map { TransactionSharePortion(participant = it.participant, shareValue = it.portionValue) }
                )
            }
            PurchaseDetail(
                transactionId = transactionId ?: "",
                name = name?.trim()?.nullIfBlank(),
                payers = payers
                    .filter { it.amountValue > BigDecimal.ZERO }
                    .map {
                        Payer(
                            participant = it.participant,
                            amount = Amount(value = it.amountValue, currency = currency),
                        )
                    },
                splitting = splittingStrategy,
                time = time,
                comments = emptyList(),
                note = note?.trim()?.nullIfBlank(),
            )
        }.mapConversionThrowable("conversion from purchase input")
    }
}
