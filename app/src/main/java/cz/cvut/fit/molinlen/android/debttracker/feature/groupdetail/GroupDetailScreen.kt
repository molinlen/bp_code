package cz.cvut.fit.molinlen.android.debttracker.feature.groupdetail

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import java.math.BigDecimal

@Composable
fun GroupDetailScreen(
    groupId: String,
    navigateAfterBackClick: () -> Unit,
    navigateAfterPurchaseClick: (purchaseId: String) -> Unit,
    navigateAfterTransferClick: (transferId: String) -> Unit,
    navigateAfterAddPurchaseClick: () -> Unit,
    navigateAfterAddTransferClick: () -> Unit,
    navigateAfterInfoClick: () -> Unit,
    navigateAfterMembersClick: () -> Unit,
) {
    val viewModel: GroupDetailViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId)
    }

    GroupDetailScreenImpl(
        userId = viewModel.userId,
        onBackClick = navigateAfterBackClick,
        onPurchaseClick = navigateAfterPurchaseClick,
        onTransferClick = navigateAfterTransferClick,
        onAddPurchaseClick = navigateAfterAddPurchaseClick,
        onAddTransferClick = navigateAfterAddTransferClick,
        onInfoClick = navigateAfterInfoClick,
        onMembersClick = navigateAfterMembersClick,
        viewState = viewModel.viewState.collectAsState().value,
    )
}

enum class GroupDetailBottomSheetMode {
    MENU,
    TRANSACTION_ADD,
}

@OptIn(ExperimentalMaterialApi::class) // for BottomSheet
@Composable
fun GroupDetailScreenImpl(
    userId: String?,
    onBackClick: () -> Unit,
    onPurchaseClick: (purchaseId: String) -> Unit,
    onTransferClick: (transferId: String) -> Unit,
    onAddPurchaseClick: () -> Unit,
    onAddTransferClick: () -> Unit,
    onInfoClick: () -> Unit,
    onMembersClick: () -> Unit,
    viewState: GroupDetailViewState,
) {
    val bottomSheetMenuState = rememberBottomSheetMenuState()
    val bottomSheetMode = remember { mutableStateOf(GroupDetailBottomSheetMode.MENU) }
    val coroutineScope = rememberCoroutineScope()

    BottomSheetMenu(
        sheetState = bottomSheetMenuState,
        coroutineScope = coroutineScope,
        menuContent = {
            GroupDetailBottomSheetContent(
                bottomSheetMode = bottomSheetMode,
                groupParticipants = viewState.usersGroup?.group?.participants ?: emptySet(),
                onInfoClick = onInfoClick,
                onMembersClick = onMembersClick,
                onAddPurchaseClick = onAddPurchaseClick,
                onAddTransferClick = onAddTransferClick,
            )
        },
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                GroupDetailScreenHeader(
                    usersGroupName = viewState.usersGroup?.group?.name ?: stringResource(R.string.group_detail),
                    onBackClick = onBackClick,
                    onMenuClick = {
                        bottomSheetMode.value = GroupDetailBottomSheetMode.MENU
                        toggleMenuState(coroutineScope, bottomSheetMenuState)
                    }
                )
            },
            floatingActionButton = {
                if (viewState.usersGroup?.group?.participants?.isNotEmpty() == true) {
                    AddTransactionButton {
                        bottomSheetMode.value = GroupDetailBottomSheetMode.TRANSACTION_ADD
                        toggleMenuState(coroutineScope, bottomSheetMenuState)
                    }
                }
            },
        ) { paddingValues ->
            when {
                viewState.loading -> LoadingScreen()
                viewState.error != null -> ErrorScreen(error = viewState.error)
                else -> viewState.usersGroup?.let { usersGroup ->
                    GroupDetailScreenContent(
                        usersGroup = usersGroup,
                        userId = userId,
                        paddingValues = paddingValues,
                        onPurchaseClick = onPurchaseClick,
                        onTransferClick = onTransferClick,
                    )
                }
            }
        }
    }
}

@Composable
private fun GroupDetailScreenContent(
    usersGroup: UsersGroupDetail,
    userId: String?,
    paddingValues: PaddingValues,
    onPurchaseClick: (purchaseId: String) -> Unit,
    onTransferClick: (transferId: String) -> Unit,
) {
    when {
        usersGroup.group.participants.isEmpty() ->
            JustPromptColumn(modifier = Modifier.screenContentModifier(paddingValues)) { NoGroupParticipantsPrompt() }
        else ->
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .screenContentModifier(paddingValues),
            ) {
                item { ParticipantStatuses(usersGroup) }
                item {
                    Text(
                        text = stringResource(R.string.group_transaction_history_heading),
                        style = MaterialTheme.typography.h5,
                    )
                }
                items(usersGroup.group.transactions) { transaction ->
                    TransactionCard(
                        transaction = transaction,
                        userId = userId,
                        userParticipantId = usersGroup.userParticipantId,
                        onPurchaseClick = onPurchaseClick,
                        onTransferClick = onTransferClick,
                    )
                }
                when {
                    usersGroup.group.participants.size < 2 -> item { NotEnoughGroupParticipantsPrompt() }
                    usersGroup.group.transactions.isEmpty() -> item { NoGroupTransactionsPrompt() }
                }
                item { Spacer(Modifier.defaultMinSize(minHeight = 75.dp)) } // so that lowest transaction can be pulled to sight
            }
    }
}

@Composable
private fun TransactionCard(
    transaction: Transaction,
    userId: String?,
    userParticipantId: String?,
    onPurchaseClick: (String) -> Unit,
    onTransferClick: (String) -> Unit,
) {
    val backgroundColor = when {
        transaction.comments.any { it.writerUserId == userId } -> MaterialTheme.colors.secondary
        transaction.comments.isNotEmpty() -> AccentColors.surfaceAccentMedium
        else -> MaterialTheme.colors.background
    }
//    val backgroundColor = when { // testing accent colors
//        transaction.comments.any { it.writerUserId == userId } -> AccentColors.surfaceAccentStrong
//        transaction.comments.isNotEmpty() -> AccentColors.surfaceAccentMedium
//        transaction is Transfer -> AccentColors.surfaceAccentMild
//        else -> MaterialTheme.colors.background
//    }
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 5.dp),
        backgroundColor = backgroundColor,
    ) {
        Row(
            modifier = Modifier
                .padding(all = 5.dp)
                .fillMaxWidth()
                .clickable {
                    when (transaction) {
                        is Transfer -> onTransferClick(transaction.transactionId)
                        is Purchase -> onPurchaseClick(transaction.transactionId)
                    }
                },
            horizontalArrangement = Arrangement.Center,
        ) {
            Text(
                text = transaction.time.toShortPrettyStringDate(),
                modifier = Modifier.padding(start = 5.dp),
            )
            Column(
                modifier = Modifier.padding(horizontal = 5.dp),
            ) {
                val textPaddingModifier = Modifier.padding(horizontal = 3.dp)
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Text(
                        text = when (transaction) {
                            is Transfer -> stringResource(R.string.transfer)
                            is Purchase -> transaction.name ?: stringResource(R.string.purchase)
                        },
                        fontStyle = if (transaction is Transfer) FontStyle.Italic else null,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        modifier = textPaddingModifier.weight(1f),
                    )
                    Text(
                        text = transaction.fullAmount.mapAbsToString(),
                        textAlign = TextAlign.End,
                        modifier = textPaddingModifier,
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                    Text(
                        text = cardParticipantListString(
                            prefix = stringResource(R.string.from_participants_prefix),
                            participants = transaction.fromWhom,
                            userParticipantId = userParticipantId,
                        ),
                        style = MaterialTheme.typography.caption,
                        modifier = textPaddingModifier.weight(1f),
                    )
                    Text(
                        text = cardParticipantListString(
                            prefix = stringResource(R.string.for_participants_prefix),
                            participants = transaction.toWho,
                            userParticipantId = userParticipantId,
                        ),
                        style = MaterialTheme.typography.caption,
                        textAlign = TextAlign.End,
                        modifier = textPaddingModifier.weight(1f),
                    )
                }
            }
        }
    }
}

@Composable
private fun cardParticipantListString(
    prefix: String,
    participants: List<Participant>,
    userParticipantId: String?,
): AnnotatedString {
    return buildAnnotatedString {
        append(prefix)
        if (participants.any { it.participantId == userParticipantId }) {
            withStyle(SpanStyle(fontWeight = FontWeight.ExtraBold)) {
                append(stringResource(R.string.you))
            }
            if (participants.size > 1) {
                append(", ")
            }
        }
        append(
            participants
                .filter { it.participantId != userParticipantId }
                .joinToString { it.nickname }
        )
    }
}

@Composable
fun ParticipantStatuses(
    usersGroup: UsersGroupDetail,
) {
    Text(
        text = stringResource(R.string.current_statuses),
        style = MaterialTheme.typography.h5,
        modifier = Modifier.padding(top = 8.dp),
    )
    val userStatus = usersGroup.group.participants
        .firstOrNull { it.participantId == usersGroup.userParticipantId }
    val otherStatuses = usersGroup.group.participants
        .filter { it.participantId != usersGroup.userParticipantId }
        .sortedBy { it.currentStatus.value }
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .padding(all = 10.dp),
    ) {
        ParticipantStatusColumn(
            userParticipantText = userStatus?.let { stringResource(R.string.you_prefix) }
                ?.let { buildAnnotatedString { append(it) } },
            otherParticipantTexts = otherStatuses.map { buildAnnotatedString { append(it.nickname + ":") } },
            horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .weight(weight = 1f, fill = false),
        )
        ParticipantStatusColumn(
            userParticipantText = userStatus?.currentStatus?.let {
                buildAnnotatedString {
                    withStyle(SpanStyle(color = colorFromSign(amount = it))) { append(it.mapToStringWithSign()) }
                }
            },
            otherParticipantTexts = otherStatuses.map {
                buildAnnotatedString {
                    withStyle(SpanStyle(color = colorFromSign(amount = it.currentStatus))) {
                        append(it.currentStatus.mapToStringWithSign())
                    }
                }
            },
            horizontalAlignment = Alignment.End,
            modifier = Modifier.padding(horizontal = 5.dp),
        )
    }
}

@Composable
fun ParticipantStatusColumn(
    userParticipantText: AnnotatedString?,
    otherParticipantTexts: List<AnnotatedString>,
    horizontalAlignment: Alignment.Horizontal,
    modifier: Modifier,
) {
    Column(
        horizontalAlignment = horizontalAlignment,
        modifier = modifier,
    ) {
        userParticipantText?.let {
            Text(
                text = it,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }
        otherParticipantTexts.map {
            Text(
                text = it,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Normal,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
            )
        }
    }
}

@Composable
private fun colorFromSign(amount: Amount): Color {
    return when {
        amount.value < BigDecimal.ZERO -> AccentColors.redContent
        amount.value > BigDecimal.ZERO -> AccentColors.greenContent
        else -> Color.Unspecified
    }
}

@Composable
private fun GroupDetailBottomSheetContent(
    bottomSheetMode: MutableState<GroupDetailBottomSheetMode>,
    groupParticipants: Set<ParticipantWithStatus>,
    onInfoClick: () -> Unit,
    onMembersClick: () -> Unit,
    onAddPurchaseClick: () -> Unit,
    onAddTransferClick: () -> Unit,
) {
    BottomSheetMenuContentColumn(horizontalAlignment = Alignment.Start) {
        when (bottomSheetMode.value) {
            GroupDetailBottomSheetMode.MENU -> MenuContent(
                onInfoClick = onInfoClick,
                onMembersClick = onMembersClick,
            )
            GroupDetailBottomSheetMode.TRANSACTION_ADD -> AddTransactionSheetContent(
                onAddPurchaseClick = when {
                    groupParticipants.isNotEmpty() -> onAddPurchaseClick
                    else -> null
                },
                onAddTransferClick = when {
                    groupParticipants.size >= 2 -> onAddTransferClick
                    else -> null
                },
            )
        }
    }
}

@Composable
private fun MenuContent(
    onInfoClick: () -> Unit,
    onMembersClick: () -> Unit,
) {
    MenuItem(
        label = stringResource(R.string.group_info),
        imageVector = Icons.Default.Info,
        onClick = onInfoClick,
    )
    MenuItem(
        label = stringResource(R.string.members),
        painter = painterResource(R.drawable.people),
        onClick = onMembersClick,
    )
}

@Composable
private fun AddTransactionSheetContent(
    onAddPurchaseClick: (() -> Unit)?,
    onAddTransferClick: (() -> Unit)?,
) {
    Text(
        text = stringResource(R.string.add_transaction_popup_prompt),
        modifier = Modifier.padding(horizontal = screenContentPadding + 3.dp, vertical = 8.dp),
    )
    MenuItem(
        label = stringResource(R.string.add_transfer),
        onClick = onAddTransferClick ?: {},
        painter = painterResource(R.drawable.money),
        enabled = onAddTransferClick != null,
    )
    MenuItem(
        label = stringResource(R.string.add_purchase),
        onClick = onAddPurchaseClick ?: {},
        imageVector = Icons.Default.ShoppingCart,
        enabled = onAddPurchaseClick != null,
    )
}

@Composable
private fun NoGroupTransactionsPrompt() {
    PromptText(text = stringResource(R.string.no_group_transactions_group_detail_prompt), verticalPadding = 80.dp)
}

@Composable
private fun NotEnoughGroupParticipantsPrompt() {
    PromptText(text = stringResource(R.string.few_group_participants_group_detail_prompt), verticalPadding = 40.dp)
}

@Composable
private fun NoGroupParticipantsPrompt() {
    PromptText(text = stringResource(R.string.no_group_participants_group_detail_prompt))
}

@Composable
private fun PromptText(
    text: String,
    verticalPadding: Dp = 0.dp,
) {
    Text(
        text = text,
        fontStyle = FontStyle.Italic,
        textAlign = TextAlign.Center,
        modifier = Modifier.padding(vertical = verticalPadding),
    )
}

@Composable
private fun JustPromptColumn(
    modifier: Modifier,
    content: @Composable () -> Unit,
) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier,
    ) {
        content()
    }
}

@Composable
private fun AddTransactionButton(
    onClick: () -> Unit,
) {
    BasicFloatingActionButton(
        icon = Icons.Default.Add,
        contentDescription = stringResource(R.string.add_transaction),
        onClick = onClick,
    )
}

@Composable
private fun GroupDetailScreenHeader(
    usersGroupName: String,
    onBackClick: () -> Unit,
    onMenuClick: () -> Unit,
) {
    ScreenHeader(
        headingText = usersGroupName,
        leftIcon = { HeaderBackIcon(onClick = onBackClick) },
        rightIcons = listOf {
            HeaderMenuIcon(onClick = onMenuClick)
        },
    )
}
