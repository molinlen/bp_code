package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.purchaseedit

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.left
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitter
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitterImpl
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.mappers.toPurchase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.math.BigDecimal
import java.time.DateTimeException
import java.time.OffsetDateTime

const val MAX_PORTION_SHARE_VALUE = 1000

class PurchaseEditViewModel(
    private val amountSplitter: AmountSplitter = AmountSplitterImpl,
    private val getGroupParticipantsUseCase: GetGroupParticipantsUseCase = GetGroupParticipantsUseCaseImpl(),
    private val getPurchaseDetailUseCase: GetPurchaseDetailUseCase = GetPurchaseDetailUseCaseImpl(),
    private val validateAmountInputUseCase: ValidateAmountInputUseCase = ValidateAmountInputUseCaseImpl(),
    private val validateIntNonNegativeInputUseCase: ValidateIntNonNegativeInputUseCase = ValidateIntNonNegativeInputUseCaseImpl(),
    private val createPurchaseUseCase: CreatePurchaseUseCase = CreatePurchaseUseCaseImpl(),
    private val updatePurchaseUseCase: UpdatePurchaseUseCase = UpdatePurchaseUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(PurchaseEditViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String
    private var purchaseId: String? = null

    private val auth = Firebase.auth


    fun loadData(groupId: String, purchaseId: String?) {
        this.groupId = groupId
        this.purchaseId = purchaseId
        auth.currentUser
            ?.let { user ->
                getGroupsParticipants(user.uid)
                purchaseId
                    ?.let { purchaseId ->
                        getExistingPurchaseForStart(userId = user.uid, purchaseId = purchaseId)
                    }
                    ?: createEmptyPurchase()
            }
            ?: run {
                _viewState.update { PurchaseEditViewState(errorMessage = "No user signed in") }
                Log.e(APP_TAG, "$ERROR_PREFIX in purchase edit without currentUser present")
            }
    }

    fun updateName(name: String?) {
        _viewState.update { it.copy(purchase = it.purchase?.copy(name = name), saveError = null) }
    }

    fun updateNote(note: String?) {
        _viewState.update { it.copy(purchase = it.purchase?.copy(note = note), saveError = null) }
    }

    fun updateCurrency(currency: CurrencyCode) {
        _viewState.update { it.copy(purchase = it.purchase?.copy(currency = currency), saveError = null) }
    }

    fun updateDate(year: Int, month: Int, dayOfMonth: Int) {
        try {
            Log.d(APP_TAG, "Update date request: y [$year] m [$month] d [$dayOfMonth] (in purchase edit)")
            // for now setting midday time - todo update if user is able to set time also
            val dateTime = OffsetDateTime.of(year, month, dayOfMonth, 12, 0, 0, 0, OffsetDateTime.now().offset)
            _viewState.update { it.copy(purchase = it.purchase?.copy(time = dateTime), saveError = null) }
        } catch (e: DateTimeException) {
            Log.w(APP_TAG, "Invalid update date request: y [$year] m [$month] d [$dayOfMonth] (in purchase edit)", e)
        } catch (e: Exception) {
            Log.w(APP_TAG, "Failed update date request: y [$year] m [$month] d [$dayOfMonth] (in purchase edit)", e)
        }
    }

    fun updatePayer(old: PayerInputData, updated: PayerInputData) {
        _viewState.update { state ->
            val payers = state.purchase?.payers?.map { payer ->
                if (payer == old) {
                    val amountInput = validateAmountInputUseCase.validateAmountInput(
                        currentInputString = updated.amountValueString,
                        previousInputString = old.amountValueString,
                        currency = state.purchase.currency,
                    )
                    updated.copy(
                        amountValue = amountInput.resultingAmount?.value
                            ?: when (amountInput.resultingInputString) {
                                old.amountValueString -> old.amountValue
                                else -> BigDecimal.ZERO
                            },
                        amountValueString = amountInput.resultingInputString,
                    )
                } else payer
            }
            val purchase = state.purchase?.copy(payers = payers ?: emptyList())
            val splitting = when (purchase?.totalAmount) {
                state.purchase?.totalAmount -> purchase?.splitting
                else -> purchase?.let { updatedSplitting(it) }
            }
            state.copy(
                purchase = splitting?.let { purchase?.copy(splitting = it) ?: purchase },
                saveError = null,
            )
        }
    }

    fun addPayer(payer: Participant) {
        _viewState.update {
            val payerData = PayerInputData(participant = payer, amountValue = BigDecimal.ZERO, amountValueString = "")
            it.copy(
                purchase = it.purchase?.copy(payers = it.purchase.payers + payerData)?.withUpdatedSplitting(),
                saveError = null,
            )
        }
    }

    fun removePayer(toRemove: Participant) {
        _viewState.update {
            it.copy(
                purchase = it.purchase?.copy(
                    payers = it.purchase.payers.filter { payer -> payer.participant.participantId != toRemove.participantId }
                )?.withUpdatedSplitting(),
                saveError = null,
            )
        }
    }

    fun updateSplittingType(selected: SplittingType) {
        // in the future, recalculation may happen here
        _viewState.update {
            it.copy(
                purchase = it.purchase?.copy(
                    splitting = it.purchase.splitting.copy(selectedType = selected),
                ),
            )
        }
    }

    fun updateSharer(old: TransactionParticipantBasicInputData, updated: TransactionParticipantBasicInputData) {
        _viewState.update { state ->
            state.copy(
                purchase = state.purchase?.copy(
                    splitting = state.purchase.splitting.copy(
                        shares = state.purchase.splitting.shares.map { sharer ->
                            if (sharer == old) {
                                val explicitInput = validateAmountInputUseCase.validateAmountInput(
                                    currentInputString = updated.explicitAmountValueString,
                                    previousInputString = old.explicitAmountValueString,
                                    currency = state.purchase.currency,
                                )
                                val portionInput = validateIntNonNegativeInputUseCase.validateNonNegativeIntInput(
                                    currentInputString = updated.portionValueString,
                                    previousInputString = old.portionValueString,
                                    maxAllowedInput = MAX_PORTION_SHARE_VALUE,
                                )
                                updated.copy(
                                    selected = updated.selected,
                                    explicitAmountValue = explicitInput.resultingAmount?.value
                                        ?: when (explicitInput.resultingInputString) {
                                            old.explicitAmountValueString -> old.explicitAmountValue
                                            else -> BigDecimal.ZERO
                                        },
                                    explicitAmountValueString = explicitInput.resultingInputString,
                                    portionValue = portionInput.resultingValue
                                        ?: when (portionInput.resultingInputString) {
                                            old.portionValueString -> old.portionValue
                                            else -> 0
                                        },
                                    portionValueString = portionInput.resultingInputString,
                                )
                            } else sharer
                        },
                    ),
                )?.withUpdatedSplitting(),
                saveError = null,
            )
        }
    }

    fun markAllAsSharersAsSelected(selected: Boolean) {
        _viewState.update {
            it.copy(
                purchase = it.purchase?.copy(
                    splitting = it.purchase.splitting.copy(
                        shares = it.purchase.splitting.shares.map { sharer -> sharer.copy(selected = selected) }
                    )
                )?.withUpdatedSplitting(),
            )
        }
    }

    fun splitAmountEqually() {
        _viewState.update {
            val purchase = when (it.purchase?.splitting?.selectedType) {
                SplittingType.EXPLICIT -> it.purchase.copy(
                    splitting = equalExplicitSplitting(it.purchase),
                )
                SplittingType.PORTION -> it.purchase.copy(
                    splitting = equalPortionSplitting(it.purchase),
                ).withUpdatedSplitting()
                null -> null
            }
            it.copy(
                purchase = purchase,
                saveError = null,
            )
        }
    }

    private fun equalExplicitSplitting(purchase: PurchaseDetailInputData): TransactionSplittingInput {
        val eachAmount = purchase.totalAmount
            .let { total -> amountSplitter.splitAmount(total, purchase.splitting.shares.count { it.selected }) }
            .fold({ null }, { it })
        return eachAmount
            ?.let { each ->
                purchase.splitting.copy(
                    shares = purchase.splitting.shares.map {
                        when {
                            it.selected -> it.copy(
                                explicitAmountValue = each.amount.value,
                                explicitAmountValueString = each.amount.value.toPlainString()
                            )
                            else -> it
                        }
                    }
                )
            }
            ?: purchase.splitting
    }

    private fun equalPortionSplitting(purchase: PurchaseDetailInputData): TransactionSplittingInput {
        val maxShare = maxOf(purchase.splitting.shares.filter { it.selected }.maxOf { it.portionValue }, 1)
        return purchase.splitting.copy(
            shares = purchase.splitting.shares.map {
                when {
                    it.selected -> it.copy(
                        portionValue = maxShare,
                        portionValueString = maxShare.toString(),
                    )
                    else -> it
                }
            }
        )
    }

    private fun PurchaseDetailInputData.withUpdatedSplitting(): PurchaseDetailInputData {
        return copy(splitting = updatedSplitting(this))
    }

    private fun updatedSplitting(purchase: PurchaseDetailInputData): TransactionSplittingInput {
        val portionsResultMap = amountSplitter.splitAmountToPortions(
            amount = purchase.totalAmount,
            portions = purchase.splitting.shares.filter { it.selected }.map { it.portionValue },
        ).orNull()?.portionResults
        return purchase.splitting.copy(
            shares = purchase.splitting.shares.map {
                it.copy(
                    portionShareResult = portionsResultMap?.get(it.portionValue)?.value ?: BigDecimal.ZERO,
                )
            }
        )
    }

    fun canSave(): Boolean {
        return viewState.value.purchase?.let { purchaseInput ->
            savableInput(purchaseInput).isRight()
        } ?: false
    }

    fun save(navigateAfterwards: () -> Unit) {
        viewState.value.purchase?.let { purchaseInput ->
            viewModelScope.launch {
                savableInput(purchaseInput).fold(
                    { inputProblem ->
                        _viewState.update { it.copy(saveError = inputProblem) }
                    }, { purchase ->
                        _viewState.update { it.copy(processing = true, saveError = null) }
                        val resultFlow = purchaseId
                            ?.let {
                                updatePurchaseUseCase.updatePurchase(
                                    userId = auth.currentUser?.uid!!, groupId = groupId, purchase = purchase,
                                )
                            }
                            ?: createPurchaseUseCase.createPurchase(
                                userId = auth.currentUser?.uid!!, groupId = groupId, purchase = purchase,
                            )
                        resultFlow.collect { result ->
                            result.map {
                                withContext(Dispatchers.Main) { navigateAfterwards() }
                            }
                            _viewState.update {
                                it.copy(
                                    processing = false,
                                    saveError = result.fold({ problem -> problem }, { null }),
                                )
                            }
                        }
                    }
                )
            }
        }
    }

    private fun savableInput(input: PurchaseDetailInputData): ProblemOr<PurchaseDetail> {
        val purchaseInput = input.copy(
            splitting = input.splitting.copy(
                shares = input.splitting.shares.filter {
                    it.selected && when {
                        input.splitting.selectedType == SplittingType.EXPLICIT && it.explicitAmountValue > BigDecimal.ZERO -> true
                        input.splitting.selectedType == SplittingType.PORTION && it.portionValue > 0 -> true
                        else -> false
                    }
                },
            ),
        )
        if (
            purchaseInput.splitting.shares.isEmpty()
            || purchaseInput.totalAmount.value <= BigDecimal.ZERO
            || purchaseInput.neededToSplit(purchaseInput.splitting.shares.count()) != null
        ) {
            return Problem("Invalid purchase input").left()
        }
        return purchaseInput.toPurchase()
    }

    private fun createEmptyPurchase() {
        viewModelScope.launch {
            _viewState.collect { state ->
                state.groupCurrency?.let { groupCurrency ->
                    if (state.purchase == null) {
                        _viewState.update {
                            it.copy(
                                purchase = emptyPurchaseDetailInput(
                                    currency = groupCurrency,
                                    participants = it.participants,
                                    eachAmount = BigDecimal.ZERO,
                                ),
                                loading = false,
                            )
                        }
                        this.coroutineContext.job.cancel()
                    }
                }
            }
        }
    }

    private fun getGroupsParticipants(userId: String) = viewModelScope.launch {
        getGroupParticipantsUseCase.getUsersGroupParticipants(userId = userId, groupId = groupId).collect { result ->
            result.fold({ problem ->
                _viewState.update { PurchaseEditViewState(error = problem) }
            }, { usersGroupParticipants ->
                _viewState.update {
                    it.copy(
                        participants = usersGroupParticipants.groupParticipants,
                        usersParticipantId = usersGroupParticipants.userParticipantId,
                        groupCurrency = usersGroupParticipants.groupCurrency,
                    )
                }
            })
        }
    }

    private fun getExistingPurchaseForStart(userId: String, purchaseId: String) = viewModelScope.launch {
        getPurchaseDetailUseCase.getPurchaseDetail(userId = userId, groupId = groupId, purchaseId = purchaseId)
            .collect { existing ->
                Log.d(APP_TAG, "Purchase edit getExistingPurchaseForStart, purchase [${viewState.value.purchase}]")
                if (viewState.value.purchase == null && viewState.value.participants.isNotEmpty()) {
                    existing.fold(
                        { problem -> _viewState.update { PurchaseEditViewState(error = problem) } },
                        { purchase ->
                            _viewState.update { state ->
                                state.copy(
                                    purchase = fillPurchaseDetailInput(purchase).withUpdatedSplitting(),
                                    loading = false,
                                ).withUpdatedNotOwingParticipants(currency = purchase.fullAmount.currency)
                            }
                            this.coroutineContext.job.cancel()
                        }
                    )
                }
            }
    }
}
