package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInfo
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInput
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputProblem
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputValidator
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputValidatorImpl
import java.util.*

interface ValidateRegistrationInputUseCase {
    suspend fun validateRegistrationInput(
        registrationInput: RegistrationInput,
    ): Either<EnumSet<RegistrationInputProblem>, RegistrationInfo>
}

class ValidateRegistrationInputUseCaseImpl(
    private val registrationInputValidator: RegistrationInputValidator = RegistrationInputValidatorImpl(),
) : ValidateRegistrationInputUseCase {
    override suspend fun validateRegistrationInput(
        registrationInput: RegistrationInput,
    ): Either<EnumSet<RegistrationInputProblem>, RegistrationInfo> {
        return registrationInputValidator.validateRegistrationInput(registrationInput = registrationInput)
    }
}
