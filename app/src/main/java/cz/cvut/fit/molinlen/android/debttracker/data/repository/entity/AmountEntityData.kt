package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode

data class AmountEntityData(
    var value: String? = null,
    var currency: CurrencyCode? = null,
)
