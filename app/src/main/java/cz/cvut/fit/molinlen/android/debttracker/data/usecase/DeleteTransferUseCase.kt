package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.TransferRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.TransferRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface DeleteTransferUseCase {
    suspend fun deleteTransfer(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<Unit>>
}

class DeleteTransferUseCaseImpl(
    private val transferRepository: TransferRepository = if (USE_FAKE_DATA) TransferRepositoryFake() else TransferRepositoryImpl(),
) : DeleteTransferUseCase {
    override suspend fun deleteTransfer(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<Unit>> =
        transferRepository.deleteTransfer(userId = userId, groupId = groupId, transferId = transferId)
}
