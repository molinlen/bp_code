package cz.cvut.fit.molinlen.android.debttracker.data.model

import arrow.core.Either
import arrow.core.left

data class Problem(
    val description: String,
    val details: String? = null,
    val cause: Throwable? = null,
)

typealias ProblemOr<T> = Either<Problem, T>

//inline fun <E, I, O> Either<E, I>.mapIfRight(transform: (I) -> Either<E, O>): Either<E, O> {
inline fun <I, O> ProblemOr<I>.mapIfRight(transform: (I) -> ProblemOr<O>): ProblemOr<O> {
    return this.fold(
        { problem -> problem.left() },
        { result -> transform(result) }
    )
}
