package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.transferdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TransferDetailViewModel(
    private val getTransferDetailUseCase: GetTransferDetailUseCase = GetTransferDetailUseCaseImpl(),
    private val getUserParticipantIdUseCase: GetUserParticipantIdUseCase = GetUserParticipantIdUseCaseImpl(),
    private val deleteTransferUseCase: DeleteTransferUseCase = DeleteTransferUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(TransferDetailViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String
    private lateinit var transferId: String

    private val auth = Firebase.auth
    val userId: String? = auth.currentUser?.uid

    fun loadData(groupId: String, transferId: String) {
        this.groupId = groupId
        this.transferId = transferId
        auth.currentUser
            ?.let { user ->
                viewModelScope.launch {
                    getTransferDetailUseCase.getTransferDetail(
                        userId = user.uid,
                        groupId = groupId,
                        transferId = transferId,
                    ).collect { result ->
                        result.fold({ problem ->
                            Log.e(APP_TAG,
                                "$ERROR_PREFIX could not get transfer detail user [${user.uid}], group [$groupId], transfer [$transferId]: $problem"
                            )
                            _viewState.update { TransferDetailViewState(error = problem) }
                        }, { transfer ->
                            _viewState.update { it.copy(transfer = transfer, loading = false) }
                        })
                    }
                }
                viewModelScope.launch {
                    getUserParticipantIdUseCase.getUserParticipantId(
                        userId = user.uid, groupId = groupId,
                    ).collect { result ->
                        result.fold({ problem ->
                            Log.e(APP_TAG,
                                "$ERROR_PREFIX could not get user [${user.uid}] participant id, group [$groupId]: $problem")
                        }, { userParticipantId ->
                            _viewState.update { it.copy(userParticipantId = userParticipantId) }
                        })
                    }
                }
            }
            ?: run {
                _viewState.update { TransferDetailViewState(errorMessage = "No user signed in") }
                Log.e(APP_TAG, "$ERROR_PREFIX in transfer detail without currentUser present")
            }
    }

    fun deleteTransfer(navigateAfterwards: () -> Unit) {
        viewModelScope.launch {
            auth.currentUser?.let { user ->
                deleteTransferUseCase.deleteTransfer(
                    userId = user.uid,
                    groupId = groupId,
                    transferId = transferId,
                ).collect {
                    it.map {
                        withContext(Dispatchers.Main) {
                            navigateAfterwards()
                        }
                    }
                }
            }
        }
    }
}
