package cz.cvut.fit.molinlen.android.debttracker.data.validation

import cz.cvut.fit.molinlen.android.debttracker.data.model.AmountInputValidationResult
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.createAmount
import java.math.BigDecimal

interface AmountInputValidator {
    fun validateAmountInput(
        currentInputString: String,
        previousInputString: String,
        currency: CurrencyCode,
    ): AmountInputValidationResult
}

class AmountInputValidatorImpl : AmountInputValidator {
    override fun validateAmountInput(
        currentInputString: String,
        previousInputString: String,
        currency: CurrencyCode,
    ): AmountInputValidationResult {
        return try {
            val amountValue = BigDecimal(currentInputString)
            val newAmount = createAmount(amountValue, currency)
            if (newAmount.value >= BigDecimal.ZERO) {
                val newValueString = when (newAmount.value.abs() < BigDecimal(currentInputString).abs()) {
                    true -> newAmount.value.toPlainString() // user added too many digits behind decimal point (will not catch 0s but who cares)
                    else -> currentInputString
                }
                AmountInputValidationResult(
                    resultingAmount = newAmount,
                    resultingInputString = newValueString,
                )
            } else {
                AmountInputValidationResult(
                    resultingAmount = null,
                    resultingInputString = previousInputString,
                )
            }
        } catch (e: Exception) { // invalid number
            if (currentInputString.isBlank()) {
                AmountInputValidationResult(
                    resultingAmount = null,
                    resultingInputString = "",
                )
            } else {
                AmountInputValidationResult(
                    resultingAmount = null,
                    resultingInputString = previousInputString,
                )
            }
        }
    }
}
