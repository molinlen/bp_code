package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem


@Composable
fun ErrorScreen(
    error: Problem,
    modifier: Modifier = Modifier,
    standardLeadingErrorMessage: Boolean = true,
    fillMaxSize: Boolean = true,
    standardPadding: Boolean = true,
    colorBackground: Boolean = true,
) {
    ErrorBox(
        errorMessage = error.description,
        errorDetails = error.details,
        errorCause = error.cause,
        modifier = modifier,
        standardLeadingErrorMessage = standardLeadingErrorMessage,
        fillMaxSize = fillMaxSize,
        standardPadding = standardPadding,
        colorBackground = colorBackground,
    )
}

@Composable
fun ErrorBox(
    errorMessage: String,
    modifier: Modifier = Modifier,
    errorDetails: String? = null,
    errorCause: Throwable? = null,
    standardLeadingErrorMessage: Boolean = true,
    fillMaxSize: Boolean = true,
    standardPadding: Boolean = true,
    colorBackground: Boolean = true,
) {
    Box(
        modifier = modifier
            .alsoIf(fillMaxSize) { it.fillMaxSize() }
            .alsoIf(standardPadding) { it.padding(screenContentPadding) }
            .alsoIf(colorBackground) { it.background(MaterialTheme.colors.error) },
        contentAlignment = Alignment.Center,
    ) {
        BasicSelectionContainer {
            Text(
                text = buildErrorMessage(
                    standardLeadingErrorMessage = standardLeadingErrorMessage,
                    errorMessage = errorMessage,
                    errorDetails = errorDetails,
                    errorCause = errorCause,
                ),
                textAlign = TextAlign.Center,
                color = MaterialTheme.colors.onError,
                modifier = Modifier.padding(20.dp)
            )
        }
    }
}

@Composable
fun buildErrorMessage(
    errorMessage: String,
    standardLeadingErrorMessage: Boolean = false,
    errorDetails: String? = null,
    errorCause: Throwable? = null,
): AnnotatedString = buildAnnotatedString {
    if (standardLeadingErrorMessage) {
        withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
            append(stringResource(R.string.general_error_message) + "\n")
        }
    }
    append(errorMessage)
    if (errorDetails != null || errorCause != null) {
        withStyle(SpanStyle(fontSize = MaterialTheme.typography.subtitle2.fontSize)) {
            append("\n") // to separate main message from details
        }
    }
    errorDetails?.let { errorDetails ->
        withStyle(SpanStyle(fontStyle = FontStyle.Italic, fontSize = MaterialTheme.typography.subtitle2.fontSize)) {
            append("\n" + errorDetails)
        }
    }
    errorCause?.let { errorCause ->
        append("\n" + "Cause: ")
        withStyle(SpanStyle(fontFamily = FontFamily.Monospace, fontSize = MaterialTheme.typography.caption.fontSize)) {
            append(errorCause.javaClass.name)
        }
    }
}

@Composable
fun ErrorCard(
    text: String,
    modifier: Modifier = Modifier,
    textAlign: TextAlign = TextAlign.Center,
) {
    ErrorCard(text = buildAnnotatedString { append(text) }, modifier = modifier, textAlign = textAlign)
}

@Composable
fun ErrorCard(
    text: AnnotatedString,
    modifier: Modifier = Modifier,
    textAlign: TextAlign = TextAlign.Center,
) {
    Card(
        backgroundColor = MaterialTheme.colors.error,
        elevation = 0.dp,
        modifier = modifier.padding(vertical = 1.dp)
    ) {
        BasicSelectionContainer {
            Text(
                text = text,
                textAlign = textAlign,
                modifier = Modifier.padding(vertical = 10.dp, horizontal = 30.dp),
            )
        }
    }
}
