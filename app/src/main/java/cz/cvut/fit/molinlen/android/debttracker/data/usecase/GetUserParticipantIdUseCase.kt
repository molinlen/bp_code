package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface GetUserParticipantIdUseCase {
    suspend fun getUserParticipantId(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<String?>>
}

class GetUserParticipantIdUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : GetUserParticipantIdUseCase {
    override suspend fun getUserParticipantId(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<String?>> = groupRepository.getUsersGroupParticipantId(userId = userId, groupId = groupId)
}
