package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.transferedit

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.time.OffsetDateTime

data class TransferEditViewState(
    val transfer: TransferDetailInputData? = null,
    val participants: List<Participant> = emptyList(),
    val groupCurrency: CurrencyCode? = null,
    val usersParticipantId: String? = null,
    val saveError: Problem? = null,
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}

fun emptyTransferDetailInput(currency: CurrencyCode) = TransferDetailInputData(
    transactionId = null,
    from = null,
    to = null,
    amountValueString = "",
    amount = amountZero(currency),
    time = OffsetDateTime.now(),
    note = null,
)

fun fillTransferDetailInput(transfer: TransferDetail) = TransferDetailInputData(
    transactionId = transfer.transactionId,
    from = transfer.from,
    to = transfer.to,
    amountValueString = transfer.amount.value.toPlainString(),
    amount = transfer.amount,
    time = transfer.time,
    note = transfer.note?.nullIfBlank(),
)
