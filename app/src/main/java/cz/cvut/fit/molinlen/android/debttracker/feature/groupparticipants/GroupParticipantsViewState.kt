package cz.cvut.fit.molinlen.android.debttracker.feature.groupparticipants

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupParticipants

data class GroupParticipantsViewState(
    val groupParticipants: UsersGroupParticipants? = null,
    val currentlyAddingParticipantNickname: String = "",
    val participantAdditionError: Problem? = null,
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
