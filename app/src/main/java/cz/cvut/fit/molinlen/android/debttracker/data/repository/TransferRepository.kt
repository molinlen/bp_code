package cz.cvut.fit.molinlen.android.debttracker.data.repository

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import kotlinx.coroutines.flow.Flow

interface TransferRepository {
    suspend fun getTransferDetail(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<TransferDetail>>

    suspend fun createTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<String>>

    suspend fun updateTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<Unit>>

    suspend fun deleteTransfer(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<Unit>>
}
