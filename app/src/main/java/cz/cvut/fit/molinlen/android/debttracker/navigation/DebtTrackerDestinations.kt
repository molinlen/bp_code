package cz.cvut.fit.molinlen.android.debttracker.navigation

enum class DebtTrackerDestinations(val route: String) {
    LOGIN("login"),
    REGISTER("register"),
    USER_DETAIL("user_detail"),
    USER_EDIT("user_edit"),
    GROUP_LIST("group_list"),
    GROUP_DETAIL("group_detail"),
    GROUP_INFO("group_info"),
    GROUP_EDIT("group_edit"),
    GROUP_PARTICIPANTS("group_participants"),
    PURCHASE_DETAIL("purchase_detail"),
    TRANSFER_DETAIL("transfer_detail"),
    PURCHASE_EDIT("purchase_edit"),
    TRANSFER_EDIT("transfer_edit"),
}
