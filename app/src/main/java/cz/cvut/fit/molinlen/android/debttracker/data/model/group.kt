package cz.cvut.fit.molinlen.android.debttracker.data.model

import java.util.UUID.randomUUID

interface Group {
    val groupId: String
    val name: String
    val defaultCurrency: CurrencyCode
}

data class GroupInfoBasic(
    override val groupId: String,
    override val name: String,
    override val defaultCurrency: CurrencyCode,
) : Group

data class GroupPreview(
    override val groupId: String = randomUUID().toString(), //todo rm prefill
    override val name: String,
    override val defaultCurrency: CurrencyCode,
    val currentOwnStatus: Amount,
) : Group

data class GroupBasic(
    override val groupId: String,
    override val name: String,
    override val defaultCurrency: CurrencyCode,
    val transactions: List<Transaction>,
    val participants: Set<Participant>,
) : Group

data class GroupDetail(
    override val groupId: String,
    override val name: String,
    override val defaultCurrency: CurrencyCode,
    val transactions: List<Transaction>,
    val participants: Set<ParticipantWithStatus>,
) : Group

data class GroupInputData(
    val groupId: String?,
    val name: String,
    val defaultCurrency: CurrencyCode?,
)


interface Participant {
    val participantId: String
    val nickname: String
}

data class ParticipantBasic(
    override val participantId: String,
    override val nickname: String,
) : Participant

data class ParticipantWithStatus(
    override val participantId: String,
    override val nickname: String,
    val currentStatus: Amount,
) : Participant

data class UsersGroupParticipants(
    val groupParticipants: Set<Participant>,
    val userParticipantId: String?,
    val groupCurrency: CurrencyCode,
)


interface UsersGroup {
    val group: Group
    val userParticipantId: String?
}

data class UsersGroupInfoBasic(
    override val group: GroupInfoBasic,
    override val userParticipantId: String?,
) : UsersGroup

data class UsersGroupPreview(
    override val group: GroupPreview,
    override val userParticipantId: String?,
) : UsersGroup

data class UsersGroupBasic(
    override val group: GroupBasic,
    override val userParticipantId: String?,
) : UsersGroup

data class UsersGroupDetail(
    override val group: GroupDetail,
    override val userParticipantId: String?,
) : UsersGroup
