package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface LeaveGroupUseCase {
    suspend fun leaveGroup(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<Unit>>
}

class LeaveGroupUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : LeaveGroupUseCase {
    override suspend fun leaveGroup(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<Unit>> = groupRepository.leaveGroup(
        userId = userId,
        groupId = groupId,
    )
}
