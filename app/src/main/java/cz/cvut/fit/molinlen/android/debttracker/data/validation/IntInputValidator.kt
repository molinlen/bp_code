package cz.cvut.fit.molinlen.android.debttracker.data.validation

import cz.cvut.fit.molinlen.android.debttracker.data.model.IntInputValidationResult

interface IntInputValidator {
    fun validateNonNegativeIntInput(
        currentInputString: String,
        previousInputString: String,
        maxAllowedInput: Int?,
    ): IntInputValidationResult
}

class IntInputValidatorImpl : IntInputValidator {
    override fun validateNonNegativeIntInput(
        currentInputString: String,
        previousInputString: String,
        maxAllowedInput: Int?,
    ): IntInputValidationResult {
        return try {
            val newValue = currentInputString.toInt()
            when {
                newValue >= 0 && (maxAllowedInput == null || newValue <= maxAllowedInput) ->
                    IntInputValidationResult(
                        resultingValue = newValue,
                        resultingInputString = newValue.toString(),
                    )
                else ->
                    IntInputValidationResult(
                        resultingValue = null,
                        resultingInputString = previousInputString,
                    )
            }
        } catch (e: Exception) { // invalid number
            IntInputValidationResult(
                resultingValue = null,
                resultingInputString = when {
                    currentInputString.isBlank() -> "" // strip unnecessary whitespace
                    else -> previousInputString
                },
            )
        }
    }
}
