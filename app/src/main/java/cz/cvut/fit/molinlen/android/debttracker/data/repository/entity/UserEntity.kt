package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode

data class UserEntity(
    var userId: String? = null,
    var data: UserEntityData? = null,
    var userGroupIds: List<UsersGroupEntity> = emptyList(),
)

data class UserEntityData(
    var primaryCurrency: CurrencyCode? = null,
    // nick,...
)

data class UserEntityUpdateData(
    var primaryCurrency: CurrencyCode,
)

data class UsersGroupEntity(
    var groupId: String? = null,
    var data: UsersGroupEntityData? = null,
)

data class UsersGroupEntityData(
    var userParticipantId: String? = null,
    var groupId: String? = null,
)
