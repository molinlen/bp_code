package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.purchasedetail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PurchaseDetailViewModel(
    private val getPurchaseDetailUseCase: GetPurchaseDetailUseCase = GetPurchaseDetailUseCaseImpl(),
    private val getUserParticipantIdUseCase: GetUserParticipantIdUseCase = GetUserParticipantIdUseCaseImpl(),
    private val deletePurchaseUseCase: DeletePurchaseUseCase = DeletePurchaseUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(PurchaseDetailViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String
    private lateinit var purchaseId: String

    private val auth = Firebase.auth
    val userId: String? = auth.currentUser?.uid

    fun loadData(groupId: String, purchaseId: String) {
        this.groupId = groupId
        this.purchaseId = purchaseId
        auth.currentUser
            ?.let { user ->
                viewModelScope.launch {
                    getPurchaseDetailUseCase.getPurchaseDetail(
                        userId = user.uid,
                        groupId = groupId,
                        purchaseId = purchaseId,
                    ).collect {
                        it.fold({ problem ->
                            Log.e(APP_TAG,
                                "$ERROR_PREFIX could not get purchase detail user [${user.uid}], group [$groupId], purchase [$purchaseId]: $problem"
                            )
                            _viewState.update { PurchaseDetailViewState(error = problem) }
                        }, { purchase ->
                            _viewState.update { PurchaseDetailViewState(purchase = purchase) }
                        })
                    }
                }
                viewModelScope.launch {
                    getUserParticipantIdUseCase.getUserParticipantId(
                        userId = user.uid, groupId = groupId,
                    ).collect { result ->
                        result.fold({ problem ->
                            Log.e(APP_TAG,
                                "$ERROR_PREFIX could not get user [${user.uid}] participant id, group [$groupId]: $problem")
                        }, { userParticipantId ->
                            _viewState.update { it.copy(userParticipantId = userParticipantId) }
                        })
                    }
                }
            }
            ?: run {
                _viewState.update { PurchaseDetailViewState(errorMessage = "No user signed in") }
                Log.e(APP_TAG, "$ERROR_PREFIX in purchase detail without currentUser present")
            }
    }

    fun deletePurchase(navigateAfterwards: () -> Unit) {
        viewModelScope.launch {
            auth.currentUser?.let { user ->
                deletePurchaseUseCase.deletePurchase(
                    userId = user.uid,
                    groupId = groupId,
                    purchaseId = purchaseId,
                ).collect {
                    it.map {
                        withContext(Dispatchers.Main) {
                            navigateAfterwards()
                        }
                    }
                }
            }
        }
    }
}
