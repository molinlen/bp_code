package cz.cvut.fit.molinlen.android.debttracker.data.computation.fake

import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.computation.OverallStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.model.*

class OverallStatusCalculatorFake : OverallStatusCalculator {

    override suspend fun calculateOverallUserStatusFromPreview(
        usersGroups: List<UsersGroupPreview>,
        currency: CurrencyCode,
    ): ProblemOr<GlobalOwedSumsOverview> {
        return GlobalOwedSumsOverview(
            sumOwed = amountFromDouble(1500.0, currency),
            sumCollectable = amountFromDouble(1000.0, currency),
        ).right()
    }
}
