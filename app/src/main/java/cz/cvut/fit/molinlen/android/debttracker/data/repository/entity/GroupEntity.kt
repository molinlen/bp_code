package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode

data class GroupEntityData(
    var name: String? = null,
    var defaultCurrency: CurrencyCode? = null,
    var participants: Map<String, ParticipantEntityData> = emptyMap(),
)

data class GroupEntityInfoUpdateData( // to update parameters without overwriting participants
    var name: String? = null,
    var defaultCurrency: CurrencyCode? = null,
)

data class GroupEntity(
    var groupId: String? = null,
    var group: GroupEntityData? = null,
    var transfers: List<TransferEntity> = emptyList(),
    var purchases: List<PurchaseEntity> = emptyList(),
)
