package cz.cvut.fit.molinlen.android.debttracker.feature.groupedit

import cz.cvut.fit.molinlen.android.debttracker.data.model.Group
import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupInputData
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.validation.GroupInfoInputProblem
import java.util.EnumSet

data class GroupEditViewState(
    val groupInput: GroupInputData? = null,
    val inputProblems: EnumSet<GroupInfoInputProblem> = EnumSet.noneOf(GroupInfoInputProblem::class.java),
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}

fun fillFromGroup(group: Group) = GroupInputData(
    groupId = group.groupId,
    name = group.name,
    defaultCurrency = group.defaultCurrency,
)

fun emptyGroupInput() = GroupInputData(
    groupId = null,
    name = "",
    defaultCurrency = null,
)
