package cz.cvut.fit.molinlen.android.debttracker.feature.login

import com.google.firebase.auth.FirebaseUser
import cz.cvut.fit.molinlen.android.debttracker.data.model.LoginInfo
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr

data class LoginViewState(
    val email: String = "",
    val password: String = "", //todo password here may not be okay
    val loginResult: ProblemOr<FirebaseUser>? = null,
    val processing: Boolean = false,
) {
    fun toLoginInfo() = LoginInfo(
        email = email,
        password = password,
    )
}
