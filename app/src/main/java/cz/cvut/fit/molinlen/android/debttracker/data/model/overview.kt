package cz.cvut.fit.molinlen.android.debttracker.data.model

data class GlobalOwedSumsOverview(
    val sumOwed: Amount,
    val sumCollectable: Amount,
) {
    // currency should be same for both - todo make it safer (?)
    val balance: Amount =
        createAmount(value = sumCollectable.value.subtract(sumOwed.value), currency = sumOwed.currency)
}

data class UsersGroupsOverview(
    val userHasPrimaryCurrency: Boolean?,
    val sumsOverview: GlobalOwedSumsOverview?,
    val usersGroups: List<UsersGroupPreview>,
)
