package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupInfoBasic
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface GetGroupBasicInfoUseCase {
    suspend fun getBasicGroupInfo(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupInfoBasic>>
}

class GetGroupBasicInfoUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : GetGroupBasicInfoUseCase {
    override suspend fun getBasicGroupInfo(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupInfoBasic>> {
        return groupRepository.getBasicGroupInfo(userId = userId, groupId = groupId)
    }
}
