package cz.cvut.fit.molinlen.android.debttracker.data.model

import java.math.BigDecimal
import java.time.OffsetDateTime

data class TransferDetailInputData(
    val transactionId: String?,
    val from: Participant?,
    val to: Participant?,
    val amountValueString: String,
    val amount: Amount,
    val time: OffsetDateTime,
    val note: String?,
)

data class PurchaseDetailInputData(
    val transactionId: String?,
    val name: String?,
    val currency: CurrencyCode,
    val payers: List<PayerInputData>,
    val splitting: TransactionSplittingInput,
    val time: OffsetDateTime,
    val note: String?,
) {
    val totalAmount: Amount
        get() = createAmount(
            value = payers.map { it.amountValue }.fold(BigDecimal.ZERO, BigDecimal::add),
            currency = currency,
        )

    /**
     *  How much of totalAmount is yet to be split between participants in the "paid for" category.
     *  If too low to split the remainder validly equally in currency, null is returned.
     */
    fun neededToSplit(between: Int): BigDecimal? {
        return when (splitting.selectedType) {
            SplittingType.EXPLICIT -> neededToSplitExplicit(between)
            SplittingType.PORTION -> neededToSplitPortion()
        }
    }

    private fun neededToSplitExplicit(between: Int): BigDecimal? {
        val remainder = this.totalAmount.value
            .minus(
                this.splitting.shares
                    .filter { it.selected && it.explicitAmountValue > BigDecimal.ZERO }
                    .sumOf { it.explicitAmountValue }
            )
        return if (remainder < BigDecimal.ZERO || remainder >= this.currency.minimalUnit * between.toBigDecimal()
        ) {
            remainder
        } else null
    }

    private fun neededToSplitPortion(): BigDecimal? {
        return when {
            splitting.shares.any { it.selected && it.portionValue > 0 } -> null
            totalAmount.value <= BigDecimal.ZERO -> null
            else -> totalAmount.value
        }
    }
}

data class PayerInputData(
    val participant: Participant,
    val amountValue: BigDecimal,
    val amountValueString: String,
)

// TransactionParticipantBasicInputData can be easily extended for "simple" splitting, but if support for sub-splitting is ever added, TransactionSplittingInput needs to change
data class TransactionSplittingInput(
    val selectedType: SplittingType,
    val shares: List<TransactionParticipantBasicInputData>,
)

interface TransactionParticipantBasicInput {
    val participant: Participant
    val selected: Boolean
    val explicitAmountValue: BigDecimal
    val explicitAmountValueString: String
    val portionValue: Int
    val portionValueString: String
    val portionShareResult: BigDecimal
}

data class TransactionParticipantBasicInputData(
    override val participant: Participant,
    override val selected: Boolean,
    override val explicitAmountValue: BigDecimal,
    override val explicitAmountValueString: String,
    override val portionValue: Int,
    override val portionValueString: String,
    override val portionShareResult: BigDecimal,
) : TransactionParticipantBasicInput
