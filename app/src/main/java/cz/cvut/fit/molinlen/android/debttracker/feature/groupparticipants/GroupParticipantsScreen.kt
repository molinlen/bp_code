package cz.cvut.fit.molinlen.android.debttracker.feature.groupparticipants

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupParticipants
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun GroupParticipantsScreen(
    groupId: String,
    navigateAfterBackClick: () -> Unit,
) {
    val viewModel: GroupParticipantsViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId)
    }

    GroupParticipantsScreenImpl(
        viewState = viewModel.viewState.collectAsState().value,
        onBackClick = navigateAfterBackClick,
        updateFutureNickName = viewModel::updateFutureParticipant,
        canAddFutureParticipant = viewModel.canAddCurrentParticipant(),
        onAddParticipantClick = viewModel::addParticipant,
        updateUsersParticipant = viewModel::pairUserToParticipant,
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun GroupParticipantsScreenImpl(
    viewState: GroupParticipantsViewState,
    onBackClick: () -> Unit,
    updateFutureNickName: (String) -> Unit,
    canAddFutureParticipant: Boolean,
    onAddParticipantClick: () -> Unit,
    updateUsersParticipant: (Participant?) -> Unit,
) {
    val bottomSheetMenuState = rememberBottomSheetMenuState()
    val coroutineScope = rememberCoroutineScope()

    BottomSheetMenu(
        sheetState = bottomSheetMenuState,
        coroutineScope = coroutineScope,
        menuContent = {
            AddParticipantSheetContent(
                currentNickname = viewState.currentlyAddingParticipantNickname,
                lastAdditionError = viewState.participantAdditionError,
                updateAddingNickName = updateFutureNickName,
                canAdd = canAddFutureParticipant,
                onAddClick = {
                    onAddParticipantClick()
                    toggleMenuState(coroutineScope, bottomSheetMenuState)
                },
                onCancelClick = {
                    updateFutureNickName("")
                    toggleMenuState(coroutineScope, bottomSheetMenuState)
                },
            )
        },
    ) {
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> Scaffold(
                modifier = Modifier.fillMaxSize(),
                topBar = {
                    GroupParticipantsScreenHeader(onBackClick = onBackClick)
                },
                floatingActionButton = {
                    AddParticipantButton { toggleMenuState(coroutineScope, bottomSheetMenuState) }
                },
            ) { paddingValues ->
                if (viewState.processing) {
                    LoadingOverlay()
                } else if (bottomSheetMenuState.isVisible.not()) {
                    //todo is this ok?
//                    Log.d(APP_TAG, "GroupParticipantsScreenImpl: clearing focus")
                    LocalFocusManager.current.clearFocus() // so that focus does not remain on text-field in bottomSheet when bottomSheet was dismissed
                }
                viewState.groupParticipants?.let { participantsInfo ->
                    GroupParticipantsScreenContent(
                        participantsInfo = participantsInfo,
                        updateUsersParticipant = updateUsersParticipant,
                        paddingValues = paddingValues,
                    )
                }
            }
        }
    }
}

@Composable
private fun GroupParticipantsScreenContent(
    participantsInfo: UsersGroupParticipants,
    updateUsersParticipant: (Participant?) -> Unit,
    paddingValues: PaddingValues,
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .screenContentModifier(paddingValues)
            .verticalScroll(scrollState),
    ) {
        ContentTopSpacer()
        Text(
            text = stringResource(R.string.group_members_intro_beginning)
                    + stringResource(R.string.is_me)
                    + stringResource(R.string.group_members_intro_end)
        )
        if (participantsInfo.groupParticipants.isNotEmpty()) {
            IsMeRow()
        }
        participantsInfo.groupParticipants.sortedBy { it.nickname }.map { participant ->
            val isUsers = participant.participantId == participantsInfo.userParticipantId
            ParticipantCard(
                participant = participant,
                isUser = isUsers,
                onCardClick = { /*TODO edit participant*/ },
                onFavoriteClick = { updateUsersParticipant(if (isUsers) null else participant) },
            )
        }
        Spacer(Modifier.defaultMinSize(minHeight = 75.dp)) // so that lowest participants are not blocked by floating button
    }
}

@Composable
private fun IsMeRow() {
    Row(
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 5.dp, start = 8.dp, end = 8.dp),
    ) {
        Text(
            text = stringResource(R.string.is_me),
        )
    }
}

@Composable
private fun ParticipantCard(
    participant: Participant,
    isUser: Boolean,
    onCardClick: () -> Unit,
    onFavoriteClick: () -> Unit,
) {
    Card(modifier = Modifier.padding(vertical = 5.dp)) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .clickable { onCardClick() },
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Icon(
                imageVector = Icons.Default.Person,
                contentDescription = "",
                modifier = Modifier.padding(5.dp),
            )
            Text(
                text = participant.nickname,
                modifier = Modifier
                    .fillMaxWidth(0.85f)
                    .padding(5.dp),
            )
            IconButton(onClick = { onFavoriteClick() }) {
                Icon(
                    painter = when {
                        isUser -> painterResource(R.drawable.star_filled)
                        else -> painterResource(R.drawable.star_outline)
                    },
                    contentDescription = stringResource(R.string.is_me),
                    modifier = Modifier.padding(2.dp),
                )
            }
        }
    }
}

@Composable
private fun AddParticipantSheetContent(
    currentNickname: String,
    lastAdditionError: Problem?,
    updateAddingNickName: (String) -> Unit,
    canAdd: Boolean,
    onAddClick: () -> Unit,
    onCancelClick: () -> Unit,
) {
    BottomSheetMenuContentColumn(
        modifier = Modifier
            .padding(horizontal = screenContentPadding)
            .clearFocusOnTap()
    ) {
        BasicTextField(
            label = stringResource(R.string.new_participant_nickname),
            value = currentNickname,
            onValueChange = updateAddingNickName,
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.Words,
                imeAction = ImeAction.Done,
            ),
            modifier = Modifier.padding(vertical = 10.dp)
        )
        lastAdditionError?.let {
            ErrorCard(text = buildErrorMessage(
                errorMessage = stringResource(R.string.try_again_prompt),
                errorDetails = it.description,
                errorCause = it.cause,
            ))
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly,
        ) {
            BasicRectangleTextButton(
                text = stringResource(R.string.cancel),
                onClick = onCancelClick,
                buttonColors = errorTextButtonColors(),
            )
            BasicRectangleTextButton(
                text = stringResource(R.string.add),
                onClick = onAddClick,
                enabled = canAdd,
            )
        }
    }
}

@Composable
private fun AddParticipantButton(
    onClick: () -> Unit,
) {
    BasicFloatingActionButton(
        icon = Icons.Default.Add,
        contentDescription = stringResource(R.string.add_a_member),
        onClick = onClick,
    )
}

@Composable
private fun GroupParticipantsScreenHeader(
    onBackClick: () -> Unit,
) {
    ScreenHeader(
        headingText = stringResource(R.string.group_members),
        leftIcon = { HeaderBackIcon(onClick = onBackClick) },
    )
}
