package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.purchaseedit

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.math.BigDecimal
import java.time.OffsetDateTime

data class PurchaseEditViewState(
    val purchase: PurchaseDetailInputData? = null,
    val participants: Set<Participant> = emptySet(),
    val groupCurrency: CurrencyCode? = null,
    val usersParticipantId: String? = null,
    val saveError: Problem? = null,
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))

    fun neededToSplit(): BigDecimal? {
        if (participants.isEmpty() || purchase == null) {
            return BigDecimal.ZERO
        }
        return purchase.neededToSplit(participants.size)
    }

    fun withUpdatedNotOwingParticipants(currency: CurrencyCode): PurchaseEditViewState {
        val updated = purchase?.splitting?.shares
            ?.map { it.participant.participantId }?.toSet()
            ?.let { owingParticipantIds ->
                val emptyAmount = amountZero(currency)
                val emptyPortion = 0
                val mappedExistingNotOwingShares = purchase.splitting.shares
                    .map {
                        when {
                            it.selected.not() -> it.copy(
                                explicitAmountValue = emptyAmount.value,
                                explicitAmountValueString = emptyAmount.value.toPlainString(),
                            )
                            else -> it
                        }
                    }
                val mappedNewShares = participants
                    .filter { it.participantId !in owingParticipantIds }
                    .sortedBy { it.nickname }
                    .map {
                        TransactionParticipantBasicInputData(
                            participant = it,
                            selected = false,
                            explicitAmountValue = emptyAmount.value,
                            explicitAmountValueString = emptyAmount.value.toPlainString(),
                            portionValue = emptyPortion,
                            portionValueString = emptyPortion.toString(),
                            portionShareResult = emptyAmount.value,
                        )
                    }
                this.copy(
                    purchase = purchase.copy(
                        splitting = purchase.splitting.copy(
                            shares = mappedExistingNotOwingShares + mappedNewShares
                        )
                    )
                )
            }
        return updated ?: this
    }
}

fun emptyPurchaseDetailInput(
    currency: CurrencyCode,
    participants: Set<Participant>,
    eachAmount: BigDecimal,
) = PurchaseDetailInputData(
    transactionId = "",
    name = null,
    currency = currency,
    payers = emptyList(),
    splitting = TransactionSplittingInput(
        selectedType = SplittingType.PORTION, //todo what should be default?
        shares = participants.sortedBy { it.nickname }.map {
            TransactionParticipantBasicInputData(
                participant = it,
                selected = true,
                explicitAmountValue = eachAmount,
                explicitAmountValueString = eachAmount.toPlainString(),
                portionValue = 1,
                portionValueString = "1",
                portionShareResult = amountZero(currency).value,
            )
        },
    ),
    time = OffsetDateTime.now(),
    note = null,
)

fun fillPurchaseDetailInput(purchase: PurchaseDetail) = PurchaseDetailInputData(
    transactionId = purchase.transactionId,
    name = purchase.name,
    currency = purchase.fullAmount.currency,
    payers = purchase.payers.sortedBy { it.participant.nickname }.map {
        PayerInputData(
            participant = it.participant,
            amountValue = it.amount.value,
            amountValueString = it.amount.value.toPlainString(),
        )
    },
    splitting = TransactionSplittingInput(
        selectedType = purchase.splitting.getType(),
        shares = when (purchase.splitting) {
            is TransactionSplittingExplicit -> purchase.splitting.sharers.sortedBy { it.participant.nickname }.map {
                TransactionParticipantBasicInputData(
                    participant = it.participant,
                    selected = true,
                    explicitAmountValue = it.shareValue,
                    explicitAmountValueString = it.shareValue.toPlainString(),
                    portionValue = 1,
                    portionValueString = "1",
                    portionShareResult = BigDecimal.ZERO, // will be updated by ViewModel
                )
            }
            is TransactionSplittingPortion -> purchase.splitting.sharers.sortedBy { it.participant.nickname }.map {
                TransactionParticipantBasicInputData(
                    participant = it.participant,
                    selected = true,
                    explicitAmountValue = it.shareValueComputed ?: BigDecimal.ZERO,
                    explicitAmountValueString = it.shareValueComputed?.toPlainString() ?: "0",
                    portionValue = it.shareValue,
                    portionValueString = it.shareValue.toString(),
                    portionShareResult = BigDecimal.ZERO, // will be updated by ViewModel
                )
            }
        }
    ),
    time = purchase.time,
    note = purchase.note,
)
