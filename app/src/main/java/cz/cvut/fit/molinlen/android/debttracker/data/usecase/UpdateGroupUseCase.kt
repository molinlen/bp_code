package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupInfoBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface UpdateGroupUseCase {
    suspend fun updateGroupInfo(groupInfo: GroupInfoBasic): Flow<ProblemOr<Unit>>
}

class UpdateGroupUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : UpdateGroupUseCase {
    override suspend fun updateGroupInfo(groupInfo: GroupInfoBasic): Flow<ProblemOr<Unit>> =
        groupRepository.updateGroupInfo(groupInfo)
}
