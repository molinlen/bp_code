package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import android.util.Log
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInput
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputValidator
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputValidatorImpl
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

interface RegisterNewUserUseCase {
    //todo callbackFlow ? or what return type ?
    suspend fun registerNewUser(registrationInput: RegistrationInput): Flow<ProblemOr<FirebaseUser>>

    val minPasswordLength: Int
}

class RegisterNewUserUseCaseImpl(
    private val registrationInputValidator: RegistrationInputValidator = RegistrationInputValidatorImpl(),
) : RegisterNewUserUseCase {
    private val auth = Firebase.auth

    override suspend fun registerNewUser(registrationInput: RegistrationInput): Flow<ProblemOr<FirebaseUser>> {
        return callbackFlow {
            registrationInputValidator.validateRegistrationInput(registrationInput).fold(
                { trySend(Problem("Invalid registration fields", "Problems: $it").left()) },
                { registrationInfo -> //todo delegate to a repository ?
                    Either.catch {
                        auth.createUserWithEmailAndPassword(registrationInfo.email, registrationInfo.password)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Log.d(APP_TAG, "createUserWithEmail:success")
                                    trySend(
                                        auth.currentUser?.right()
                                            ?: Problem("Registration succeeded but no user gotten").left()
                                    )
                                } else {
                                    Log.w(APP_TAG, "createUserWithEmail:failure", task.exception)
                                    trySend(Problem("Registration failed", null, task.exception).left())
                                }
                            }
                    }.mapLeft {
                        Log.e(APP_TAG, "createUserWithEmail : thrown failure", it)
                        trySend(Problem("Registration failed", it.message, it).left())
                    }
                    awaitClose { }
                }
            )
        }
    }

    override val minPasswordLength: Int
        get() = RegistrationInputValidator.MIN_PASSWORD_LENGTH
}
