package cz.cvut.fit.molinlen.android.debttracker.feature.grouplist

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.nullIfBlank
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GroupListViewModel(
    private val getGroupsOverviewUseCase: GetGroupsOverviewUseCase = GetGroupsOverviewUseCaseImpl(),
    private val joinExistingGroupUseCase: JoinExistingGroupUseCase = JoinExistingGroupUseCaseImpl(),
    private val logoutUseCase: LogoutUseCase = LogoutUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(GroupListViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private val auth = Firebase.auth

    fun loadData() {
        viewModelScope.launch {
            auth.currentUser
                ?.let { user ->
                    getGroupsOverviewUseCase.getGroupsOverview(user.uid).collect { groupsOverview ->
                        _viewState.update {
                            groupsOverview.fold(
                                { problem -> GroupListViewState(error = problem) },
                                { overview ->
                                    GroupListViewState(
                                        groups = overview.usersGroups,
                                        overview = overview.sumsOverview,
                                        userHasPrimaryCurrency = overview.userHasPrimaryCurrency,
                                    )
                                }
                            )
                        }
                    }
                }
                ?: run {
                    _viewState.update { GroupListViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in group list without currentUser present")
                }
        }
    }

    fun clearGroupJoinError() {
        _viewState.update { it.copy(groupJoinError = null) }
    }

    fun canJoinSpecifiedGroup(inputGroupId: String): Boolean = groupIdToJoin(inputGroupId) != null

    private fun groupIdToJoin(inputGroupId: String): String? = inputGroupId.trim().nullIfBlank()

    fun joinGroup(inputGroupId: String, navigateAfterwards: (groupId: String) -> Unit) {
        auth.currentUser?.uid?.let { userId ->
            groupIdToJoin(inputGroupId)?.let { groupId ->
                _viewState.update { it.copy(processing = true, groupJoinError = null) }
                viewModelScope.launch {
                    joinExistingGroupUseCase.joinGroup(userId = userId, groupId = groupId, userParticipantId = null)
                        .collect { result ->
                            result.map {
                                withContext(Dispatchers.Main) {
                                    navigateAfterwards(groupId)
                                }
                            }
                            _viewState.update {
                                it.copy(
                                    processing = false,
                                    groupJoinError = result.fold({ problem -> problem }, { null }),
                                )
                            }
                        }
                }
            }
        }
    }

    fun logout(navigateAfterwards: () -> Unit) {
        _viewState.update { it.copy(processing = true) }
        viewModelScope.launch {
            logoutUseCase.logout().collect {
                withContext(Dispatchers.Main) {
                    navigateAfterwards()
                }
                _viewState.update { it.copy(processing = false) }
            }
        }
    }
}
