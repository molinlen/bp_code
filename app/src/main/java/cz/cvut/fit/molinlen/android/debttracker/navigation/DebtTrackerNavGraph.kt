package cz.cvut.fit.molinlen.android.debttracker.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.feature.groupInfo.GroupInfoScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.groupdetail.GroupDetailScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.groupedit.GroupEditScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.grouplist.GroupListScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.groupparticipants.GroupParticipantsScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.login.LoginScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.registration.RegistrationScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.purchasedetail.PurchaseDetailScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.transferdetail.TransferDetailScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.purchaseedit.PurchaseEditScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.transferedit.TransferEditScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.userdetail.UserDetailScreen
import cz.cvut.fit.molinlen.android.debttracker.feature.useredit.UserInfoEditScreen
import cz.cvut.fit.molinlen.android.debttracker.navigation.DebtTrackerNavArguments.EMAIL_KEY
import cz.cvut.fit.molinlen.android.debttracker.navigation.DebtTrackerNavArguments.PASSWORD_KEY

private val home = DebtTrackerDestinations.GROUP_LIST

@Composable
fun DebtTrackerNavGraph(
    preferredStartDestination: DebtTrackerDestinations? = null,
    navController: NavHostController = rememberNavController(),
) {
    val startDestination = preferredStartDestination
        ?: if (Firebase.auth.currentUser == null) DebtTrackerDestinations.LOGIN else home
    NavHost(
        navController = navController,
        startDestination = startDestination.route,
    ) {
        composable(DebtTrackerDestinations.LOGIN.route) {
            LoginScreen(
                navigateAfterLogIn = {
                    navController.navigate(home.route) { popUpTo(navController.graph.id) { inclusive = true } }
                },
                navigateAfterRegister = { viewState ->
                    navController.navigate("${DebtTrackerDestinations.REGISTER.route}?$EMAIL_KEY=${viewState.email}&$PASSWORD_KEY=${viewState.password}")
                },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.REGISTER.route}?$EMAIL_KEY={${EMAIL_KEY}}&$PASSWORD_KEY={$PASSWORD_KEY}",
            arguments = listOf(
                navArgument(EMAIL_KEY) {
                    nullable = true
                    type = NavType.StringType
                },
                navArgument(PASSWORD_KEY) {
                    nullable = true
                    type = NavType.StringType
                },
            )
        ) {
            RegistrationScreen(
                email = it.arguments?.getString(EMAIL_KEY),
                password = it.arguments?.getString(PASSWORD_KEY),
                navigateAfterCreateAccount = { // someday we might want a tutorial or something here
                    navController.navigate(home.route) { popUpTo(navController.graph.id) { inclusive = true } }
                },
            )
        }

        composable(DebtTrackerDestinations.USER_DETAIL.route) {
            UserDetailScreen(
                navigateAfterDataEditClick = { navController.navigate(DebtTrackerDestinations.USER_EDIT.route) },
                navigateAfterBackClick = { navController.navigateUp() },
            )
        }

        composable(DebtTrackerDestinations.USER_EDIT.route) {
            UserInfoEditScreen(
                navigateOnExit = { navController.navigateUp() },
            )
        }

        composable(DebtTrackerDestinations.GROUP_LIST.route) {
            GroupListScreen(
                navigateAfterLogout = {
                    navController.navigate(DebtTrackerDestinations.LOGIN.route) {
                        popUpTo(navController.graph.id) { inclusive = true }
                    }
                },
                navigateAfterAccountClick = { navController.navigate(DebtTrackerDestinations.USER_DETAIL.route) },
                navigateAfterGroupClick = { groupId ->
                    navController.navigate("${DebtTrackerDestinations.GROUP_DETAIL.route}/$groupId")
                },
                navigateAfterCreateGroupClick = { navController.navigate(DebtTrackerDestinations.GROUP_EDIT.route) },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.GROUP_DETAIL.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                }
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            GroupDetailScreen(
                groupId = groupId,
                navigateAfterBackClick = { navController.navigateUp() },
                navigateAfterPurchaseClick = { purchaseId ->
                    navController.navigate("${DebtTrackerDestinations.PURCHASE_DETAIL.route}/$groupId/$purchaseId")
                },
                navigateAfterTransferClick = { transferId ->
                    navController.navigate("${DebtTrackerDestinations.TRANSFER_DETAIL.route}/$groupId/$transferId")
                },
                navigateAfterAddPurchaseClick = { navController.navigate("${DebtTrackerDestinations.PURCHASE_EDIT.route}/$groupId") },
                navigateAfterAddTransferClick = { navController.navigate("${DebtTrackerDestinations.TRANSFER_EDIT.route}/$groupId") },
                navigateAfterInfoClick = { navController.navigate("${DebtTrackerDestinations.GROUP_INFO.route}/$groupId") },
                navigateAfterMembersClick = { navController.navigate("${DebtTrackerDestinations.GROUP_PARTICIPANTS.route}/$groupId") },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.GROUP_INFO.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                }
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            GroupInfoScreen(
                groupId = groupId,
                navigateAfterBackClick = { navController.navigateUp() },
                navigateAfterEditGroupClick = { navController.navigate("${DebtTrackerDestinations.GROUP_EDIT.route}?${DebtTrackerNavArguments.GROUP_ID_KEY}=$groupId") },
                navigateAfterLeaveGroupClick = {
                    navController.navigate(home.route) { popUpTo(navController.graph.id) { inclusive = true } }
                },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.GROUP_PARTICIPANTS.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                }
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            GroupParticipantsScreen(
                groupId = groupId,
                navigateAfterBackClick = { navController.navigateUp() },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.GROUP_EDIT.route}?${DebtTrackerNavArguments.GROUP_ID_KEY}={${DebtTrackerNavArguments.GROUP_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    nullable = true
                    type = NavType.StringType
                },
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)
            GroupEditScreen(
                groupId = groupId,
                navigateAfterSave = { potentiallyNewGroupId ->
                    groupId
                        ?.let { navController.navigateUp() }
                        ?: run {
                            navController.popBackStack()
                            navController.navigate("${DebtTrackerDestinations.GROUP_DETAIL.route}/$potentiallyNewGroupId")
                        }
                },
                navigateAfterDiscard = { navController.navigateUp() },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.PURCHASE_DETAIL.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}/{${DebtTrackerNavArguments.PURCHASE_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                },
                navArgument(DebtTrackerNavArguments.PURCHASE_ID_KEY) {
                    type = NavType.StringType
                },
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            val purchaseId = it.arguments?.getString(DebtTrackerNavArguments.PURCHASE_ID_KEY)!!
            PurchaseDetailScreen(
                groupId = groupId,
                purchaseId = purchaseId,
                navigateAfterBackClick = { navController.navigateUp() },
                navigateAfterEditClick = { navController.navigate("${DebtTrackerDestinations.PURCHASE_EDIT.route}/$groupId?${DebtTrackerNavArguments.PURCHASE_ID_KEY}=$purchaseId") },
                navigateAfterDeleteClick = { navController.navigateUp() },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.TRANSFER_DETAIL.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}/{${DebtTrackerNavArguments.TRANSFER_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                },
                navArgument(DebtTrackerNavArguments.TRANSFER_ID_KEY) {
                    type = NavType.StringType
                },
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            val transferId = it.arguments?.getString(DebtTrackerNavArguments.TRANSFER_ID_KEY)!!
            TransferDetailScreen(
                groupId = groupId,
                transferId = transferId,
                navigateAfterBackClick = { navController.navigateUp() },
                navigateAfterEditClick = { navController.navigate("${DebtTrackerDestinations.TRANSFER_EDIT.route}/$groupId?${DebtTrackerNavArguments.TRANSFER_ID_KEY}=$transferId") },
                navigateAfterDeleteClick = { navController.navigateUp() },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.PURCHASE_EDIT.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}?${DebtTrackerNavArguments.PURCHASE_ID_KEY}={${DebtTrackerNavArguments.PURCHASE_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                },
                navArgument(DebtTrackerNavArguments.PURCHASE_ID_KEY) {
                    nullable = true
                    type = NavType.StringType
                },
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            val purchaseId = it.arguments?.getString(DebtTrackerNavArguments.PURCHASE_ID_KEY)
            PurchaseEditScreen(
                groupId = groupId,
                purchaseId = purchaseId,
                navigateOnExit = { navController.navigateUp() },
            )
        }

        composable(
            route = "${DebtTrackerDestinations.TRANSFER_EDIT.route}/{${DebtTrackerNavArguments.GROUP_ID_KEY}}?${DebtTrackerNavArguments.TRANSFER_ID_KEY}={${DebtTrackerNavArguments.TRANSFER_ID_KEY}}",
            arguments = listOf(
                navArgument(DebtTrackerNavArguments.GROUP_ID_KEY) {
                    type = NavType.StringType
                },
                navArgument(DebtTrackerNavArguments.TRANSFER_ID_KEY) {
                    nullable = true
                    type = NavType.StringType
                },
            )
        ) {
            val groupId = it.arguments?.getString(DebtTrackerNavArguments.GROUP_ID_KEY)!!
            val transferId = it.arguments?.getString(DebtTrackerNavArguments.TRANSFER_ID_KEY)
            TransferEditScreen(
                groupId = groupId,
                transferId = transferId,
                navigateOnExit = { navController.navigateUp() },
            )
        }
    }
}
