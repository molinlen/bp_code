package cz.cvut.fit.molinlen.android.debttracker.data.computation

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupDetail
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupPreview

interface GroupStatusCalculator {
    fun computeGroupStatus(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupDetail>
    fun computeGroupPreview(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupPreview>

    //todo rm this lower method and use the upper one
    fun computeGroupPreviews(usersGroups: List<UsersGroupBasic>): ProblemOr<List<UsersGroupPreview>>
}
