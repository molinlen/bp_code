package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ParticipantBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface CreateGroupParticipantsUseCase {
    suspend fun addGroupParticipant(
        groupId: String,
        participant: ParticipantBasic,
    ): Flow<ProblemOr<Unit>>
}

class CreateGroupParticipantsUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : CreateGroupParticipantsUseCase {
    override suspend fun addGroupParticipant(
        groupId: String,
        participant: ParticipantBasic,
    ): Flow<ProblemOr<Unit>> {
        return groupRepository.addGroupParticipants(groupId = groupId, newParticipants = listOf(participant))
    }
}
