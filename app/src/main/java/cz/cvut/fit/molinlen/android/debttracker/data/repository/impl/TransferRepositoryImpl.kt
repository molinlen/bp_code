package cz.cvut.fit.molinlen.android.debttracker.data.repository.impl

import arrow.core.computations.either
import arrow.core.flatten
import arrow.core.left
import arrow.core.zip
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.Db
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl.FireStoreDbImpl
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf

class TransferRepositoryImpl(
    private val db: Db = FireStoreDbImpl(),
) : TransferRepository {
    override suspend fun getTransferDetail(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<TransferDetail>> = flow {
        combine(
            db.getTransfer(groupId, transferId),
            db.getGroupParticipants(groupId)
        ) { transferEither, participantsEither ->
            val result = transferEither.zip(participantsEither) { transferEntity, participantEntities ->
                either.eager {
                    val participants = participantEntities.map { it.toModel().bind() }
                    transferEntity.toModelWithParticipants(participants.associatedByParticipantIds()).bind()
                }
            }.flatten()
            result
        }.collect { emit(it) }
    }

    override suspend fun createTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<String>> =
        db.createTransfer(groupId, transfer.toEntityData())

    override suspend fun updateTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<Unit>> {
        return if (transfer.transactionId.isNotBlank()) {
            db.updateTransfer(groupId, transfer.toEntity())
        } else {
            flowOf(Problem("Blank transactionId in updateTransfer").left())
        }
    }

    override suspend fun deleteTransfer(userId: String, groupId: String, transferId: String): Flow<ProblemOr<Unit>> =
        db.deleteTransfer(groupId = groupId, transferId = transferId)
}
