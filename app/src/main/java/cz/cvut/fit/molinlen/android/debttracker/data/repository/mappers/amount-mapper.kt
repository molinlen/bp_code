package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.Amount
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.AmountEntityData
import java.math.BigDecimal

fun Amount.toEntityData() = AmountEntityData(
    value = value.toPlainString(), //todo find the difference between the 3 toString variations
    currency = currency,
)

fun AmountEntityData.toModel(): ProblemOr<Amount> {
    return Either.catch {
        Amount(
            value = BigDecimal(value),
            currency = currency!!,
        )
    }.mapConversionThrowable("AmountEntity")
}


