package cz.cvut.fit.molinlen.android.debttracker.data.repository

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import kotlinx.coroutines.flow.Flow

interface PurchaseRepository {
    suspend fun getPurchaseDetail(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<PurchaseDetail>>

    suspend fun createPurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<String>>

    suspend fun updatePurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<Unit>>

    suspend fun deletePurchase(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<Unit>>
}
