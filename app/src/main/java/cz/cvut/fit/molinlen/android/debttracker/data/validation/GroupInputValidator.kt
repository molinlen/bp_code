package cz.cvut.fit.molinlen.android.debttracker.data.validation

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupInfoBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupInputData
import java.util.*

interface GroupInputValidator {
    suspend fun validateGroupInput(groupInput: GroupInputData): Either<EnumSet<GroupInfoInputProblem>, GroupInfoBasic>
}

class GroupInputValidatorImpl : GroupInputValidator {
    override suspend fun validateGroupInput(groupInput: GroupInputData): Either<EnumSet<GroupInfoInputProblem>, GroupInfoBasic> {
        val fails = listOfNotNull(
            validateName(groupInput),
            validateCurrency(groupInput),
        )

        return when {
            fails.isNotEmpty() -> EnumSet.copyOf(fails).left()
            else -> GroupInfoBasic(
                groupId = groupInput.groupId ?: "",
                name = groupInput.name.trim(),
                defaultCurrency = groupInput.defaultCurrency!!,
            ).right()
        }
    }

    private fun validateName(groupInput: GroupInputData): GroupInfoInputProblem? {
        return if (groupInput.name.isBlank()) {
            GroupInfoInputProblem.GROUP_NAME_BLANK
        } else null
    }

    private fun validateCurrency(groupInput: GroupInputData): GroupInfoInputProblem? {
        return if (groupInput.defaultCurrency == null) {
            GroupInfoInputProblem.GROUP_CURRENCY_MISSING
        } else null
    }
}
