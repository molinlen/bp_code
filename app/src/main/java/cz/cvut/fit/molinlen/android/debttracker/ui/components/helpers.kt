package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.ui.Modifier

inline fun Modifier.alsoIf(
    condition: Boolean,
    then: (Modifier) -> Modifier,
): Modifier {
    return this.alsoIfOtherwise(
        condition = condition,
        then = then,
    )
}

inline fun Modifier.alsoIfOtherwise(
    condition: Boolean,
    then: (Modifier) -> Modifier,
    otherwise: (Modifier) -> Modifier = { it },
): Modifier {
    return if (condition) {
        then(this)
    } else {
        otherwise(this)
    }
}

inline fun Modifier.alsoWhen(
    then: (Modifier) -> Modifier,
): Modifier {
    return then(this)
}
