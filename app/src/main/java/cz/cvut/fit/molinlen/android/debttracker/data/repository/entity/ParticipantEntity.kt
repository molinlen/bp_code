package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

data class ParticipantEntityData(
    var nickname: String? = null,
    //todo connectedUserUuids ?
)

data class ParticipantEntity(
    val participantId: String,
    val participant: ParticipantEntityData,
)
