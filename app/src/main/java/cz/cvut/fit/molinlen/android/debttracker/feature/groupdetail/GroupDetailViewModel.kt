package cz.cvut.fit.molinlen.android.debttracker.feature.groupdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetGroupDetailUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetGroupDetailUseCaseImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class GroupDetailViewModel(
    private val getGroupDetailUseCase: GetGroupDetailUseCase = GetGroupDetailUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(GroupDetailViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String

    private val auth = Firebase.auth
    val userId: String? = auth.currentUser?.uid

    fun loadData(groupId: String) {
        this.groupId = groupId
        viewModelScope.launch {
            auth.currentUser
                ?.let { user ->
                    getGroupDetailUseCase.getUsersGroupDetail(userId = user.uid, groupId = groupId)
                        .collect {
                            it.fold(
                                { problem ->
                                    _viewState.update { GroupDetailViewState(error = problem) }
                                },
                                { usersGroupDetail ->
                                    _viewState.update { GroupDetailViewState(usersGroupDetail) }
                                },
                            )
                        }
                }
                ?: run {
                    _viewState.update { GroupDetailViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in group detail without currentUser present")
                }
        }
    }
}
