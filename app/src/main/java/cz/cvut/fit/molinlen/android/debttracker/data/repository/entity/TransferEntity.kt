package cz.cvut.fit.molinlen.android.debttracker.data.repository.entity

import com.google.firebase.Timestamp

data class TransferEntityData(
    var fromParticipantId: String? = null,
    var toParticipantId: String? = null,
    var time: Timestamp? = null,
    var amount: AmountEntityData? = null,
    var note: String? = null,
    var comments: List<CommentEntity> = emptyList(),
//    var comments: Map<String, CommentEntityData> = emptyMap(),
)

data class TransferEntityUpdateData(
    val fromParticipantId: String?,
    val toParticipantId: String?,
    val time: Timestamp?,
    val amount: AmountEntityData?,
    val note: String?,
)

data class TransferEntity(
    var transactionId: String? = null,
    var transfer: TransferEntityData? = null,
)
