package cz.cvut.fit.molinlen.android.debttracker.data.repository.impl

import android.util.Log
import arrow.core.*
import arrow.core.computations.either
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.Db
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl.FireStoreDbImpl
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.GroupEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.PurchaseEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toModel
import kotlinx.coroutines.flow.*

class GroupRepositoryImpl(
    private val db: Db = FireStoreDbImpl(),
) : GroupRepository {

    override suspend fun getGroupList(userId: String): Flow<ProblemOr<List<UsersGroupBasic>>> = flow {
        db.getUsersGroupParticipants(userId).collect { usersGroupParticipantsResult ->
            usersGroupParticipantsResult.fold(
                { problem -> emit(problem.left()) },
                { usersGroupsData ->
                    val groupFlows = usersGroupsData.map { usersGroupData ->
                        usersGroupData.groupId
                            ?.let { groupId ->
                                getGroupWithUserParticipant(
                                    groupId = groupId,
                                    userParticipantId = usersGroupData.userParticipantId,
                                )
                            }
                            ?: run {
                                Log.e(APP_TAG, "No groupId in GroupRepositoryImpl::getGroupList")
                                flowOf(Problem("No groupId in GroupRepositoryImpl::getGroupList").left())
                            }
                    }
                    combineGroupFlows(groupFlows).collect { result ->
                        emit(result)
                    }
                }
            )
        }
    }

    private fun combineGroupFlows(
        groupFlows: List<Flow<ProblemOr<UsersGroupBasic>>>,
    ): Flow<ProblemOr<List<UsersGroupBasic>>> {
        return if (groupFlows.isEmpty()) {
            flowOf(emptyList<UsersGroupBasic>().right())
        } else {
            combine(groupFlows) { groupResults ->
                groupResults.map { either -> either.mapLeft { Log.e(APP_TAG, "Error getting group: $it") } }
                val groups = groupResults.mapNotNull { groupResult ->
                    groupResult.fold(
                        { problem -> Log.e(APP_TAG, "Error getting group: $problem"); null },
                        { group -> group }
                    )
                }
                if (groups.isEmpty() && groupResults.isNotEmpty()) { // group(s) found but none ok
                    either.eager { groupResults.map { it.bind() } }
                } else { // at least one group ok -> no error propagation
                    groups.right()
                }
            }
        }
    }

    private fun getGroupWithUserParticipant(
        groupId: String,
        userParticipantId: String?,
    ): Flow<ProblemOr<UsersGroupBasic>> = flow {
        combine(
            db.getGroupInfo(groupId = groupId),
            db.getGroupPurchases(groupId = groupId),
            db.getGroupTransfers(groupId = groupId),
        ) { groupInfoEither, purchasesEither, transfersEither ->
            val result = groupInfoEither.zip(purchasesEither, transfersEither
            ) { groupInfoEntity, purchaseEntities, transferEntities ->
                composeCompleteGroup(
                    userParticipantId = userParticipantId,
                    groupInfoEntity = groupInfoEntity,
                    transferEntities = transferEntities,
                    purchaseEntities = purchaseEntities,
                )
            }.flatten()
            result
        }.collect { emit(it) }
    }

    private fun composeCompleteGroup(
        userParticipantId: String?,
        groupInfoEntity: GroupEntity,
        transferEntities: List<TransferEntity>,
        purchaseEntities: List<PurchaseEntity>,
    ): ProblemOr<UsersGroupBasic> {
        return either.eager {
            UsersGroupBasic(
                userParticipantId = userParticipantId,
                group = groupInfoEntity.copy(
                    transfers = transferEntities,
                    purchases = purchaseEntities,
                ).toModel().bind()
            )
        }
    }

    override suspend fun getGroup(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupBasic>> = flow {
        combine(
            db.getGroupInfo(groupId = groupId),
            db.getUsersGroupParticipant(userId = userId, groupId = groupId),
            db.getGroupPurchases(groupId = groupId),
            db.getGroupTransfers(groupId = groupId),
        ) { groupInfoEither, userParticipantEither, purchasesEither, transfersEither ->
            val result = groupInfoEither.zip(userParticipantEither, purchasesEither, transfersEither
            ) { groupInfoEntity, userParticipant, purchaseEntities, transferEntities ->
                composeCompleteGroup(
                    userParticipantId = userParticipant.userParticipantId,
                    groupInfoEntity = groupInfoEntity,
                    transferEntities = transferEntities,
                    purchaseEntities = purchaseEntities,
                )
            }.flatten()
            result
        }.collect { emit(it) }
    }

    override suspend fun getBasicGroupInfo(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupInfoBasic>> = flow {
        combine(
            db.getGroupInfo(groupId = groupId),
            db.getUsersGroupParticipant(userId = userId, groupId = groupId),
        ) { groupInfoEither, userParticipantEither ->
            val result = groupInfoEither.zip(userParticipantEither) { groupInfoEntity, userParticipant ->
                val data = either.eager {
                    UsersGroupInfoBasic(
                        userParticipantId = userParticipant.userParticipantId,
                        group = groupInfoEntity.toModel().bind().let {
                            GroupInfoBasic(
                                groupId = it.groupId,
                                name = it.name,
                                defaultCurrency = it.defaultCurrency,
                            )
                        }
                    )
                }
                data
            }.flatten()
            result
        }.collect { emit(it) }
    }

    override suspend fun getUsersGroupParticipants(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupParticipants>> = flow {
        combine(
            db.getGroupInfo(groupId = groupId),
            db.getUsersGroupParticipant(userId = userId, groupId = groupId),
        ) { groupInfoEither, userParticipantEither ->
            val result = groupInfoEither.zip(userParticipantEither) { groupInfoEntity, userParticipant ->
                val data = either.eager {
                    val group = groupInfoEntity.toModel().bind()
                    UsersGroupParticipants(
                        groupParticipants = group.participants,
                        userParticipantId = userParticipant.userParticipantId,
                        groupCurrency = group.defaultCurrency,
                    )
                }
                data
            }.flatten()
            result
        }.collect { emit(it) }
    }

    override suspend fun getUsersGroupParticipantId(userId: String, groupId: String): Flow<ProblemOr<String?>> = flow {
        db.getUsersGroupParticipant(userId = userId, groupId = groupId).collect { result ->
            emit(result.map { it.userParticipantId })
        }
    }

    override suspend fun createNewGroup(userId: String, groupData: GroupInfoBasic): Flow<ProblemOr<String>> = flow {
        if (groupData.name.isBlank()) {
            emit(Problem("Blank group name").left())
        } else {
            db.createGroup(groupData.toEntityData()).collect { groupCreationResult ->
                groupCreationResult.fold(
                    { problem -> emit(problem.left()) },
                    { groupId ->
                        db.linkGroupSetParticipant(userId = userId, groupId = groupId, userParticipantId = null)
                            .collect { result ->
                                result.fold(
                                    { problem ->
                                        emit(problem.copy(
                                            description = "Created group [$groupId] but failed to link to user (you can save the id and try to connect again).",
                                            details = problem.description).left())
                                    },
                                    { emit(groupId.right()) }
                                )
                            }
                    }
                )
            }
        }
    }

    override suspend fun updateGroupInfo(groupInfo: GroupInfoBasic): Flow<ProblemOr<Unit>> = flow {
        if (groupInfo.name.isBlank()) {
            emit(Problem("Blank group name").left())
        } else {
            db.updateGroup(groupInfo.toEntity()).collect { groupCreationResult ->
                emit(groupCreationResult)
            }
        }
    }

    override suspend fun joinExistingGroup(
        userId: String,
        groupId: String,
        asParticipantId: String?,
    ): Flow<ProblemOr<GroupInfoBasic>> = flow {
        db.linkGroupSetParticipant(userId = userId, groupId = groupId, userParticipantId = asParticipantId)
            .collect { result ->
                result.fold(
                    { problem -> emit(problem.left()) },
                    {
                        db.getGroupInfo(groupId).collect { groupGetResult ->
                            groupGetResult.fold(
                                {},
                                { groupEntity ->
                                    val groupInfo = groupEntity.toModel().map { group ->
                                        GroupInfoBasic(
                                            groupId = group.groupId,
                                            name = group.name,
                                            defaultCurrency = group.defaultCurrency,
                                        )
                                    }
                                    emit(groupInfo)
                                }
                            )
                        }
                    }
                )
            }
    }

    override suspend fun leaveGroup(userId: String, groupId: String): Flow<ProblemOr<Unit>> = flow {
        db.unlinkUserFromGroup(userId = userId, groupId = groupId)
            .collect { result ->
                emit(result)
            }
    }

    override suspend fun addGroupParticipants(
        groupId: String,
        newParticipants: List<ParticipantBasic>,
    ): Flow<ProblemOr<Unit>> = db.addParticipants(groupId, newParticipants.map { it.toEntityData() }.toSet())
}
