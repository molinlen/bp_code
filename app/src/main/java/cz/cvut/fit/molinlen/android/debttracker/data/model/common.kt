package cz.cvut.fit.molinlen.android.debttracker.data.model

import java.math.BigDecimal
import java.math.RoundingMode

enum class CurrencyCode(val precision: Int) {
    CZK(2),
    EUR(2),
    USD(2),
    ;

    val minimalUnit: BigDecimal
        get() = BigDecimal(
            ""
                    + (if (precision > 0) "0." else "")
                    + "0".repeatIfPositive(precision - 1)
                    + "1"
        ).setScale(precision, RoundingMode.UP)
}
//todo map CurrencyCode to language-specific string

data class Amount(
    val value: BigDecimal,
    val currency: CurrencyCode,
) {
    fun copyAmount(value: BigDecimal = this.value, currency: CurrencyCode = this.currency): Amount {
        //todo can this change be somehow applied within kotlin .copy() ? would be much better
        return createAmount(value, currency)
    }

    fun mapToStringWithSign(): String {
        val prefix = when {
            value > BigDecimal.ZERO -> "+ "
            value < BigDecimal.ZERO -> "- "
            else -> ""
        }
        return prefix + mapAbsToString()
    }

    fun mapAbsToString(): String {
        //todo language-specific number spacing, commas/dots
        val valuePostfixLength = maxOf(currency.precision - value.scale(), 0)
        val valuePostfix = "0".repeat(valuePostfixLength)
        return value.abs().toPlainString() + valuePostfix + " " + currency
    }
}

data class AmountWithCarry(
    val amount: Amount,
    val carry: BigDecimal,
)

data class AmountPortionsWithCarry(
    val portionResults: Map<Int, Amount>,
    val carry: BigDecimal,
)

fun createAmount(
    value: BigDecimal,
    currency: CurrencyCode,
) = Amount(
    value = value.scaleFor(currency),
    currency = currency,
)

fun amountZero(currency: CurrencyCode) = Amount(value = BigDecimal.ZERO.scaleFor(currency), currency = currency)

fun BigDecimal.scaleFor(currency: CurrencyCode): BigDecimal {
    return this.setScale(currency.precision, RoundingMode.DOWN)
}

// rm this later
internal fun amountFromDouble(value: Double, currencyCode: CurrencyCode = CurrencyCode.CZK) =
    createAmount(value = BigDecimal.valueOf(value), currency = currencyCode)

data class AmountInputValidationResult(
    val resultingAmount: Amount?,
    val resultingInputString: String,
)

data class IntInputValidationResult(
    val resultingValue: Int?,
    val resultingInputString: String,
)



fun String?.nullIfBlank(): String? = if (this?.isNotBlank() == true) this else null

fun String.repeatIfPositive(n: Int) = if (n < 0) "" else this.repeat(n)


