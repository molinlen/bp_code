package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.purchasedetail

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.PurchaseDetail

data class PurchaseDetailViewState(
    val purchase: PurchaseDetail? = null,
    val userParticipantId: String? = null,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
