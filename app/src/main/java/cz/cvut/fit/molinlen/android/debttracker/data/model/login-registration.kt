package cz.cvut.fit.molinlen.android.debttracker.data.model

data class LoginInfo(
    val email: String,
    val password: String,
)

data class RegistrationInfo(
    val email: String,
    val password: String,
    val termsAccepted: Boolean,
)

data class RegistrationInput(
    val email: String,
    val passwordFirst: String,
    val passwordRepeated: String,
    val termsAccepted: Boolean,
)

