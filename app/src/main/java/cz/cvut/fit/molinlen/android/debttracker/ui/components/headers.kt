package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.Typography

@Composable
fun ScreenHeader(
    headingText: String,
    leftIcon: (@Composable () -> Unit)? = null,
    rightIcons: List<@Composable () -> Unit> = emptyList(),
    backgroundColor: Color = ScreenHeaderColors.standardColor,
) {
    TopAppBar(
        title = {
            ScreenHeaderText(
                headingText = headingText,
            )
        },
        navigationIcon = leftIcon,
        actions = {
            rightIcons.map { it() }
        },
        backgroundColor = backgroundColor,
    )
}

object ScreenHeaderColors {
    val standardColor
        @Composable get() = MaterialTheme.colors.primary
    val editScreenColor
        @Composable get() = AccentColors.surfaceAccentMild
}

@Composable
fun ScreenHeaderText(
    headingText: String,
    modifier: Modifier = Modifier,
) {
    //todo shorten overly long text somehow
    Text(
        text = headingText,
        style = Typography.h6,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = modifier,
    )
}
