package cz.cvut.fit.molinlen.android.debttracker.data.repository.fake

import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.amountFromDouble
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.amy
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.catherine
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.danny
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.john
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.tom
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.user
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.time.ZoneOffset

class PurchaseRepositoryFake : PurchaseRepository {
    override suspend fun getPurchaseDetail(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<PurchaseDetail>> = flowOf(purchaseDetail.right())

    override suspend fun createPurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<String>> = flowOf(purchaseDetail.transactionId.right())

    override suspend fun updatePurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<Unit>> = flowOf(Unit.right())

    override suspend fun deletePurchase(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<Unit>> = flowOf(Unit.right())

    private val purchaseDetail = PurchaseDetail(
        transactionId = cz.cvut.fit.molinlen.android.debttracker.data.computation.fake.randomId(),
        name = "Dumpling Apocalypse",
        payers = listOf(
            Payer(
                participant = amy,
                amount = amountFromDouble(800.0)
            ),
            Payer(
                participant = danny,
                amount = amountFromDouble(700.0)
            ),
        ),
        comments = listOf(
            CommentDetail(
                writerUserId = "4IePFt8v0eNqtioh1GyGK2RJj123",
                content = "Hi from molinlen",
                time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                edited = false,
            ),
            CommentDetail(
                writerUserId = "5IePFt8v0eNqtioh1GyGK2RJj125",
                content = "Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Praesent in mauris eu tortor porttitor accumsan. Mauris dictum facilisis augue. Donec vitae arcu. Duis condimentum augue id magna semper rutrum. Praesent in mauris eu tortor porttitor accumsan. Integer vulputate sem a nibh rutrum consequat. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Aenean id metus id velit ullamcorper pulvinar. In enim a arcu imperdiet malesuada. Curabitur sagittis hendrerit ante. ",
                time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                edited = true,
            ),
        ),
        splitting = TransactionSplittingExplicit(
            sharers = listOf(
                TransactionShareExplicit(
                    participant = danny,
                    shareValue = BigDecimal.valueOf(250)
                ),
                TransactionShareExplicit(
                    participant = catherine,
                    shareValue = BigDecimal.valueOf(250)
                ),
                TransactionShareExplicit(
                    participant = tom,
                    shareValue = BigDecimal.valueOf(150)
                ),
                TransactionShareExplicit(
                    participant = user,
                    shareValue = BigDecimal.valueOf(300)
                ),
                TransactionShareExplicit(
                    participant = amy,
                    shareValue = BigDecimal.valueOf(250)
                ),
                TransactionShareExplicit(
                    participant = john,
                    shareValue = BigDecimal.valueOf(300)
                ),
            )
        ),
        time = OffsetDateTime.of(2022, 3, 3, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
        note = "Wasn't so apocalyptic after all. 7/10."
                + " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer in sapien. Etiam dictum tincidunt diam. Nulla pulvinar eleifend sem.",
    )
}
