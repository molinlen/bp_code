package cz.cvut.fit.molinlen.android.debttracker.data

import android.util.Log
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode.*
import java.math.BigDecimal
import java.math.RoundingMode

interface CurrencyConvertor {
    fun convert(value: BigDecimal, from: CurrencyCode, to: CurrencyCode): BigDecimal
    fun convert(from: Amount, to: CurrencyCode): Amount
}

class CurrencyConvertorImpl : CurrencyConvertor {
    //todo store these in DB (and ideally update on background)
    private val multipliers: Map<Pair<CurrencyCode, CurrencyCode>, BigDecimal> = mapOf(
        Pair(Pair(EUR, USD), BigDecimal.valueOf(0.9965)),
        Pair(Pair(EUR, CZK), BigDecimal.valueOf(24.6196)),
        Pair(Pair(USD, CZK), BigDecimal.valueOf(24.7061)),
    )

    private val divisors: Map<Pair<CurrencyCode, CurrencyCode>, BigDecimal> =
        multipliers.mapKeys { entry -> Pair(entry.key.second, entry.key.first) }

    override fun convert(value: BigDecimal, from: CurrencyCode, to: CurrencyCode): BigDecimal {
        if (from == to) {
            return value
        }
        return multipliers[Pair(from, to)]
            ?.let { multiplier ->
                value.multiply(multiplier).scaleFor(to)
            }
            ?: divisors[Pair(from, to)]?.let { divisor ->
                value.divide(divisor, to.precision, RoundingMode.HALF_UP).scaleFor(to)
            }
            ?: let {
                Log.e(APP_TAG, "$ERROR_PREFIX no currency conversion found for pair [${from}] - [$to]")
                BigDecimal.ZERO.scaleFor(to) //todo possibly throw an exception here?
            }
    }

    override fun convert(from: Amount, to: CurrencyCode): Amount {
        return createAmount(
            value = convert(value = from.value, from = from.currency, to = to),
            currency = to,
        )
    }
}
