package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.transferdetail

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.nullIfBlank
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.OptionalCommentsColumn
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.TransactionDeleteDialog
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.TransactionMenuItems
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun TransferDetailScreen(
    groupId: String,
    transferId: String,
    navigateAfterBackClick: () -> Unit,
    navigateAfterEditClick: () -> Unit,
    navigateAfterDeleteClick: () -> Unit,
) {
    val viewModel: TransferDetailViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId = groupId, transferId = transferId)
    }

    TransferDetailScreenImpl(
        userId = viewModel.userId,
        onBackClick = navigateAfterBackClick,
        onEditClick = navigateAfterEditClick,
        onDeleteConfirm = { viewModel.deleteTransfer(navigateAfterDeleteClick) },
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@OptIn(ExperimentalMaterialApi::class) // for BottomSheet
@Composable
fun TransferDetailScreenImpl(
    userId: String?,
    onBackClick: () -> Unit,
    onEditClick: () -> Unit,
    onDeleteConfirm: () -> Unit,
    viewState: TransferDetailViewState,
) {
    val bottomSheetMenuState = rememberBottomSheetMenuState()
    val coroutineScope = rememberCoroutineScope()
    val deletionDialogVisible = remember { mutableStateOf(false) }
    BottomSheetMenu(
        sheetState = bottomSheetMenuState,
        coroutineScope = coroutineScope,
        menuContent = {
            BottomSheetMenuContentColumn {
                TransactionMenuItems(
                    onEditClick = onEditClick,
                    onDeleteClick = { deletionDialogVisible.value = true }
                )
            }
        },
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                TransferDetailScreenHeader(
                    onBackClick = onBackClick,
                    onMenuClick = { toggleMenuState(coroutineScope, bottomSheetMenuState) },
                )
            },
        ) { paddingValues ->
            if (deletionDialogVisible.value) {
                TransactionDeleteDialog(visible = deletionDialogVisible, onDeleteConfirm = onDeleteConfirm)
            }
            when {
                viewState.loading -> LoadingScreen()
                viewState.error != null -> ErrorScreen(error = viewState.error)
                else -> TransferDetailScreenContent(
                    viewState = viewState,
                    userId = userId,
                    paddingValues = paddingValues,
                )
            }
        }
    }
}

@Composable
private fun TransferDetailScreenContent(
    viewState: TransferDetailViewState,
    userId: String?,
    paddingValues: PaddingValues,
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .screenContentModifier(paddingValues)
            .verticalScroll(scrollState)
    ) {
        viewState.transfer?.let { transfer ->
            val textModifier = Modifier.padding(vertical = 5.dp)
            ContentTopSpacer()
            Text(
                text = transfer.fullAmount.mapAbsToString(),
                style = MaterialTheme.typography.h5,
                fontWeight = FontWeight.Bold,
                modifier = textModifier,
            )
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    text = transfer.time.toPrettyStringDate(leadingZeroes = false),
                    fontWeight = FontWeight.Bold,
                    modifier = textModifier,
                )
            }
            transfer.note?.nullIfBlank()?.let { note ->
                Text(text = note, modifier = textModifier)
            }
            Text(
                text = buildParticipantText(
                    prefix = stringResource(R.string.from_label),
                    participant = transfer.from,
                    userParticipantId = viewState.userParticipantId,
                ),
                modifier = textModifier,
            )
            Text(
                text = buildParticipantText(
                    prefix = stringResource(R.string.to_label),
                    participant = transfer.to,
                    userParticipantId = viewState.userParticipantId,
                ),
                modifier = textModifier,
            )
            OptionalCommentsColumn(comments = transfer.comments, userId = userId)
        }
    }
}

@Composable
private fun buildParticipantText(
    prefix: String,
    participant: Participant,
    userParticipantId: String?,
): AnnotatedString = buildAnnotatedString {
    append(prefix)
    if (participant.participantId == userParticipantId) {
        withStyle(SpanStyle(fontSize = MaterialTheme.typography.h6.fontSize, fontWeight = FontWeight.Bold)) {
            append(" " + stringResource(R.string.you))
        }
    } else {
        withStyle(SpanStyle(fontSize = MaterialTheme.typography.h6.fontSize)) {
            append(" " + participant.nickname)
        }
    }
}

@Composable
private fun TransferDetailScreenHeader(
    onBackClick: () -> Unit,
    onMenuClick: () -> Unit,
) {
    ScreenHeader(
        headingText = stringResource(R.string.transfer_detail),
        leftIcon = { HeaderBackIcon(onClick = onBackClick) },
        rightIcons = listOf {
            HeaderMenuIcon(onClick = onMenuClick)
        },
    )
}
