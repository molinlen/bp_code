package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Dialog

@Composable
fun LoadingScreen(
    modifier: Modifier = Modifier,
    fillMaxSize: Boolean = true,
) = LoadingBox(modifier = modifier, fillMaxSize = fillMaxSize)

@Composable
fun LoadingBox(
    modifier: Modifier = Modifier,
    fillMaxSize: Boolean = true,
) {
    Box(
        modifier = modifier
            .alsoIf(fillMaxSize) { it.fillMaxSize() },
        contentAlignment = Alignment.Center,
    ) {
        CircularProgressIndicator(
            color = MaterialTheme.colors.primaryVariant,
        )
    }
}

@Composable
fun LoadingOverlay(
    modifier: Modifier = Modifier,
    fillMaxSize: Boolean = true,
) {
    Dialog(onDismissRequest = { /* nothing */ }) {
        Box(
            modifier = modifier
                .alsoIf(fillMaxSize) { it.fillMaxSize() },
            contentAlignment = Alignment.Center,
        ) {
            CircularProgressIndicator(
                color = MaterialTheme.colors.primaryVariant,
            )
        }
    }
}
