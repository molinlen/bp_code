package cz.cvut.fit.molinlen.android.debttracker.feature.registration

import com.google.firebase.auth.FirebaseUser
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.RegistrationInput
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputProblem
import java.util.*

data class RegistrationViewState(
    val email: String = "",
    val password: String = "",
    val repeatedPassword: String = "",
    val checkboxChecked: Boolean = false,
    val inputProblems: EnumSet<RegistrationInputProblem> = EnumSet.noneOf(RegistrationInputProblem::class.java),
    val registrationResult: ProblemOr<FirebaseUser>? = null,
    val processing: Boolean = false,
) {
    fun toRegistrationInput() = RegistrationInput(
        email = email,
        passwordFirst = password,
        passwordRepeated = repeatedPassword,
        termsAccepted = checkboxChecked,
    )
}
