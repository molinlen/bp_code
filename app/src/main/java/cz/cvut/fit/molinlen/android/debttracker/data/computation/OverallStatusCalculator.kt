package cz.cvut.fit.molinlen.android.debttracker.data.computation

import cz.cvut.fit.molinlen.android.debttracker.data.model.*

interface OverallStatusCalculator {
    suspend fun calculateOverallUserStatusFromPreview(
        usersGroups: List<UsersGroupPreview>,
        currency: CurrencyCode,
    ): ProblemOr<GlobalOwedSumsOverview>
    //todo cache calculations from detail for later use?
}
