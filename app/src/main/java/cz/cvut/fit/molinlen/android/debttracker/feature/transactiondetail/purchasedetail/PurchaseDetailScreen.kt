package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.purchasedetail

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.OptionalCommentsColumn
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.TransactionDeleteDialog
import cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail.TransactionMenuItems
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun PurchaseDetailScreen(
    groupId: String,
    purchaseId: String,
    navigateAfterBackClick: () -> Unit,
    navigateAfterEditClick: () -> Unit,
    navigateAfterDeleteClick: () -> Unit,
) {
    val viewModel: PurchaseDetailViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId = groupId, purchaseId = purchaseId)
    }

    PurchaseDetailScreenImpl(
        userId = viewModel.userId,
        onBackClick = navigateAfterBackClick,
        onEditClick = navigateAfterEditClick,
        onDeleteConfirm = { viewModel.deletePurchase(navigateAfterDeleteClick) },
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@OptIn(ExperimentalMaterialApi::class) // for BottomSheet
@Composable
fun PurchaseDetailScreenImpl(
    userId: String?,
    onBackClick: () -> Unit,
    onEditClick: () -> Unit,
    onDeleteConfirm: () -> Unit,
    viewState: PurchaseDetailViewState,
) {
    val bottomSheetMenuState = rememberBottomSheetMenuState()
    val coroutineScope = rememberCoroutineScope()
    val deletionDialogVisible = remember { mutableStateOf(false) }
    BottomSheetMenu(
        sheetState = bottomSheetMenuState,
        coroutineScope = coroutineScope,
        menuContent = {
            BottomSheetMenuContentColumn {
                TransactionMenuItems(
                    onEditClick = onEditClick,
                    onDeleteClick = { deletionDialogVisible.value = true }
                )
            }
        },
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                PurchaseDetailScreenHeader(
                    onMenuClick = { toggleMenuState(coroutineScope, bottomSheetMenuState) },
                    onBackClick = onBackClick,
                )
            },
        ) { paddingValues ->
            if (deletionDialogVisible.value) {
                TransactionDeleteDialog(visible = deletionDialogVisible, onDeleteConfirm = onDeleteConfirm)
            }
            when {
                viewState.loading -> LoadingScreen()
                viewState.error != null -> ErrorScreen(error = viewState.error)
                else -> PurchaseDetailScreenContent(
                    viewState = viewState,
                    userId = userId,
                    paddingValues = paddingValues,
                )
            }
        }
    }
}

@Composable
private fun PurchaseDetailScreenContent(
    viewState: PurchaseDetailViewState,
    userId: String?,
    paddingValues: PaddingValues,
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .screenContentModifier(paddingValues)
            .verticalScroll(scrollState)
    ) {
        viewState.purchase?.let { purchase ->
            val textModifier = Modifier.padding(vertical = 3.dp)
            ContentTopSpacer()
            Row {
                Column(
                    modifier = Modifier.weight(1f), // will be laid out AFTER unweighted elements
                ) {
                    purchase.name?.nullIfBlank()?.let { name ->
                        Text(
                            text = name,
                            style = MaterialTheme.typography.h6,
                            modifier = textModifier,
                        )
                    }
                    CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                        Text(
                            text = purchase.time.toPrettyStringDate(leadingZeroes = false),
                            fontWeight = FontWeight.Bold,
                            modifier = textModifier,
                        )
                    }
                }
                Text(
                    text = purchase.fullAmount.mapAbsToString(),
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    modifier = textModifier.padding(start = 5.dp),
                )
            }
            purchase.note?.nullIfBlank()?.let { note ->
                Text(text = note, modifier = textModifier)
            }
            Column(
                modifier = Modifier.padding(vertical = 5.dp),
            ) {
                Card(
                    backgroundColor = MaterialTheme.colors.primary.copy(alpha = 0.45f),
                    elevation = 0.dp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 3.dp),
                ) {
                    Column(
                        modifier = Modifier.padding(5.dp)
                    ) {
                        PaidBySection(payers = purchase.payers, userParticipantId = viewState.userParticipantId)
                    }
                }
                Card(
                    backgroundColor = MaterialTheme.colors.error.copy(alpha = 0.45f),
                    elevation = 0.dp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 3.dp),
                ) {
                    Column(
                        modifier = Modifier.padding(5.dp)
                    ) {
                        SplittingSection(
                            splitting = purchase.splitting,
                            totalAmount = purchase.fullAmount,
                            userParticipantId = viewState.userParticipantId,
                        )
                    }
                }
            }
            OptionalCommentsColumn(comments = purchase.comments, userId = userId)
        }
    }
}

@Composable
private fun PaidBySection(
    payers: List<Payer>,
    userParticipantId: String?,
) {
    Text(text = stringResource(R.string.by_label), style = MaterialTheme.typography.h6)
    if (payers.size == 1) {
        Text(
            text = mapNickname(
                participant = payers.first().participant,
                userParticipantId = userParticipantId,
                addColon = false,
            ),
            modifier = Modifier.participantParticipationRow(),
        )
    } else {
        payers.map { payer ->
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.participantParticipationRow(),
            ) {
                Text(
                    text = mapNickname(
                        participant = payer.participant,
                        userParticipantId = userParticipantId,
                    ),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 5.dp),
                )
                Text(
                    text = payer.amount.mapAbsToString(),
                    fontWeight = when (payer.participant.participantId) {
                        userParticipantId -> FontWeight.Bold
                        else -> FontWeight.Normal
                    }
                )
            }
        }
    }
}

private fun Modifier.participantParticipationRow(topPadding: Dp = 5.dp, horizontalPadding: Dp = 10.dp): Modifier = this
    .fillMaxWidth()
    .padding(top = topPadding, start = horizontalPadding, end = horizontalPadding)

@Composable
private fun SplittingSection(
    splitting: TransactionSplittingStrategy,
    totalAmount: Amount,
    userParticipantId: String?,
) {
    Text(text = stringResource(R.string.for_label), style = MaterialTheme.typography.h6)
    when (splitting) {
        is TransactionSplittingExplicit -> {
            if (splitting.sharers.size == 1) {
                Text(
                    text = mapNickname(
                        participant = splitting.sharers.first().participant,
                        userParticipantId = userParticipantId,
                        addColon = false,
                    ),
                    modifier = Modifier.participantParticipationRow(),
                )
            } else {
                splitting.sharers.map { sharer ->
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.participantParticipationRow(),
                    ) {
                        Text(
                            text = mapNickname(participant = sharer.participant, userParticipantId = userParticipantId),
                            overflow = TextOverflow.Ellipsis,
                            maxLines = 1,
                            modifier = Modifier.weight(1f),
                        )
                        Text(
                            text = createAmount(sharer.shareValue, totalAmount.currency).mapAbsToString(),
                            fontWeight = when (sharer.participant.participantId) {
                                userParticipantId -> FontWeight.Bold
                                else -> FontWeight.Normal
                            }
                        )
                    }
                }
            }
        }
        is TransactionSplittingPortion -> {
            if (splitting.sharers.size == 1) {
                Text(
                    text = mapNickname(
                        participant = splitting.sharers.first().participant,
                        userParticipantId = userParticipantId,
                        addColon = false,
                    ),
                    modifier = Modifier.participantParticipationRow(),
                )
            } else {
                val maxAmountTextLength = splitting.sharers
                    .map { share ->
                        share.shareValueComputed?.let { createAmount(it, totalAmount.currency).mapAbsToString() } ?: ""
                    }.maxOf { it.length }
                splitting.sharers.map { sharer ->
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.participantParticipationRow(),
                    ) {
                        Text(
                            text = mapNickname(participant = sharer.participant, userParticipantId = userParticipantId),
                            overflow = TextOverflow.Ellipsis,
                            maxLines = 1,
                            modifier = Modifier.weight(1f)
                        )
                        Text(
                            text = sharer.shareValue.toString(),
                            textAlign = TextAlign.End,
                            fontWeight = when (sharer.participant.participantId) {
                                userParticipantId -> FontWeight.Bold
                                else -> FontWeight.Normal
                            },
                            modifier = Modifier.padding(horizontal = 10.dp)
                        )
                        val amountText = sharer.shareValueComputed
                            ?.let { createAmount(it, totalAmount.currency).mapAbsToString() } ?: ""
                        Text(
                            text = buildAnnotatedString {
                                append("  ".repeatIfPositive(maxAmountTextLength - amountText.length))
                                withStyle(SpanStyle(fontWeight = if (sharer.participant.participantId == userParticipantId) FontWeight.Medium else FontWeight.Light)) {
                                    append(amountText)
                                }
                            },
                            textAlign = TextAlign.End,
                            fontStyle = FontStyle.Italic,
                        )
                    }
                }
                Text(
                    text = stringResource(R.string.portion_splitting_little_label),
                    style = MaterialTheme.typography.caption,
                    textAlign = TextAlign.End,
                    modifier = Modifier.participantParticipationRow(topPadding = 2.dp)
                )
            }
        }
    }
}

@Composable
private fun mapNickname(
    participant: Participant,
    userParticipantId: String?,
    addColon: Boolean = true,
): AnnotatedString = buildAnnotatedString {
    if (participant.participantId == userParticipantId) {
        withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
            append(stringResource(R.string.you))
            if (addColon) {
                append(':')
            }
        }
    } else {
        append(participant.nickname)
        if (addColon) {
            append(':')
        }
    }
}

@Composable
private fun PurchaseDetailScreenHeader(
    onMenuClick: () -> Unit,
    onBackClick: () -> Unit,
) {
    ScreenHeader(
        headingText = stringResource(R.string.purchase_detail),
        leftIcon = { HeaderBackIcon(onClick = onBackClick) },
        rightIcons = listOf {
            HeaderMenuIcon(onClick = onMenuClick)
        },
    )
}
