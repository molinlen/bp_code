package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.purchaseedit

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.EditMode
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.CurrencyPicker
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.DatePickerRow
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.TransactionSaveErrorCard
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.SpecificShapes
import java.math.BigDecimal
import java.time.OffsetDateTime

@Composable
fun PurchaseEditScreen(
    groupId: String,
    purchaseId: String?,
    navigateOnExit: () -> Unit,
) {
    val viewModel: PurchaseEditViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId = groupId, purchaseId = purchaseId)
    }

    PurchaseEditScreenImpl(
        screenMode = EditMode.fromId(purchaseId),
        viewState = viewModel.viewState.collectAsState().value,
        canSave = viewModel.canSave(),
        onUpdateTitle = { viewModel.updateName(it) },
        onUpdateNote = { viewModel.updateNote(it) },
        onUpdateCurrency = { viewModel.updateCurrency(it) },
        onUpdateDate = { year, month, dayOfMonth ->
            viewModel.updateDate(year = year, month = month, dayOfMonth = dayOfMonth)
        },
        onPayerChange = { old: PayerInputData, updated: PayerInputData ->
            viewModel.updatePayer(old = old, updated = updated)
        },
        onPayerAdd = { viewModel.addPayer(it) },
        onPayerRemove = { viewModel.removePayer(it) },
        onSplittingTypeChange = { viewModel.updateSplittingType(it) },
        onSharerUpdate = { old: TransactionParticipantBasicInputData, updated: TransactionParticipantBasicInputData ->
            viewModel.updateSharer(old = old, updated = updated)
        },
        onSplitEquallyClick = { viewModel.splitAmountEqually() },
        onSelectAllClick = { viewModel.markAllAsSharersAsSelected(selected = true) },
        onUnSelectAllClick = { viewModel.markAllAsSharersAsSelected(selected = false) },
        onSave = { viewModel.save(navigateOnExit) },
        onDiscard = navigateOnExit,
        onClose = navigateOnExit,
    )
}

@Composable
fun PurchaseEditScreenImpl(
    screenMode: EditMode,
    viewState: PurchaseEditViewState,
    canSave: Boolean,
    onUpdateTitle: (String) -> Unit,
    onUpdateNote: (String) -> Unit,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onUpdateDate: (year: Int, month: Int, dayOfMonth: Int) -> Unit,
    onPayerChange: (old: PayerInputData, updated: PayerInputData) -> Unit,
    onPayerAdd: (Participant) -> Unit,
    onPayerRemove: (Participant) -> Unit,
    onSplittingTypeChange: (SplittingType) -> Unit,
    onSharerUpdate: (old: TransactionParticipantBasicInputData, updated: TransactionParticipantBasicInputData) -> Unit,
    onSplitEquallyClick: () -> Unit,
    onSelectAllClick: () -> Unit,
    onUnSelectAllClick: () -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    onClose: () -> Unit,
) {
    val scrollState = rememberScrollState()

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = when (screenMode) {
                    EditMode.CREATE -> stringResource(R.string.create_purchase)
                    EditMode.EDIT -> stringResource(R.string.edit_purchase)
                },
                leftIcon = { HeaderCloseIcon(onClick = onClose) },
                backgroundColor = ScreenHeaderColors.editScreenColor,
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> Column(
                modifier = Modifier
                    .screenContentModifier(paddingValues)
                    .verticalScroll(scrollState)
                    .clearFocusOnTap()
            ) {
                PurchaseEditScreenContent(
                    viewState = viewState,
                    screenMode = screenMode,
                    canSave = canSave,
                    onUpdateTitle = onUpdateTitle,
                    onUpdateNote = onUpdateNote,
                    onUpdateDate = onUpdateDate,
                    onPayerChange = onPayerChange,
                    onPayerRemove = onPayerRemove,
                    onPayerAdd = onPayerAdd,
                    onUpdateCurrency = onUpdateCurrency,
                    onSplittingTypeChange = onSplittingTypeChange,
                    onSharerUpdate = onSharerUpdate,
                    onSplitEquallyClick = onSplitEquallyClick,
                    onSelectAllClick = onSelectAllClick,
                    onUnSelectAllClick = onUnSelectAllClick,
                    onSave = onSave,
                    onDiscard = onDiscard,
                )
            }
        }
    }
}

@Composable
private fun PurchaseEditScreenContent(
    viewState: PurchaseEditViewState,
    screenMode: EditMode,
    canSave: Boolean,
    onUpdateTitle: (String) -> Unit,
    onUpdateNote: (String) -> Unit,
    onUpdateDate: (year: Int, month: Int, dayOfMonth: Int) -> Unit,
    onPayerChange: (old: PayerInputData, updated: PayerInputData) -> Unit,
    onPayerRemove: (Participant) -> Unit,
    onPayerAdd: (Participant) -> Unit,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onSplittingTypeChange: (SplittingType) -> Unit,
    onSharerUpdate: (old: TransactionParticipantBasicInputData, updated: TransactionParticipantBasicInputData) -> Unit,
    onSplitEquallyClick: () -> Unit,
    onSelectAllClick: () -> Unit,
    onUnSelectAllClick: () -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
) {
    val datePickerExpanded = remember { mutableStateOf(false) }
    val datePickerDialog = datePickerDialog(datePickerExpanded = datePickerExpanded, onUpdateDate = onUpdateDate)
    val spacerModifier = Modifier.padding(10.dp)
    viewState.purchase?.let { purchase ->
        ContentTopSpacer()
        TitleAndNoteTextFields(purchase = purchase, onUpdateTitle = onUpdateTitle, onUpdateNote = onUpdateNote)
        DatePickerRow(
            datePickerExpanded = datePickerExpanded,
            time = purchase.time,
            modifier = Modifier.padding(vertical = 2.dp),
            textModifier = Modifier.padding(end = 15.dp),
        )
        Spacer(modifier = spacerModifier)
        PaidBySection(
            participants = viewState.participants,
            purchase = purchase,
            usersParticipantId = viewState.usersParticipantId,
            onPayerChange = onPayerChange,
            onPayerRemove = onPayerRemove,
            onPayerAdd = onPayerAdd,
        )
        TotalAmountAndCurrencyRow(
            totalAmount = purchase.totalAmount,
            onUpdateCurrency = onUpdateCurrency,
        )
        Spacer(modifier = spacerModifier)
        PaidForSection(
            purchase = purchase,
            usersParticipantId = viewState.usersParticipantId,
            onSplittingTypeChange = onSplittingTypeChange,
            onSharerUpdate = onSharerUpdate,
            onSplitEquallyClick = onSplitEquallyClick,
            onSelectAllClick = onSelectAllClick,
            onUnSelectAllClick = onUnSelectAllClick,
        )
        if (purchase.totalAmount.value <= BigDecimal.ZERO) {
            MissingPaidAmountPrompt()
        }
        viewState.neededToSplit()?.let { neededToSplit ->
            SplitAdjustmentNeededPrompt(neededToSplit = neededToSplit, currency = purchase.currency)
        }
        if (datePickerExpanded.value) {
            DatePickerDialogShow(
                time = purchase.time,
                dialog = datePickerDialog
            )
        }
        viewState.saveError?.let { problem ->
            TransactionSaveErrorCard(problem)
        }
        SaveAndDiscardButtons(
            mode = screenMode,
            saveEnabled = canSave,
            onSave = onSave,
            onDiscard = onDiscard,
        )
    }
}

@Composable
private fun TitleAndNoteTextFields(
    purchase: PurchaseDetailInputData,
    onUpdateTitle: (String) -> Unit,
    onUpdateNote: (String) -> Unit,
) {
    BasicTextField(
        label = stringResource(R.string.title),
        value = purchase.name ?: "",
        keyboardOptions = KeyboardOptions(
            capitalization = KeyboardCapitalization.Sentences, imeAction = ImeAction.Next,
        ),
        onValueChange = onUpdateTitle,
    )
    BasicTextField(
        label = stringResource(R.string.note),
        value = purchase.note ?: "",
        keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
        singleLine = false,
        onValueChange = onUpdateNote,
    )
}

@Composable
private fun PaidBySection(
    participants: Set<Participant>,
    purchase: PurchaseDetailInputData,
    usersParticipantId: String?,
    onPayerChange: (old: PayerInputData, updated: PayerInputData) -> Unit,
    onPayerRemove: (Participant) -> Unit,
    onPayerAdd: (Participant) -> Unit,
) {
    Text(text = stringResource(R.string.paid_by_label), style = MaterialTheme.typography.h5)
    val otherEligiblePayers = (participants - purchase.payers.map { it.participant }.toSet())
        .sortedBy { it.nickname }
    purchase.payers.map { payer ->
        PayerRow(
            payerData = payer,
            eligiblePayers = otherEligiblePayers,
            userParticipantId = usersParticipantId,
            onOtherParticipantPick = { onPayerChange(payer, payer.copy(participant = it)) },
            onAmountValueChange = { onPayerChange(payer, payer.copy(amountValueString = it)) },
            onRemoveClick = { onPayerRemove(payer.participant) },
        )
    }
    if (otherEligiblePayers.isNotEmpty()) {
        PayerRow(
            payerData = null,
            eligiblePayers = otherEligiblePayers,
            userParticipantId = usersParticipantId,
            isParticipantError = purchase.payers.isEmpty(),
            onOtherParticipantPick = { onPayerAdd(it) },
        )
    }
}

@Composable
private fun TotalAmountAndCurrencyRow(
    totalAmount: Amount,
    onUpdateCurrency: (CurrencyCode) -> Unit,
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth(),
    ) {
        Text(text = stringResource(R.string.total_label))
        Row(
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = totalAmount.value.toPlainString(),
                fontWeight = FontWeight.Bold,
                color = if (totalAmount.value <= BigDecimal.ZERO) AccentColors.redContent else Color.Unspecified,
                modifier = Modifier.padding(10.dp),
            )
            val currencyPickerExpanded = remember { mutableStateOf(false) }
            CurrencyPicker(
                currencyPickerExpanded = currencyPickerExpanded,
                pickedCurrency = totalAmount.currency,
                onUpdateCurrency = onUpdateCurrency,
                modifier = Modifier.fillMaxWidth(0.5f),
            )
        }
    }
}

private fun SplittingType.toIndex(): Int = when (this) {
    SplittingType.PORTION -> 0
    SplittingType.EXPLICIT -> 1
}

val types = SplittingType.values().sortedBy { it.toIndex() }

@Composable
private fun SplittingType.headingText(): String = when (this) {
    SplittingType.EXPLICIT -> stringResource(R.string.purchase_edit_specific_amounts_tab)
    SplittingType.PORTION -> stringResource(R.string.purchase_edit_portions_tab)
}

@Composable
private fun PaidForSection(
    purchase: PurchaseDetailInputData,
    usersParticipantId: String?,
    onSplittingTypeChange: (SplittingType) -> Unit,
    onSharerUpdate: (old: TransactionParticipantBasicInputData, updated: TransactionParticipantBasicInputData) -> Unit,
    onSplitEquallyClick: () -> Unit,
    onSelectAllClick: () -> Unit,
    onUnSelectAllClick: () -> Unit,
) {
    Text(text = stringResource(R.string.paid_for_label), style = MaterialTheme.typography.h5)
    Spacer(modifier = Modifier.padding(top = 3.dp))
    val tabBaseColor = AccentColors.surfaceAccentMild
    val tabBorderThickness = 3.dp
    TabRow(
        selectedTabIndex = purchase.splitting.selectedType.toIndex(),
        backgroundColor = tabBaseColor,
        divider = {},
        indicator = {},
        modifier = Modifier
            .clip(shape = SpecificShapes.tab) // so that upper row edges are not sharp
            .border(width = tabBorderThickness, color = tabBaseColor, shape = SpecificShapes.tab)
    ) {
        types.forEach { splittingType ->
            val selected = purchase.splitting.selectedType == splittingType
            Tab(
                selected = selected,
                text = { Text(text = splittingType.headingText()) },
                onClick = { onSplittingTypeChange(splittingType) },
                modifier = Modifier
                    .clip(shape = SpecificShapes.tab) // so that clickable effect does not spread to upper edges
                    .alsoIf(selected) {
                        it.background(color = MaterialTheme.colors.background, shape = SpecificShapes.tab)
                    }
            )
        }
    }
    Box(contentAlignment = Alignment.TopEnd) {
        val checkboxState = when {
            purchase.splitting.shares.all { it.selected } -> ToggleableState.On
            purchase.splitting.shares.all { it.selected.not() } -> ToggleableState.Off
            else -> ToggleableState.Indeterminate
        }
        BasicTriStateCheckbox(
            state = checkboxState,
            onClick = {
                when (checkboxState) {
                    ToggleableState.On -> onUnSelectAllClick()
                    ToggleableState.Off -> onSelectAllClick()
                    ToggleableState.Indeterminate -> onSelectAllClick()
                }
            },
            colors = alternateCheckboxColors(),
            modifier = Modifier.offset(y = (-6).dp),
        )
        Column {
            Spacer(modifier = Modifier.padding(top = 24.dp))
            purchase.splitting.shares.map { share ->
                SharerRow(
                    splittingType = purchase.splitting.selectedType,
                    share = share,
                    userParticipantId = usersParticipantId,
                    onValueChange = when {
                        share.selected -> when (purchase.splitting.selectedType) {
                            SplittingType.EXPLICIT -> {
                                { onSharerUpdate(share, share.copy(explicitAmountValueString = it)) }
                            }
                            SplittingType.PORTION -> {
                                { onSharerUpdate(share, share.copy(portionValueString = it)) }
                            }
                        }
                        else -> null
                    },
                    onCheckedChange = { onSharerUpdate(share, share.copy(selected = it)) },
                )
            }
        }
    }
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxWidth(),
    ) {
        BasicRectangleTextButton(
            text = stringResource(R.string.split_equally),
            iconPainter = painterResource(R.drawable.shuffle),
            onClick = onSplitEquallyClick,
            buttonColors = alternativeTextButtonColors(),
//            modifier = Modifier.fillMaxWidth()
        )
    }
    Divider(color = tabBaseColor, thickness = tabBorderThickness, modifier = Modifier.padding(vertical = 5.dp))
}

@Composable
private fun MissingPaidAmountPrompt() {
    InvalidInputPrompt(text = stringResource(R.string.no_paid_amount_was_input))
}

@Composable
private fun SplitAdjustmentNeededPrompt(
    neededToSplit: BigDecimal,
    currency: CurrencyCode,
) {
    val amountText = createAmount(neededToSplit, currency).mapAbsToString()
    InvalidInputPrompt(
        text = when {
            neededToSplit > BigDecimal.ZERO -> stringResource(R.string.purchase_splitting_less_shared_prompt) + amountText
            neededToSplit < BigDecimal.ZERO -> stringResource(R.string.purchase_splitting_more_shared_prompt_1) +
                    amountText + stringResource(R.string.purchase_splitting_more_shared_prompt_2)
            else -> stringResource(R.string.no_participants_in_edit_purchase_prompt)
        },
    )
}

@Composable
private fun InvalidInputPrompt(
    text: String,
) {
    Text(
        text = text,
        fontWeight = FontWeight.Bold,
        color = AccentColors.redContent,
    )
}

@Composable
private fun PayerRow(
    payerData: PayerInputData?,
    eligiblePayers: List<Participant>,
    userParticipantId: String?,
    isParticipantError: Boolean = false,
    onOtherParticipantPick: (Participant) -> Unit,
    onAmountValueChange: (String) -> Unit = {},
    onRemoveClick: () -> Unit = {},
) {
    val participantPickerExpanded = remember { mutableStateOf(false) }
    val pickedIsUser = payerData?.participant?.participantId == userParticipantId && payerData != null
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth(),
    ) {
        DropDownPicker(
            menuExpanded = participantPickerExpanded,
            pickedText = if (pickedIsUser) stringResource(R.string.you) else payerData?.participant?.nickname ?: "...",
            textWeight = if (pickedIsUser) FontWeight.Bold else FontWeight.Normal,
            isError = isParticipantError,
            contentDescription = stringResource(R.string.pick_payer),
            modifier = Modifier.fillMaxWidth(0.5f)
        ) {
            eligiblePayers.map {
                DropdownMenuItem(
                    onClick = {
                        onOtherParticipantPick(it)
                        participantPickerExpanded.value = false
                    },
                ) {
                    if (it.participantId == userParticipantId) {
                        Text(text = stringResource(R.string.you), fontWeight = FontWeight.Bold)
                    } else {
                        Text(
                            text = it.nickname,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                        )
                    }
                }
            }
        }
        payerData?.let {
            BasicTextField(
                label = stringResource(R.string.amount),
                value = it.amountValueString,
                onValueChange = onAmountValueChange,
                isError = it.amountValue <= BigDecimal.ZERO,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Next),
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 5.dp),
            )
            IconButton(onClick = onRemoveClick) {
                Icon(imageVector = Icons.Default.Close, contentDescription = stringResource(R.string.remove_payer))
            }
        }
    }
}

@Composable
private fun SharerRow(
    splittingType: SplittingType,
    share: TransactionParticipantBasicInput,
    userParticipantId: String?,
    onValueChange: ((String) -> Unit)?,
    onCheckedChange: (Boolean) -> Unit,
) {
    val isUser = share.participant.participantId == userParticipantId
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = if (isUser) stringResource(R.string.you) else share.participant.nickname,
            fontWeight = if (isUser) FontWeight.Bold else FontWeight.Normal,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .fillMaxWidth(when (splittingType) {
                    SplittingType.EXPLICIT -> 0.5f
                    SplittingType.PORTION -> 0.4f
                })
                .padding(start = 15.dp, end = 5.dp),
        )
        BasicTextField(
            label = when (splittingType) {
                SplittingType.EXPLICIT -> stringResource(R.string.amount)
                SplittingType.PORTION -> stringResource(R.string.portion)
            },
            value = when (splittingType) {
                SplittingType.EXPLICIT -> share.explicitAmountValueString
                SplittingType.PORTION -> share.portionValueString
            },
            onValueChange = onValueChange ?: {},
            enabled = onValueChange != null,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Next),
            modifier = Modifier
                .weight(4f)
                .padding(start = 5.dp),
        )
        if (splittingType == SplittingType.PORTION) {
            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                Text(
                    text = if (share.selected) share.portionShareResult.toPlainString() else "",
                    textAlign = TextAlign.End,
                    fontStyle = FontStyle.Italic,
                    fontWeight = FontWeight.Light,
                    modifier = Modifier
                        .weight(3f)
                        .padding(start = 5.dp),
                )
            }
        }
        BasicCheckbox(
            checked = share.selected,
            onCheckedChange = onCheckedChange,
        )
    }
}

@Preview
@Composable
private fun Preview() {
    PurchaseEditScreenImpl(
        screenMode = EditMode.EDIT,
        viewState = PurchaseEditViewState(
            purchase = PurchaseDetailInputData(
                currency = CurrencyCode.EUR,
                payers = listOf(
                    PayerInputData(
                        participant = ParticipantBasic(participantId = "1", nickname = "First Payer"),
                        amountValue = createAmount(10.toBigDecimal(), CurrencyCode.EUR).value,
                        amountValueString = createAmount(10.toBigDecimal(), CurrencyCode.EUR).value.toPlainString(),
                    ),
                ),
                splitting = TransactionSplittingInput(
                    selectedType = SplittingType.PORTION,
                    shares = listOf(
                        TransactionParticipantBasicInputData(
                            participant = ParticipantBasic("a", "First sharer"),
                            selected = true,
                            explicitAmountValue = createAmount(10.toBigDecimal(), CurrencyCode.EUR).value,
                            explicitAmountValueString = createAmount(10.toBigDecimal(),
                                CurrencyCode.EUR).value.toPlainString(),
                            portionValue = 23,
                            portionValueString = "23",
                            portionShareResult = createAmount(10.toBigDecimal(), CurrencyCode.EUR).value,
                        ),
                    ),
                ),
                time = OffsetDateTime.now(),
                name = "",
                note = "",
                transactionId = null,
            ),
        ),
        canSave = true,
        onUpdateTitle = {},
        onUpdateNote = {},
        onUpdateCurrency = {},
        onUpdateDate = { _: Int, _: Int, _: Int -> },
        onPayerChange = { _: PayerInputData, _: PayerInputData -> },
        onPayerAdd = {},
        onPayerRemove = {},
        onSplittingTypeChange = {},
        onSharerUpdate = { _: TransactionParticipantBasicInputData, _: TransactionParticipantBasicInputData -> },
        onSplitEquallyClick = { },
        onSelectAllClick = { },
        onUnSelectAllClick = { },
        onSave = { },
        onDiscard = { },
        onClose = {},
    )
}
