package cz.cvut.fit.molinlen.android.debttracker.data.computation

import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.math.BigDecimal
import java.math.RoundingMode

interface AmountSplitter {
    fun splitAmount(amount: Amount, between: Long): ProblemOr<AmountWithCarry>
    fun splitAmount(amount: Amount, between: Int): ProblemOr<AmountWithCarry> = splitAmount(
        amount = amount,
        between = between.toLong(),
    )

    fun splitAmountToPortions(amount: Amount, portions: List<Int>): ProblemOr<AmountPortionsWithCarry>
}

object AmountSplitterImpl : AmountSplitter {
    override fun splitAmount(amount: Amount, between: Long): ProblemOr<AmountWithCarry> {
        if (between == 0L) {
            return Problem("Division by zero").left()
        }
        val each = amount.value.divide(BigDecimal.valueOf(between), amount.currency.precision, RoundingMode.DOWN)
        return AmountWithCarry(
            amount = amount.copyAmount(value = each),
            carry = amount.value.minus(each.times(BigDecimal.valueOf(between))),
        ).right()
    }

    override fun splitAmountToPortions(amount: Amount, portions: List<Int>): ProblemOr<AmountPortionsWithCarry> {
        val portionsSum = portions.sum()
        if (portionsSum == 0) {
            return Problem("Division by zero").left()
        }

        val divisor = portionsSum.toBigDecimal().scaleFor(amount.currency)
        val results = portions.toSet().associateWith { portion ->
            createAmount(
                value = amount.value
                    .times(portion.toBigDecimal())
                    .divide(divisor, amount.currency.precision, RoundingMode.DOWN),
                currency = amount.currency,
            )
        }
        return AmountPortionsWithCarry(
            portionResults = results,
            carry = amount.value.minus(portions.map { results[it] }.sumOf { it?.value ?: BigDecimal.ZERO })
        ).right()
    }
}
