package cz.cvut.fit.molinlen.android.debttracker.data.repository.db

import android.util.Log
import arrow.core.Either
import arrow.core.left
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.*
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.flow.Flow
import java.util.*

interface UserDb {
    fun getUserInfo(userId: String): Flow<ProblemOr<UserEntityData>>

    fun getUsersGroupParticipants(userId: String): Flow<ProblemOr<List<UsersGroupEntityData>>>

    fun getUsersGroupParticipant(userId: String, groupId: String): Flow<ProblemOr<UsersGroupEntityData>>

    fun linkGroupSetParticipant(userId: String, groupId: String, userParticipantId: String?): Flow<ProblemOr<Unit>>

    fun unlinkUserFromGroup(userId: String, groupId: String): Flow<ProblemOr<Unit>>

    fun updateUserInfo(userId: String, userInfo: UserEntityUpdateData): Flow<ProblemOr<Unit>>
}

interface GroupDb {
    fun getGroupInfo(groupId: String): Flow<ProblemOr<GroupEntity>>

    fun getGroupParticipants(groupId: String): Flow<ProblemOr<List<ParticipantEntity>>>

    fun createGroup(groupData: GroupEntityData): Flow<ProblemOr<String>>

    fun updateGroup(group: GroupEntity): Flow<ProblemOr<Unit>>

    fun addParticipants(groupId: String, newParticipantsData: Set<ParticipantEntityData>): Flow<ProblemOr<Unit>>
}

interface PurchaseDb {
    fun getGroupPurchases(groupId: String): Flow<ProblemOr<List<PurchaseEntity>>>

    fun getPurchase(groupId: String, purchaseId: String): Flow<ProblemOr<PurchaseEntity>>

    fun createPurchase(groupId: String, purchaseData: PurchaseEntityData): Flow<ProblemOr<String>>

    fun updatePurchase(groupId: String, purchase: PurchaseEntity): Flow<ProblemOr<Unit>>

    fun deletePurchase(groupId: String, purchaseId: String): Flow<ProblemOr<Unit>>
}

interface TransferDb {
    fun getGroupTransfers(groupId: String): Flow<ProblemOr<List<TransferEntity>>>

    fun getTransfer(groupId: String, transferId: String): Flow<ProblemOr<TransferEntity>>

    fun createTransfer(groupId: String, transferData: TransferEntityData): Flow<ProblemOr<String>>

    fun updateTransfer(groupId: String, transfer: TransferEntity): Flow<ProblemOr<Unit>>

    fun deleteTransfer(groupId: String, transferId: String): Flow<ProblemOr<Unit>>
}

interface Db : UserDb, GroupDb, PurchaseDb, TransferDb

internal fun <T> ProducerScope<ProblemOr<T>>.sendProblemAndLogWarn(
    message: String,
    e: Exception? = null,
) {
    Log.w(APP_TAG, message, e)
    trySend(Problem(message, cause = e).left())
}

internal fun <T> ProducerScope<ProblemOr<T>>.sendProblemAndLogError(
    message: String,
    e: Exception? = null,
) {
    Log.e(APP_TAG, message, e)
    trySend(Problem(message, cause = e).left())
}

internal fun <T> ProducerScope<ProblemOr<T>>.sendProblemAndLogError(
    p: Problem,
    message: String? = null,
) {
    Log.e(APP_TAG, message ?: p.description, p.cause)
    trySend(p.left())
}

internal fun <T> ProducerScope<ProblemOr<T>>.tryToDeserialize(what: String, e: Exception?, deserialize: () -> Unit) {
    if (e != null) {
        sendProblemAndLogWarn(message = "Error getting $what", e = e)
    } else {
        Either.catch {
            deserialize()
        }.mapLeft { throwable ->
            sendProblemAndLogError(
                message = "Error deserializing $what",
                e = if (throwable is Exception) throwable else null,
            )
        }
    }
}

internal fun randomId(): String = UUID.randomUUID().toString()

