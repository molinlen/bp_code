package cz.cvut.fit.molinlen.android.debttracker.feature.login

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*

@Composable
fun LoginScreen(
    navigateAfterLogIn: () -> Unit,
    navigateAfterRegister: (LoginViewState) -> Unit,
) {
    val viewModel: LoginViewModel = viewModel()

    LoginScreenImpl(
        viewState = viewModel.viewState.collectAsState().value,
        onLogIn = { viewModel.logIn { navigateAfterLogIn() } },
        onRegister = navigateAfterRegister,
        updateEmail = { viewModel.updateEmail(it) },
        updatePassword = { viewModel.updatePassword(it) },
    )
}

@Composable
fun LoginScreenImpl(
    viewState: LoginViewState,
    onLogIn: () -> Unit,
    onRegister: (LoginViewState) -> Unit,
    updateEmail: (String) -> Unit,
    updatePassword: (String) -> Unit,
) {
    val showFailedLoginPrompt = viewState.loginResult?.isLeft() == true

    if (viewState.processing) {
        LoadingOverlay()
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .padding(horizontal = 15.dp)
            .fillMaxSize()
            .clearFocusOnTap()
    ) {
        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f),
        ) {
            Text(
                text = stringResource(R.string.app_name),
                style = MaterialTheme.typography.h3,
            )
        }
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Text(
                text = stringResource(R.string.login),
                style = MaterialTheme.typography.h5,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 2.dp),
            )
            BasicTextField(
                label = stringResource(R.string.email),
                value = viewState.email,
                onValueChange = updateEmail,
                modifier = Modifier.padding(vertical = 2.dp),
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
            )
            PasswordTextField(
                label = stringResource(R.string.password),
                value = viewState.password,
                onValueChange = updatePassword,
                modifier = Modifier.padding(vertical = 2.dp),
                keyboardActions = defaultKeyboardActions(onConfirmAlso = onLogIn)
            )
            Spacer(modifier = Modifier.padding(if (showFailedLoginPrompt) 3.dp else 15.dp))
            BasicRectangleButton(
                text = stringResource(R.string.log_in_prompt),
                onClick = onLogIn,
                enabled = viewState.password.isNotEmpty() && viewState.email.isNotBlank(),
                modifier = Modifier.padding(vertical = 2.dp),
            )
            when (showFailedLoginPrompt) {
                true -> ErrorCard(
                    text = stringResource(R.string.try_again_check_credentials_prompt),
                )
                false -> Spacer(modifier = Modifier.padding(25.dp))
            }
            Text(
                text = stringResource(R.string.registration_lead),
                modifier = Modifier.padding(top = 10.dp),
            )
            BasicRectangleButton(
                text = stringResource(R.string.register),
                onClick = { onRegister(viewState) },
                modifier = Modifier.padding(bottom = 12.dp),
            )
        }
    }
}
