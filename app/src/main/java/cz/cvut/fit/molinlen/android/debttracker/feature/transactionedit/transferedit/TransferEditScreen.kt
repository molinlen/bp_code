package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.transferedit

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.EditMode
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetailInputData
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.CurrencyPicker
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.DatePickerRow
import cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.TransactionSaveErrorCard
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import java.math.BigDecimal

@Composable
fun TransferEditScreen(
    groupId: String,
    transferId: String?,
    navigateOnExit: () -> Unit,
) {
    val viewModel: TransferEditViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData(groupId = groupId, transferId = transferId)
    }

    TransferEditScreenImpl(
        viewState = viewModel.viewState.collectAsState().value,
        screenMode = EditMode.fromId(transferId),
        canSave = viewModel.canSave(),
        onAmountInputChange = { viewModel.updateAmountValue(it) },
        onUpdateCurrency = { viewModel.updateAmountCurrency(it) },
        onUpdateFromParticipant = { viewModel.updateFrom(it) },
        onUpdateToParticipant = { viewModel.updateTo(it) },
        onUpdateNote = { viewModel.updateNote(it) },
        onUpdateDate = { year, month, dayOfMonth ->
            viewModel.updateDate(year = year, month = month, dayOfMonth = dayOfMonth)
        },
        onSave = { viewModel.save(navigateOnExit) },
        onDiscard = navigateOnExit,
        onClose = navigateOnExit,
    )
}

@Composable
fun TransferEditScreenImpl(
    viewState: TransferEditViewState,
    screenMode: EditMode,
    canSave: Boolean,
    onAmountInputChange: (String) -> Unit,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onUpdateFromParticipant: (Participant) -> Unit,
    onUpdateToParticipant: (Participant) -> Unit,
    onUpdateNote: (String) -> Unit,
    onUpdateDate: (year: Int, month: Int, dayOfMonth: Int) -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
    onClose: () -> Unit,
) {
    val scrollState = rememberScrollState()

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = when (screenMode) {
                    EditMode.CREATE -> stringResource(R.string.create_transfer)
                    EditMode.EDIT -> stringResource(R.string.edit_transfer)
                },
                leftIcon = { HeaderCloseIcon(onClick = onClose) },
                backgroundColor = ScreenHeaderColors.editScreenColor,
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        when {
            viewState.loading -> LoadingScreen()
            viewState.error != null -> ErrorScreen(error = viewState.error)
            else -> Column(
                modifier = Modifier
                    .screenContentModifier(paddingValues)
                    .verticalScroll(scrollState)
                    .clearFocusOnTap()
            ) {
                TransferEditScreenContent(
                    viewState = viewState,
                    screenMode = screenMode,
                    canSave = canSave,
                    onAmountInputChange = onAmountInputChange,
                    onUpdateCurrency = onUpdateCurrency,
                    onUpdateFromParticipant = onUpdateFromParticipant,
                    onUpdateToParticipant = onUpdateToParticipant,
                    onUpdateDate = onUpdateDate,
                    onUpdateNote = onUpdateNote,
                    onSave = onSave,
                    onDiscard = onDiscard,
                )
            }
        }
    }
}

@Composable
private fun TransferEditScreenContent(
    viewState: TransferEditViewState,
    screenMode: EditMode,
    canSave: Boolean,
    onAmountInputChange: (String) -> Unit,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    onUpdateFromParticipant: (Participant) -> Unit,
    onUpdateToParticipant: (Participant) -> Unit,
    onUpdateDate: (year: Int, month: Int, dayOfMonth: Int) -> Unit,
    onUpdateNote: (String) -> Unit,
    onSave: () -> Unit,
    onDiscard: () -> Unit,
) {
    val currencyPickerExpanded = remember { mutableStateOf(false) }
    val fromPickerExpanded = remember { mutableStateOf(false) }
    val toPickerExpanded = remember { mutableStateOf(false) }
    val datePickerExpanded = remember { mutableStateOf(false) }
    val datePickerDialog = datePickerDialog(datePickerExpanded = datePickerExpanded, onUpdateDate = onUpdateDate)
    viewState.transfer?.let { transfer ->
        ContentTopSpacer()
        AmountAndCurrencyRow(
            transfer = transfer,
            onAmountInputChange = onAmountInputChange,
            currencyPickerExpanded = currencyPickerExpanded,
            onUpdateCurrency = onUpdateCurrency,
        )
        ParticipantPickerRow(
            pickerExpanded = fromPickerExpanded,
            leadingText = stringResource(R.string.from_label),
            picked = transfer.from,
            participants = viewState.participants,
            userParticipantId = viewState.usersParticipantId,
            contentDescription = stringResource(R.string.pick_who_paid),
            onParticipantClick = onUpdateFromParticipant,
        )
        ParticipantPickerRow(
            pickerExpanded = toPickerExpanded,
            leadingText = stringResource(R.string.to_label),
            picked = transfer.to,
            participants = viewState.participants,
            userParticipantId = viewState.usersParticipantId,
            contentDescription = stringResource(R.string.pick_who_was_paid),
            onParticipantClick = onUpdateToParticipant,
        )
        DatePickerRow(
            datePickerExpanded = datePickerExpanded,
            time = transfer.time,
            modifier = Modifier,
            textModifier = Modifier
                .leadingPickTextModifier()
                .tweakForLabeledPickerRowLeadingText(),
            supportingText = "",
        )
        BasicTextField(
            label = stringResource(R.string.note),
            value = transfer.note ?: "",
            onValueChange = onUpdateNote,
            keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences),
            singleLine = false,
            modifier = Modifier.padding(vertical = 2.dp),
        )
        if (datePickerExpanded.value) {
            DatePickerDialogShow(
                time = transfer.time,
                dialog = datePickerDialog
            )
        }
        viewState.saveError?.let { problem ->
            TransactionSaveErrorCard(problem)
        }
        SaveAndDiscardButtons(
            mode = screenMode,
            saveEnabled = canSave,
            onSave = onSave,
            onDiscard = onDiscard,
        )
    }
}

@Composable
private fun AmountAndCurrencyRow(
    transfer: TransferDetailInputData,
    onAmountInputChange: (String) -> Unit,
    currencyPickerExpanded: MutableState<Boolean>,
    onUpdateCurrency: (CurrencyCode) -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        BasicTextField(
            label = stringResource(R.string.amount),
            value = transfer.amountValueString,
            onValueChange = onAmountInputChange,
            isError = transfer.amount.value <= BigDecimal.ZERO,
            supportingText = stringResource(R.string.field_is_mandatory),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Done),
            modifier = Modifier
                .fillMaxWidth(3 / 5f)
                .padding(end = 30.dp),
        )
        CurrencyPicker(
            currencyPickerExpanded = currencyPickerExpanded,
            pickedCurrency = transfer.amount.currency,
            onUpdateCurrency = onUpdateCurrency,
            supportingText = "",
            onlyShowSupportingTextForError = false,
        )
    }
}

@Composable
private fun ParticipantPickerRow(
    pickerExpanded: MutableState<Boolean>,
    leadingText: String,
    picked: Participant?,
    participants: List<Participant>,
    userParticipantId: String?,
    contentDescription: String,
    onParticipantClick: (Participant) -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        Text(
            text = leadingText,
            modifier = Modifier
                .leadingPickTextModifier()
                .tweakForLabeledPickerRowLeadingText(),
        )
        val pickedIsUser = picked?.participantId == userParticipantId && picked?.participantId != null
        DropDownPicker(
            menuExpanded = pickerExpanded,
            pickedText = if (pickedIsUser) stringResource(R.string.you) else picked?.nickname ?: "...",
            textWeight = if (pickedIsUser) FontWeight.Bold else FontWeight.Normal,
            contentDescription = contentDescription,
            isError = picked == null,
            supportingText = stringResource(R.string.field_is_mandatory),
        ) {
            participants.map { participant ->
                DropdownMenuItem(
                    onClick = {
                        onParticipantClick(participant)
                        pickerExpanded.value = false
                    }
                ) {
                    if (participant.participantId == userParticipantId) {
                        Text(text = stringResource(R.string.you), fontWeight = FontWeight.Bold)
                    } else {
                        Text(
                            text = participant.nickname,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                        )
                    }
                }
            }
        }
    }
}

private fun Modifier.leadingPickTextModifier(): Modifier {
    return this
        .padding(end = 5.dp)
        .defaultMinSize(minWidth = 60.dp)
}
