package cz.cvut.fit.molinlen.android.debttracker.feature.groupInfo

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetGroupBasicInfoUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetGroupBasicInfoUseCaseImpl
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.LeaveGroupUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.LeaveGroupUseCaseImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GroupInfoViewModel(
    private val getGroupBasicInfoUseCase: GetGroupBasicInfoUseCase = GetGroupBasicInfoUseCaseImpl(),
    private val leaveGroupUseCase: LeaveGroupUseCase = LeaveGroupUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(GroupInfoViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String

    private val auth = Firebase.auth
    val userId: String? = auth.currentUser?.uid

    fun loadData(groupId: String) {
        this.groupId = groupId
        viewModelScope.launch {
            userId
                ?.let { userId ->
                    getGroupBasicInfoUseCase.getBasicGroupInfo(userId = userId, groupId = groupId)
                        .collect {
                            it.fold(
                                { problem ->
                                    _viewState.update { GroupInfoViewState(error = problem) }
                                },
                                { groupInfo ->
                                    _viewState.update { GroupInfoViewState(usersGroup = groupInfo) }
                                },
                            )
                        }
                }
                ?: run {
                    _viewState.update { GroupInfoViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in group info without currentUser present")
                }
        }
    }

    fun leaveGroup(navigateAfterwards: () -> Unit) {
        auth.currentUser?.uid?.let { userId ->
            viewModelScope.launch {
                _viewState.update { it.copy(processing = true) }
                leaveGroupUseCase.leaveGroup(userId = userId, groupId = groupId)
                    .collect { result ->
                        result.map {
                            withContext(Dispatchers.Main) {
                                navigateAfterwards()
                            }
                            _viewState.update { it.copy(processing = false) }
                        }
                    }
            }
        }
    }

    fun copyGroupIdToClipboard(
        context: Context,
    ) {
        try {
            val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData: ClipData = ClipData.newPlainText("group id", viewState.value.usersGroup?.group?.groupId)
            clipboardManager.setPrimaryClip(clipData)
        } catch (e: Exception) {
            Log.e(APP_TAG, "Failed to copy text to clipboard", e)
        }
    }
}
