package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit.transferedit

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import arrow.core.left
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.mappers.toTransfer
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.math.BigDecimal
import java.time.DateTimeException
import java.time.OffsetDateTime

class TransferEditViewModel(
    private val getGroupParticipantsUseCase: GetGroupParticipantsUseCase = GetGroupParticipantsUseCaseImpl(),
    private val getTransferDetailUseCase: GetTransferDetailUseCase = GetTransferDetailUseCaseImpl(),
    private val validateAmountInputUseCase: ValidateAmountInputUseCase = ValidateAmountInputUseCaseImpl(),
    private val createTransferUseCase: CreateTransferUseCase = CreateTransferUseCaseImpl(),
    private val updateTransferUseCase: UpdateTransferUseCase = UpdateTransferUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(TransferEditViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private lateinit var groupId: String
    private var transferId: String? = null

    private val auth = Firebase.auth

    fun loadData(groupId: String, transferId: String?) {
        this.groupId = groupId
        this.transferId = transferId
        auth.currentUser
            ?.let { user ->
                getGroupsParticipants(user.uid)
                transferId
                    ?.let { purchaseId ->
                        getExistingPurchaseForStart(userId = user.uid, transferId = purchaseId)
                    }
                    ?: createEmptyTransfer()
            }
            ?: run {
                _viewState.update { TransferEditViewState(errorMessage = "No user signed in") }
                Log.e(APP_TAG, "$ERROR_PREFIX in transfer edit without currentUser present")
            }
    }

    fun updateFrom(from: Participant) {
        _viewState.update {
            val to = if (it.transfer?.to?.participantId != from.participantId) it.transfer?.to else null
            it.copy(transfer = it.transfer?.copy(from = from, to = to), saveError = null)
        }
    }

    fun updateTo(to: Participant) {
        _viewState.update {
            val from = if (it.transfer?.from?.participantId != to.participantId) it.transfer?.from else null
            it.copy(transfer = it.transfer?.copy(from = from, to = to), saveError = null)
        }
    }

    fun updateAmountValue(amountValueString: String) {
        _viewState.value.transfer?.amount?.currency?.let { currency ->
            val inputValidationResult = validateAmountInputUseCase.validateAmountInput(
                currentInputString = amountValueString,
                previousInputString = _viewState.value.transfer?.amountValueString ?: "",
                currency = currency,
            )
            val newValueString = inputValidationResult.resultingInputString
            val newAmount = inputValidationResult.resultingAmount
                ?: when (newValueString) {
                    _viewState.value.transfer?.amountValueString -> _viewState.value.transfer?.amount
                    else -> amountZero(currency)
                }
            _viewState.update {
                it.copy(
                    transfer = it.transfer?.copy(
                        amount = newAmount ?: amountZero(currency),
                        amountValueString = newValueString,
                    ),
                    saveError = null,
                )
            }
        }
    }

    fun updateAmountCurrency(currency: CurrencyCode) {
        _viewState.value.transfer?.amount?.copy(currency = currency)
            ?.let { newAmount ->
                _viewState.update { it.copy(transfer = it.transfer?.copy(amount = newAmount), saveError = null) }
            }
    }

    fun updateNote(note: String?) {
        _viewState.update { it.copy(transfer = it.transfer?.copy(note = note), saveError = null) }
    }

    fun updateDate(year: Int, month: Int, dayOfMonth: Int) {
        try {
            Log.d(APP_TAG, "Update date request: y [$year] m [$month] d [$dayOfMonth] (in transfer edit)")
            // for now setting midday time - todo update if user is able to set time also
            val dateTime = OffsetDateTime.of(year, month, dayOfMonth, 12, 0, 0, 0, OffsetDateTime.now().offset)
            _viewState.update { it.copy(transfer = it.transfer?.copy(time = dateTime), saveError = null) }
        } catch (e: DateTimeException) {
            Log.w(APP_TAG, "Invalid update date request: y [$year] m [$month] d [$dayOfMonth] (in transfer edit)", e)
        } catch (e: Exception) {
            Log.w(APP_TAG, "Failed update date request: y [$year] m [$month] d [$dayOfMonth] (in transfer edit)", e)
        }
    }

    fun canSave(): Boolean {
        return viewState.value.transfer?.let { transferInput ->
            savableInput(transferInput).isRight()
        } ?: false
    }

    private fun savableInput(transferInput: TransferDetailInputData): ProblemOr<TransferDetail> {
        val currency = transferInput.amount.currency
        return validateAmountInputUseCase.validateAmountInput(
            currentInputString = viewState.value.transfer?.amountValueString ?: "",
            previousInputString = viewState.value.transfer?.amountValueString ?: "",
            currency = currency,
        ).resultingAmount
            ?.let { amount ->
                if (amount.value > BigDecimal.ZERO) { // to check if current amount input is not BS
                    transferInput.toTransfer()
                } else null
            } ?: Problem("Invalid amount").left()
    }

    fun save(navigateAfterwards: () -> Unit) {
        viewState.value.transfer?.let { transferInput ->
            viewModelScope.launch {
                savableInput(transferInput).map { transfer ->
                    _viewState.update { it.copy(processing = true, saveError = null) }
                    val resultFlow = transferId
                        ?.let {
                            updateTransferUseCase.updateTransfer(
                                userId = auth.currentUser?.uid!!, groupId = groupId, transfer = transfer,
                            )
                        }
                        ?: createTransferUseCase.createTransfer(
                            userId = auth.currentUser?.uid!!, groupId = groupId, transfer = transfer,
                        )
                    resultFlow.collect { result ->
                        result.map {
                            withContext(Dispatchers.Main) {
                                navigateAfterwards()
                            }
                        }
                        _viewState.update {
                            it.copy(
                                processing = false,
                                saveError = result.fold({ problem -> problem }, { null }),
                            )
                        }
                    }
                }
            }
        }
    }

    private fun createEmptyTransfer() {
        viewModelScope.launch {
            _viewState.collect { state ->
                state.groupCurrency?.let { groupCurrency ->
                    if (state.transfer == null) {
                        _viewState.update {
                            it.copy(
                                transfer = emptyTransferDetailInput(groupCurrency),
                                loading = false,
                            )
                        }
                        this.coroutineContext.job.cancel()
                    }
                }
            }
        }
    }

    private fun getGroupsParticipants(userId: String) = viewModelScope.launch {
        getGroupParticipantsUseCase.getUsersGroupParticipants(userId, groupId).collect { result ->
            Log.d(APP_TAG, "Got participants: [$result]")
            result.fold({ problem ->
                _viewState.update { TransferEditViewState(error = problem) }
            }, { usersGroupParticipants ->
                _viewState.update {
                    it.copy(
                        participants = usersGroupParticipants.groupParticipants.sortedBy { participant -> participant.nickname },
                        usersParticipantId = usersGroupParticipants.userParticipantId,
                        groupCurrency = usersGroupParticipants.groupCurrency,
                    )
                }
            })
        }
    }

    private fun getExistingPurchaseForStart(userId: String, transferId: String) = viewModelScope.launch {
        getTransferDetailUseCase.getTransferDetail(userId = userId, groupId = groupId, transferId = transferId)
            .collect { existing ->
                Log.d(APP_TAG, "Transfer edit getExistingPurchaseForStart, transfer [${viewState.value.transfer}]")
                if (viewState.value.transfer == null) {
                    existing.fold({ problem ->
                        _viewState.update { TransferEditViewState(error = problem) }
                    }, { transfer ->
                        _viewState.update { it.copy(transfer = fillTransferDetailInput(transfer), loading = false) }
                    })
                }
            }
    }
}
