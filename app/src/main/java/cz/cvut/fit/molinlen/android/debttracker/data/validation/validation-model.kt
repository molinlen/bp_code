package cz.cvut.fit.molinlen.android.debttracker.data.validation

enum class RegistrationInputProblem {
    INVALID_EMAIL,
    PASSWORD_TOO_SHORT,
    //todo should we also have maximum length?
    PASSWORDS_NOT_MATCH,
    TERMS_NOT_ACCEPTED,
    EMAIL_ALREADY_REGISTERED,
}

enum class GroupInfoInputProblem {
    GROUP_NAME_BLANK,
    GROUP_CURRENCY_MISSING,
}

enum class TransferInputProblem {
    INVALID_AMOUNT,
    MISSING_TRANSFER_SOURCE,
    MISSING_TRANSFER_RECIPIENT,
    SAME_SOURCE_AND_RECIPIENT,
}
