package cz.cvut.fit.molinlen.android.debttracker.feature.grouplist

import cz.cvut.fit.molinlen.android.debttracker.data.model.GlobalOwedSumsOverview
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupPreview

data class GroupListViewState(
    val groups: List<UsersGroupPreview>? = null,
    val overview: GlobalOwedSumsOverview? = null,
    val userHasPrimaryCurrency: Boolean? = null,
    val groupJoinError: Problem? = null,
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
