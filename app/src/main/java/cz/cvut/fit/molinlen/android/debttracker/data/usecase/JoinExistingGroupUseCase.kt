package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface JoinExistingGroupUseCase {
    suspend fun joinGroup(
        userId: String,
        groupId: String,
        userParticipantId: String?,
    ): Flow<ProblemOr<Unit>>
}

class JoinExistingGroupUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : JoinExistingGroupUseCase {
    override suspend fun joinGroup(
        userId: String,
        groupId: String,
        userParticipantId: String?,
    ): Flow<ProblemOr<Unit>> = groupRepository.joinExistingGroup(
        userId = userId,
        groupId = groupId,
        asParticipantId = userParticipantId,
    ).map { result -> result.map { } }
}
