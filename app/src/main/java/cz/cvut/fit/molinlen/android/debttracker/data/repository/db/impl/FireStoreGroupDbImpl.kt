package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

import android.util.Log
import arrow.core.right
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.randomId
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogError
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogWarn
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.GroupEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.GroupEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.ParticipantEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.ParticipantEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toUpdateData
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow

class FireStoreGroupDbImpl : GroupDb {
    private val db = Firebase.firestore

    override fun getGroupInfo(groupId: String): Flow<ProblemOr<GroupEntity>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .addSnapshotListener { result, e ->
                tryToDeserialize("group info", e) {
                    result?.let { document ->
                        document.toObject(GroupEntityData::class.java)
                            ?.let {
                                trySend(GroupEntity(groupId = groupId, group = it).right())
                            }
                            ?: sendProblemAndLogError("Error mapping info for group [$groupId] - null")
                    }
                }
            }
        awaitClose { }
    }

    override fun getGroupParticipants(groupId: String): Flow<ProblemOr<List<ParticipantEntity>>> = flow {
        getGroupInfo(groupId).collect { result ->
            val participants = result.map {
                it.group?.participants?.map { participantMapEntry ->
                    ParticipantEntity(participantId = participantMapEntry.key, participant = participantMapEntry.value)
                } ?: emptyList()
            }
            emit(participants)
        }
    }

    override fun createGroup(groupData: GroupEntityData): Flow<ProblemOr<String>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .add(groupData)
            .addOnSuccessListener { result ->
                if (result.id.isNotBlank()) {
                    trySend(result.id.right())
                } else {
                    sendProblemAndLogError("Error createGroup [$groupData] - blank id [${result.id}]")
                }
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error createGroup [$groupData]", e)
            }
        awaitClose { }
    }

    override fun updateGroup(group: GroupEntity): Flow<ProblemOr<Unit>> = callbackFlow {
        group.groupId?.let { groupId ->
            group.group?.let { groupData ->
                db.collection(FireStoreCollections.GROUPS)
                    .document(groupId)
                    .set(groupData.toUpdateData(), SetOptions.merge())
                    .addOnSuccessListener {
                        trySend(Unit.right())
                    }
                    .addOnFailureListener { e ->
                        sendProblemAndLogWarn("Error updateGroup [$group]", e)
                    }
            } ?: sendProblemAndLogError("No group in updateGroup"); 0
        } ?: sendProblemAndLogError("No group id in updateGroup")
        awaitClose { }
    }

    override fun addParticipants(
        groupId: String,
        newParticipantsData: Set<ParticipantEntityData>,
    ): Flow<ProblemOr<Unit>> = callbackFlow {
        try {
            db.runTransaction { transaction ->
                val group = transaction.get(
                    db.collection(FireStoreCollections.GROUPS)
                        .document(groupId)
                )
                if (group.id.endsWith(groupId)
                    && group.data?.get(GroupEntityData::defaultCurrency.name) in CurrencyCode.values().map { it.name }
                ) {
                    newParticipantsData.map {
                        transaction.update(
                            db.collection(FireStoreCollections.GROUPS).document(groupId),
                            GroupEntityData::participants.name + "." + randomId(),
                            it,
                        )
                    }
                } else {
                    throw FirebaseFirestoreException("Group not found", FirebaseFirestoreException.Code.NOT_FOUND)
                }
            }
                .addOnSuccessListener { result ->
                    Log.d(APP_TAG, "Participants adding transaction completed [$result]")
                    trySend(Unit.right())
                }
                .addOnFailureListener { e ->
                    sendProblemAndLogWarn("Error addParticipants group [$groupId] [$newParticipantsData]", e)
                }
        } catch (e: Exception) {
            sendProblemAndLogWarn("Error addParticipants group [$groupId] [$newParticipantsData]", e)
        }
        awaitClose { }
    }
}
