package cz.cvut.fit.molinlen.android.debttracker.data.repository.impl

import arrow.core.flatMap
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserUpdateData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.UserRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.Db
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl.FireStoreDbImpl
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UserEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toModel
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toEntityUpdateData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserRepositoryImpl(
    private val db: Db = FireStoreDbImpl(),
) : UserRepository {
    override suspend fun getUserInfo(userId: String): Flow<ProblemOr<UserBasic>> = flow {
        db.getUserInfo(userId).collect { result ->
            val userInfo = result.flatMap { userEntityData ->
                UserEntity(userId = userId, data = userEntityData).toModel()
            }
            emit(userInfo)
        }
    }

    override suspend fun updateUserInfo(userId: String, data: UserUpdateData): Flow<ProblemOr<Unit>> {
        return db.updateUserInfo(userId = userId, userInfo = data.toEntityUpdateData())
    }
}
