package cz.cvut.fit.molinlen.android.debttracker.data.repository

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import kotlinx.coroutines.flow.Flow

interface GroupRepository {
    suspend fun getGroupList(userId: String): Flow<ProblemOr<List<UsersGroupBasic>>>
    suspend fun getGroup(userId: String, groupId: String): Flow<ProblemOr<UsersGroupBasic>>
    suspend fun getBasicGroupInfo(userId: String, groupId: String): Flow<ProblemOr<UsersGroupInfoBasic>>
    suspend fun getUsersGroupParticipantId(userId: String, groupId: String): Flow<ProblemOr<String?>>
    suspend fun getUsersGroupParticipants(userId: String, groupId: String): Flow<ProblemOr<UsersGroupParticipants>>
    suspend fun createNewGroup(userId: String, groupData: GroupInfoBasic): Flow<ProblemOr<String>>
    suspend fun updateGroupInfo(groupInfo: GroupInfoBasic): Flow<ProblemOr<Unit>>
    suspend fun joinExistingGroup(
        userId: String,
        groupId: String,
        asParticipantId: String?,
    ): Flow<ProblemOr<GroupInfoBasic>>
    suspend fun leaveGroup(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<Unit>>
    suspend fun addGroupParticipants(groupId: String, newParticipants: List<ParticipantBasic>): Flow<ProblemOr<Unit>>
}
