package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import cz.cvut.fit.molinlen.android.debttracker.data.model.Comment
import cz.cvut.fit.molinlen.android.debttracker.data.model.CommentDetail
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.CommentEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.CommentEntityData

fun Comment.toEntity(): CommentEntity = CommentEntity(
    writerUserId = writerUserId,
    data = toEntityData()
)

fun Comment.toEntityData() = CommentEntityData(
    content = content,
    time = time.toTimestamp(),
    edited = edited,
)

fun CommentEntity.toModel(): ProblemOr<CommentDetail> = Either.catch {
    CommentDetail(
        writerUserId = writerUserId!!,
        content = data?.content!!,
        time = data.time!!.toOffsetDateTime(),
        edited = data.edited!!,
    )
}.mapConversionThrowable("CommentEntity by [$writerUserId]")
