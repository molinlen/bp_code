package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.*
import arrow.core.computations.either
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.*

fun Purchase.toEntity(): PurchaseEntity = PurchaseEntity(
    transactionId = transactionId,
    purchase = this.toEntityData(),
)

fun Purchase.toEntityData(): PurchaseEntityData = PurchaseEntityData(
    name = name,
    payers = payers.map { it.toEntity() },
    splitting = splitting.toEntity(),
    time = time.toTimestamp(),
    note = note,
    comments = comments.map { it.toEntity() },
)

fun Payer.toEntity(): PayerEntity = PayerEntity(
    participantId = participant.participantId,
    amount = amount.toEntityData(),
)

fun PurchaseEntity.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<PurchaseDetail> {
    val commentsOrProblem = either.eager {
        purchase?.comments?.map { it.toModel().bind() } ?: emptyList()
    }
    return Either.catch {
        commentsOrProblem.flatMap { comments ->
            purchase!!.splitting!!.toModelWithParticipants(participants).flatMap { splitting ->
                either.eager {
                    purchase?.payers!!.map { payer ->
                        Payer(
                            participant = participants[payer.participantId]!!,
                            amount = payer.amount!!.toModel().bind(),
                        )
                    }
                }.map { payers ->
                    PurchaseDetail(
                        transactionId = transactionId!!,
                        name = purchase?.name,
                        payers = payers,
                        splitting = splitting,
                        comments = comments,
                        time = purchase?.time!!.toOffsetDateTime(),
                        note = purchase?.note,
                    )
                }
            }
        }
    }.mapConversionThrowable("PurchaseEntity [$transactionId]").flatten()
}

fun PurchaseEntityReadData.toPurchaseEntity(transactionId: String): ProblemOr<PurchaseEntity> {
    return Either.catch {
        val splittingData = when (splitting!!.type!!) {
            SplittingType.EXPLICIT -> {
                // shares: List<Map<String, *>>
                val shares = (splitting!!.data[TransactionSplittingExplicitEntityData::shares.name] as? List<*>)!!
                TransactionSplittingExplicitEntityData(
                    shares = shares.map {
                        val share = (it as? Map<*, *>)!!
                        TransactionParticipantShareExplicitEntityData(
                            participantId = share[TransactionParticipantShareExplicitEntityData::participantId.name] as String?,
                            shareValue = share[TransactionParticipantShareExplicitEntityData::shareValue.name] as String?,
                        )
                    }
                )
            }
            SplittingType.PORTION -> {
                // shares: List<Map<String, *>>
                val shares = (splitting!!.data[TransactionSplittingPortionEntityData::shares.name] as? List<*>)!!
                TransactionSplittingPortionEntityData(
                    shares = shares.map {
                        val share = (it as? Map<*, *>)!!
                        TransactionParticipantSharePortionEntityData(
                            participantId = share[TransactionParticipantSharePortionEntityData::participantId.name] as String?,
                            shareValue = (share[TransactionParticipantSharePortionEntityData::shareValue.name] as Long?)?.toInt(),
                        )
                    }
                )
            }
        }
        PurchaseEntity(
            transactionId = transactionId,
            purchase = PurchaseEntityData(
                name = name,
                payers = payers,
                splitting = TransactionSplittingStrategyEntity(splitting!!.type, splittingData),
                time = time,
                note = note,
                comments = comments,
            ),
        )
    }.mapConversionThrowable("PurchaseEntityRead [$transactionId]")
}

fun PurchaseEntityData.toUpdateData() = PurchaseEntityUpdateData(
    name = name,
    payers = payers,
    splitting = splitting,
    time = time,
    note = note,
)
