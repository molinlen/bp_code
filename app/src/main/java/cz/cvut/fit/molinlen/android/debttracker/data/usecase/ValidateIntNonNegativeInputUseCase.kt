package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.IntInputValidationResult
import cz.cvut.fit.molinlen.android.debttracker.data.validation.IntInputValidator
import cz.cvut.fit.molinlen.android.debttracker.data.validation.IntInputValidatorImpl

interface ValidateIntNonNegativeInputUseCase {
    fun validateNonNegativeIntInput(
        currentInputString: String,
        previousInputString: String,
        maxAllowedInput: Int? = null,
    ): IntInputValidationResult
}

class ValidateIntNonNegativeInputUseCaseImpl(
    private val intInputValidator: IntInputValidator = IntInputValidatorImpl(),
) : ValidateIntNonNegativeInputUseCase {
    override fun validateNonNegativeIntInput(
        currentInputString: String,
        previousInputString: String,
        maxAllowedInput: Int?,
    ): IntInputValidationResult = intInputValidator.validateNonNegativeIntInput(
        currentInputString = currentInputString,
        previousInputString = previousInputString,
        maxAllowedInput = maxAllowedInput,
    )
}
