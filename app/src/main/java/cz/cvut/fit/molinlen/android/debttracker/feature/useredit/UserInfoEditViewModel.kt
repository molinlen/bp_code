package cz.cvut.fit.molinlen.android.debttracker.feature.useredit

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toInputData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toUpdateData
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetUserBasicInfoUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetUserBasicInfoUseCaseImpl
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.UpdateUserInfoUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.UpdateUserInfoUseCaseImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserInfoEditViewModel(
    private val getUserBasicInfoUseCase: GetUserBasicInfoUseCase = GetUserBasicInfoUseCaseImpl(),
    private val updateUserInfoUseCase: UpdateUserInfoUseCase = UpdateUserInfoUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(UserInfoEditViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private val auth = Firebase.auth
    val userId: String? get() = auth.currentUser?.uid

    fun loadData() {
        viewModelScope.launch {
            userId
                ?.let { userId ->
                    getUserBasicInfoUseCase.getBasicUserInfo(userId = userId)
                        .collect {
                            it.fold(
                                { problem -> _viewState.update { UserInfoEditViewState(error = problem) } },
                                { userInfo -> _viewState.update { UserInfoEditViewState(userData = userInfo.toInputData()) } },
                            )
                        }
                }
                ?: run {
                    _viewState.update { UserInfoEditViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in user info edit without currentUser present")
                }
        }
    }

    fun updateCurrency(currency: CurrencyCode) {
        _viewState.update { it.copy(userData = it.userData?.copy(primaryCurrency = currency), saveError = null) }
    }

    fun canSave() = savableInput() != null

    private fun savableInput() = viewState.value.userData?.toUpdateData()?.fold({ null }, { it })

    fun save(navigateAfterwards: () -> Unit) {
        viewModelScope.launch {
            userId?.let { userId ->
                savableInput()?.let { updateData ->
                    _viewState.update { it.copy(processing = true, saveError = null) }
                    updateUserInfoUseCase.updateUserInfo(userId, updateData).collect { result ->
                        result.map {
                            withContext(Dispatchers.Main) { navigateAfterwards() }
                        }
                        _viewState.update {
                            it.copy(
                                processing = false,
                                saveError = result.fold({ problem -> problem }, { null }),
                            )
                        }
                    }
                }
            }
        }
    }
}
