package cz.cvut.fit.molinlen.android.debttracker.feature.userdetail

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic

data class UserDetailViewState(
    val userBasic: UserBasic? = null,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
