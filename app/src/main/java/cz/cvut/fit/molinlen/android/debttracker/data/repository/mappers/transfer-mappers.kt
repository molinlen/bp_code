package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.flatMap
import cz.cvut.fit.molinlen.android.debttracker.data.model.Participant
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.Transfer
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransferDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntityUpdateData

fun Transfer.toEntity() = TransferEntity(
    transactionId = transactionId,
    transfer = this.toEntityData(),
)

fun Transfer.toEntityData() = TransferEntityData(
    fromParticipantId = from.participantId,
    toParticipantId = to.participantId,
    time = time.toTimestamp(),
    amount = amount.toEntityData(),
    note = note,
    comments = comments.map { it.toEntity() },
)

fun TransferEntity.toModelWithParticipants(participants: Map<String, Participant>): ProblemOr<TransferDetail> {
    val commentsOrProblem = either.eager {
        transfer?.comments?.map { it.toModel().bind() } ?: emptyList()
    }
    return commentsOrProblem.flatMap { comments ->
        transfer?.amount?.toModel()!!.flatMap { amount ->
            Either.catch {
                TransferDetail(
                    transactionId = transactionId!!,
                    from = participants[transfer?.fromParticipantId]!!,
                    to = participants[transfer?.toParticipantId]!!,
                    time = transfer?.time!!.toOffsetDateTime(),
                    comments = comments,
                    amount = amount,
                    note = transfer!!.note,
                )

            }.mapConversionThrowable("TransferEntity [$transactionId]")
        }
    }
}

fun TransferEntityData.toUpdateData() = TransferEntityUpdateData(
    fromParticipantId = fromParticipantId,
    toParticipantId = toParticipantId,
    time = time,
    amount = amount,
    note = note
)
