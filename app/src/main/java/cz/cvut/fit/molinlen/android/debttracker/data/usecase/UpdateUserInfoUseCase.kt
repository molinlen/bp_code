package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserUpdateData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.UserRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.UserRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.UserRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface UpdateUserInfoUseCase {
    suspend fun updateUserInfo(
        userId: String,
        data: UserUpdateData,
    ): Flow<ProblemOr<Unit>>
}

class UpdateUserInfoUseCaseImpl(
    private val userRepository: UserRepository = if (USE_FAKE_DATA) UserRepositoryFake() else UserRepositoryImpl(),
) : UpdateUserInfoUseCase {
    override suspend fun updateUserInfo(
        userId: String,
        data: UserUpdateData,
    ): Flow<ProblemOr<Unit>> {
        return userRepository.updateUserInfo(
            userId = userId,
            data = data,
        )
    }
}
