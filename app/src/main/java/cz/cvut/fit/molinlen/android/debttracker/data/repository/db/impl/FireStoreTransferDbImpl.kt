package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

import android.util.Log
import arrow.core.right
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.TransferDb
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogError
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogWarn
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.tryToDeserialize
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.TransferEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toUpdateData
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class FireStoreTransferDbImpl : TransferDb {
    private val db = Firebase.firestore

    override fun getGroupTransfers(groupId: String): Flow<ProblemOr<List<TransferEntity>>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.TRANSFERS)
            .orderBy(TransferEntityData::time.name, Query.Direction.DESCENDING)
            .addSnapshotListener { result, e ->
                tryToDeserialize("group transfers", e) {
                    result?.documents?.let { documents ->
                        val transfers = documents.map { transferDoc ->
                            transferDoc.toObject(TransferEntityData::class.java)?.let {
                                TransferEntity(
                                    transactionId = transferDoc.id,
                                    transfer = it,
                                )
                            } ?: run { Log.e(APP_TAG, "Error mapping transfers for group [$groupId]"); null }
                        }
                        trySend(transfers.filterNotNull().right())
                    }
                }
            }
        awaitClose { }
    }

    override fun getTransfer(groupId: String, transferId: String): Flow<ProblemOr<TransferEntity>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.TRANSFERS)
            .document(transferId)
            .addSnapshotListener { result, e ->
                tryToDeserialize("transfer", e) {
                    result?.let { document ->
                        document.toObject(TransferEntityData::class.java)
                            ?.let {
                                val transfer = TransferEntity(
                                    transactionId = result.id,
                                    transfer = it,
                                )
                                trySend(transfer.right())
                            } ?: sendProblemAndLogError("Error mapping transfer group [$groupId] id [$transferId]")
                    }
                }
            }
        awaitClose { }
    }

    override fun createTransfer(
        groupId: String,
        transferData: TransferEntityData,
    ): Flow<ProblemOr<String>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.TRANSFERS)
            .add(transferData)
            .addOnSuccessListener { result ->
                if (result.id.isNotBlank()) {
                    trySend(result.id.right())
                } else {
                    sendProblemAndLogError(
                        "Error createTransfer [$transferData] group [$groupId] - blank id [${result.id}]")
                }
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error createTransfer [$transferData] group [$groupId]", e)
            }
        awaitClose {}
    }

    override fun updateTransfer(groupId: String, transfer: TransferEntity): Flow<ProblemOr<Unit>> = callbackFlow {
        transfer.transactionId?.let { transferId ->
            transfer.transfer?.let { transferData ->
                db.collection(FireStoreCollections.GROUPS)
                    .document(groupId)
                    .collection(FireStoreCollections.GroupSubCollections.TRANSFERS)
                    .document(transferId)
                    .set(transferData.toUpdateData(), SetOptions.merge())
                    .addOnSuccessListener {
                        trySend(Unit.right())
                    }
                    .addOnFailureListener { e ->
                        sendProblemAndLogWarn("Error updateTransfer [$transfer] group [$groupId]", e)
                    }
            } ?: sendProblemAndLogError("No transfer in updateTransfer"); 0
        } ?: sendProblemAndLogError("No transfer id in updateTransfer")
        awaitClose {}
    }

    override fun deleteTransfer(groupId: String, transferId: String): Flow<ProblemOr<Unit>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.TRANSFERS)
            .document(transferId)
            .delete()
            .addOnSuccessListener {
                trySend(Unit.right())
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error deleteTransfer [$transferId] group [$groupId]", e)
            }
        awaitClose {}
    }
}
