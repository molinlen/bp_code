package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.AmountInputValidationResult
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.validation.AmountInputValidator
import cz.cvut.fit.molinlen.android.debttracker.data.validation.AmountInputValidatorImpl

interface ValidateAmountInputUseCase {
    fun validateAmountInput(
        currentInputString: String,
        previousInputString: String,
        currency: CurrencyCode,
    ): AmountInputValidationResult
}

class ValidateAmountInputUseCaseImpl(
    private val amountInputValidator: AmountInputValidator = AmountInputValidatorImpl(),
) : ValidateAmountInputUseCase {
    override fun validateAmountInput(
        currentInputString: String,
        previousInputString: String,
        currency: CurrencyCode
    ): AmountInputValidationResult = amountInputValidator.validateAmountInput(
        currentInputString = currentInputString,
        previousInputString = previousInputString,
        currency = currency,
    )
}
