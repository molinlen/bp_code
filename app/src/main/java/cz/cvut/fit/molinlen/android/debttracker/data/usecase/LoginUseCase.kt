package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import android.util.Log
import arrow.core.Either
import arrow.core.left
import arrow.core.right
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.LoginInfo
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

interface LoginUseCase {
    suspend fun login(loginInfo: LoginInfo): Flow<ProblemOr<FirebaseUser>>
}

class LoginUseCaseImpl : LoginUseCase {
    private val auth = Firebase.auth

    override suspend fun login(loginInfo: LoginInfo): Flow<ProblemOr<FirebaseUser>> {
        return callbackFlow {
            Either.catch {
                auth.signInWithEmailAndPassword(loginInfo.email, loginInfo.password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d(APP_TAG, "login:success")
                            trySend(
                                auth.currentUser?.right()
                                    ?: Problem("Login succeeded but no user gotten").left()
                            )
                        } else {
                            Log.d(APP_TAG, "login:failure", task.exception)
                            trySend(Problem("Login failed", null, task.exception).left())
                        }
                    }
            }.mapLeft {
                Log.e(APP_TAG, "login : thrown failure", it)
                trySend(Problem("Login failed", it.message, it).left())
            }
            awaitClose { }
        }
    }
}
