package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertor
import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.computation.OverallStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.math.BigDecimal

class OverallStatusCalculatorImpl(
    private val currencyConvertor: CurrencyConvertor = CurrencyConvertorImpl(),
) : OverallStatusCalculator {

    override suspend fun calculateOverallUserStatusFromPreview(
        usersGroups: List<UsersGroupPreview>,
        currency: CurrencyCode,
    ): ProblemOr<GlobalOwedSumsOverview> {
        val owedValue = usersGroups
            .filter { it.group.currentOwnStatus.value < BigDecimal.ZERO }
            .sumOf { it.group.currentOwnStatus.inCurrency(currency) }
        val collectableValue = usersGroups
            .filter { it.group.currentOwnStatus.value > BigDecimal.ZERO }
            .sumOf { it.group.currentOwnStatus.inCurrency(currency) }

        return GlobalOwedSumsOverview(
            sumOwed = createAmount(value = owedValue.abs(), currency = currency),
            sumCollectable = createAmount(value = collectableValue, currency = currency),
        ).right()
    }

    private fun Amount.inCurrency(currency: CurrencyCode): BigDecimal {
        return currencyConvertor.convert(
            value = this.value,
            from = this.currency,
            to = currency
        )
    }
}
