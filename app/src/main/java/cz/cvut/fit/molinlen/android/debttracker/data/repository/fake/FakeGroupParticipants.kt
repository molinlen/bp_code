package cz.cvut.fit.molinlen.android.debttracker.data.repository.fake

import cz.cvut.fit.molinlen.android.debttracker.data.model.ParticipantWithStatus
import cz.cvut.fit.molinlen.android.debttracker.data.model.amountFromDouble

object FakeGroupParticipants {
    val user = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "James",
        currentStatus = amountFromDouble(217.0),
    )
    val tom = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Tom",
        currentStatus = amountFromDouble(-297.0),
    )
    val amy = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Amy",
        currentStatus = amountFromDouble(-181.0),
    )
    val schubert = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Schubert",
        currentStatus = amountFromDouble(-135.0),
    )
    val catherine = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Catherine",
        currentStatus = amountFromDouble(-121.0),
    )
    val john = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "John",
        currentStatus = amountFromDouble(175.0),
    )
    val danny = ParticipantWithStatus(
        participantId = randomId(),
        nickname = "Danny",
        currentStatus = amountFromDouble(342.0),
    )
}
