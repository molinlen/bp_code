package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.computation.GroupStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.computation.fake.GroupStatusCalculatorFake
import cz.cvut.fit.molinlen.android.debttracker.data.computation.impl.GroupStatusCalculatorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupDetail
import cz.cvut.fit.molinlen.android.debttracker.data.model.mapIfRight
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface GetGroupDetailUseCase {
    suspend fun getUsersGroupDetail(userId: String, groupId: String): Flow<ProblemOr<UsersGroupDetail>>
}

class GetGroupDetailUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
    private val groupStatusCalculator: GroupStatusCalculator = if (USE_FAKE_DATA) GroupStatusCalculatorFake() else GroupStatusCalculatorImpl(),
) : GetGroupDetailUseCase {
    override suspend fun getUsersGroupDetail(userId: String, groupId: String): Flow<ProblemOr<UsersGroupDetail>> {
        return flow {
            groupRepository.getGroup(userId = userId, groupId = groupId).collect {
                val result = it.mapIfRight { group ->
                    groupStatusCalculator.computeGroupStatus(group)
                }
                emit(result)
            }
        }
    }
}
