package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.left
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertor
import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitter
import cz.cvut.fit.molinlen.android.debttracker.data.computation.AmountSplitterImpl
import cz.cvut.fit.molinlen.android.debttracker.data.computation.GroupStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.math.BigDecimal

class GroupStatusCalculatorImpl(
    private val currencyConvertor: CurrencyConvertor = CurrencyConvertorImpl(),
    private val amountSplitter: AmountSplitter = AmountSplitterImpl,
) : GroupStatusCalculator {
    override fun computeGroupStatus(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupDetail> {
        val groupCurrency = usersGroup.group.defaultCurrency

        // MutableMap<participantId: String, statusSoFarInGroupCurrency: BigDecimal>
        val statuses = usersGroup.group.participants.associate {
            it.participantId to amountZero(groupCurrency).value
        }.toMutableMap()

        val process = Either.catch {
            usersGroup.group.transactions.forEach { transaction ->
                when (transaction) {
                    is Transfer -> {
                        statuses[transaction.from.participantId] =
                            statuses[transaction.from.participantId]!! + transaction.amount.inCurrency(groupCurrency)
                        statuses[transaction.to.participantId] =
                            statuses[transaction.to.participantId]!! - transaction.amount.inCurrency(groupCurrency)
                    }
                    is Purchase -> {
                        transaction.payers.forEach { payer ->
                            statuses[payer.participant.participantId] =
                                statuses[payer.participant.participantId]!! + payer.amount.inCurrency(groupCurrency)
                        }
                        //todo incorporate carry
                        when (transaction.splitting) {
                            is TransactionSplittingExplicit -> {
                                (transaction.splitting as TransactionSplittingExplicit).sharers.forEach { share ->
                                    statuses[share.participant.participantId] =
                                        statuses[share.participant.participantId]!! - share.shareValue.toCurrency(
                                            from = transaction.currency,
                                            to = groupCurrency,
                                        )
                                }
                            }
                            is TransactionSplittingPortion -> {
                                val splitting = (transaction.splitting as TransactionSplittingPortion)
                                val splitPortionsResult = amountSplitter.splitAmountToPortions(
                                    amount = transaction.fullAmount,
                                    portions = splitting.sharers.map { it.shareValue },
                                )
                                splitPortionsResult.fold(
                                    { problem -> return problem.left() },
                                    { splitPortions ->
                                        splitting.sharers.forEach { share ->
                                            statuses[share.participant.participantId] =
                                                statuses[share.participant.participantId]!! - (
                                                        splitPortions.portionResults[share.shareValue]
                                                            ?.inCurrency(currency = groupCurrency)
                                                            ?: return Problem("Failed to map SplittingPortion splitPortions in group [${usersGroup.group.groupId}]").left()
                                                        )
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            }
        }

        return process.map {
            usersGroup.toDetail(
                participants = usersGroup.group.participants.map {
                    ParticipantWithStatus(
                        participantId = it.participantId,
                        nickname = it.nickname,
                        currentStatus = createAmount(statuses[it.participantId]!!, groupCurrency)
                    )
                }.toSet(),
            )
        }.mapLeft { Problem("Failed group status calculation", cause = it) }
    }

    override fun computeGroupPreview(usersGroup: UsersGroupBasic): ProblemOr<UsersGroupPreview> {
        val groupCurrency = usersGroup.group.defaultCurrency
        var status = amountZero(groupCurrency).value
        usersGroup.userParticipantId?.let { userParticipantId ->
            usersGroup.group.transactions.forEach { transaction ->
                when (transaction) {
                    is Transfer -> {
                        if (transaction.from.participantId == userParticipantId) {
                            status += transaction.amount.inCurrency(groupCurrency)
                        }
                        if (transaction.to.participantId == userParticipantId) {
                            status -= transaction.amount.inCurrency(groupCurrency)
                        }
                    }
                    is Purchase -> {
                        transaction.payers
                            .filter { it.participant.participantId == userParticipantId }
                            .forEach { payer ->
                                status += payer.amount.inCurrency(groupCurrency)
                            }
                        when (transaction.splitting) {
                            is TransactionSplittingExplicit -> {
                                (transaction.splitting as TransactionSplittingExplicit).sharers
                                    .filter { it.participant.participantId == userParticipantId }
                                    .forEach { share ->
                                        status -= share.shareValue.toCurrency(
                                            from = transaction.currency,
                                            to = groupCurrency,
                                        )
                                    }
                            }
                            is TransactionSplittingPortion -> {
                                val splitting = (transaction.splitting as TransactionSplittingPortion)
                                val userShares = splitting.sharers
                                    .filter { it.participant.participantId == userParticipantId }
                                if (userShares.isNotEmpty()) {
                                    val splitPortionsResult = amountSplitter.splitAmountToPortions(
                                        amount = transaction.fullAmount,
                                        portions = splitting.sharers.map { it.shareValue },
                                    )
                                    splitPortionsResult.fold(
                                        { problem -> return problem.left() },
                                        { splitPortions ->
                                            val userPortions = userShares
                                                .map { splitPortions.portionResults[it.shareValue]?.value }
                                            when {
                                                userPortions.any { it == null } -> return Problem("Failed to map SplittingPortion splitPortion in group [${usersGroup.group.groupId}]").left()
                                                else ->
                                                    status -= userPortions.sumOf { it!! }
                                                        .toCurrency(
                                                            from = transaction.currency,
                                                            to = groupCurrency,
                                                        )
                                            }
                                        }
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }

        return usersGroup.toPreview(userStatus = createAmount(status, groupCurrency)).right()
    }

    override fun computeGroupPreviews(usersGroups: List<UsersGroupBasic>): ProblemOr<List<UsersGroupPreview>> {
        return either.eager { usersGroups.map { computeGroupPreview(it).bind() } }
    }

    private fun Amount.inCurrency(currency: CurrencyCode): BigDecimal {
        return currencyConvertor.convert(
            value = this.value,
            from = this.currency,
            to = currency
        )
    }

    private fun BigDecimal.toCurrency(from: CurrencyCode, to: CurrencyCode): BigDecimal {
        return currencyConvertor.convert(value = this, from = from, to = to)
    }
}
