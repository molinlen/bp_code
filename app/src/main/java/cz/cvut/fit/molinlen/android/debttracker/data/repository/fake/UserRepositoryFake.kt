package cz.cvut.fit.molinlen.android.debttracker.data.repository.fake

import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserUpdateData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class UserRepositoryFake : UserRepository {
    override suspend fun getUserInfo(userId: String): Flow<ProblemOr<UserBasic>> = flowOf(
        UserBasic(
            userId = userId,
            primaryCurrency = CurrencyCode.CZK,
        ).right()
    )

    override suspend fun updateUserInfo(userId: String, data: UserUpdateData): Flow<ProblemOr<Unit>> =
        flowOf(Unit.right())
}
