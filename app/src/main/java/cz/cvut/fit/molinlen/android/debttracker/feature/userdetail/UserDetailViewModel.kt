package cz.cvut.fit.molinlen.android.debttracker.feature.userdetail

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.ERROR_PREFIX
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetUserBasicInfoUseCase
import cz.cvut.fit.molinlen.android.debttracker.data.usecase.GetUserBasicInfoUseCaseImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class UserDetailViewModel(
    private val getUserBasicInfoUseCase: GetUserBasicInfoUseCase = GetUserBasicInfoUseCaseImpl(),
) : ViewModel() {
    private val _viewState = MutableStateFlow(UserDetailViewState(loading = true))
    val viewState = _viewState.asStateFlow()

    private val auth = Firebase.auth
    val userId: String? get() = auth.currentUser?.uid
    val userEmail: String? get() = auth.currentUser?.email

    fun loadData() {
        viewModelScope.launch {
            userId
                ?.let { userId ->
                    getUserBasicInfoUseCase.getBasicUserInfo(userId = userId)
                        .collect {
                            it.fold(
                                { problem -> _viewState.update { UserDetailViewState(error = problem) } },
                                { userInfo -> _viewState.update { UserDetailViewState(userBasic = userInfo) } },
                            )
                        }
                }
                ?: run {
                    _viewState.update { UserDetailViewState(errorMessage = "No user signed in") }
                    Log.e(APP_TAG, "$ERROR_PREFIX in user detail without currentUser present")
                }
        }
    }
}
