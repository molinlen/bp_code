package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

import android.util.Log
import arrow.core.right
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.PurchaseDb
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogError
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogWarn
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.tryToDeserialize
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.PurchaseEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.PurchaseEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.PurchaseEntityReadData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toPurchaseEntity
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.toUpdateData
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class FireStorePurchaseDbImpl : PurchaseDb {
    private val db = Firebase.firestore

    override fun getGroupPurchases(groupId: String): Flow<ProblemOr<List<PurchaseEntity>>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.PURCHASES)
            .orderBy(PurchaseEntityData::time.name, Query.Direction.DESCENDING)
            .addSnapshotListener { result, e ->
                tryToDeserialize("group purchases", e) {
                    result?.documents?.let { documents ->
                        val purchases = documents.map { purchaseDoc ->
                            purchaseDoc.toObject(PurchaseEntityReadData::class.java)?.let { genericPurchaseData ->
                                genericPurchaseData.toPurchaseEntity(purchaseDoc.id).fold(
                                    { problem ->
                                        sendProblemAndLogError(
                                            problem,
                                            "Error mapping purchase group [$groupId] to specific",
                                        )
                                        null
                                    },
                                    { purchaseEntity -> purchaseEntity }
                                )
                            } ?: run { Log.e(APP_TAG, "Error mapping purchases for group [$groupId]"); null }
                        }
                        trySend(purchases.filterNotNull().right())
                    }
                }
            }
        awaitClose { }
    }

    override fun getPurchase(groupId: String, purchaseId: String): Flow<ProblemOr<PurchaseEntity>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.PURCHASES)
            .document(purchaseId)
            .addSnapshotListener { result, e ->
                tryToDeserialize("purchase", e) {
                    result?.let { document ->
                        document.toObject(PurchaseEntityReadData::class.java)
                            ?.let { genericPurchaseData ->
                                genericPurchaseData.toPurchaseEntity(result.id).fold(
                                    { problem ->
                                        sendProblemAndLogError(
                                            problem,
                                            "Error mapping purchase group [$groupId] id [$purchaseId] to specific",
                                        )
                                    },
                                    { purchaseEntity ->
                                        trySend(purchaseEntity.right())
                                    }
                                )
                            } ?: sendProblemAndLogError("Error mapping purchase group [$groupId] id [$purchaseId]")
                    }
                }
            }
        awaitClose {}
    }

    override fun createPurchase(
        groupId: String,
        purchaseData: PurchaseEntityData,
    ): Flow<ProblemOr<String>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.PURCHASES)
            .add(purchaseData)
            .addOnSuccessListener { result ->
                if (result.id.isNotBlank()) {
                    trySend(result.id.right())
                } else {
                    sendProblemAndLogError(
                        "Error createPurchase [$purchaseData] group [$groupId] - blank id [${result.id}]")
                }
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error createPurchase [$purchaseData] group [$groupId]", e)
            }
        awaitClose {}
    }

    override fun updatePurchase(groupId: String, purchase: PurchaseEntity): Flow<ProblemOr<Unit>> = callbackFlow {
        purchase.transactionId?.let { purchaseId ->
            purchase.purchase?.let { purchaseData ->
                db.collection(FireStoreCollections.GROUPS)
                    .document(groupId)
                    .collection(FireStoreCollections.GroupSubCollections.PURCHASES)
                    .document(purchaseId)
                    .set(purchaseData.toUpdateData(), SetOptions.merge())
                    .addOnSuccessListener {
                        trySend(Unit.right())
                    }
                    .addOnFailureListener { e ->
                        sendProblemAndLogWarn("Error updatePurchase [$purchase] group [$groupId]", e)
                    }
            } ?: sendProblemAndLogError("No purchase in updatePurchase"); 0
        } ?: sendProblemAndLogError("No purchase id in updatePurchase")
        awaitClose {}
    }

    override fun deletePurchase(groupId: String, purchaseId: String): Flow<ProblemOr<Unit>> = callbackFlow {
        db.collection(FireStoreCollections.GROUPS)
            .document(groupId)
            .collection(FireStoreCollections.GroupSubCollections.PURCHASES)
            .document(purchaseId)
            .delete()
            .addOnSuccessListener {
                trySend(Unit.right())
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error deletePurchase [$purchaseId] group [$groupId]", e)
            }
        awaitClose {}
    }
}
