package cz.cvut.fit.molinlen.android.debttracker.feature.groupdetail

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupDetail

data class GroupDetailViewState(
    val usersGroup: UsersGroupDetail? = null,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
