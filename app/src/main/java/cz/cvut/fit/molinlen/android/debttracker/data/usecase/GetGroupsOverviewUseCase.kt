package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import android.util.Log
import arrow.core.flatMap
import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.computation.GroupStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.computation.OverallStatusCalculator
import cz.cvut.fit.molinlen.android.debttracker.data.computation.fake.GroupStatusCalculatorFake
import cz.cvut.fit.molinlen.android.debttracker.data.computation.fake.OverallStatusCalculatorFake
import cz.cvut.fit.molinlen.android.debttracker.data.computation.impl.GroupStatusCalculatorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.computation.impl.OverallStatusCalculatorImpl
import cz.cvut.fit.molinlen.android.debttracker.data.model.GlobalOwedSumsOverview
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupsOverview
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.UserRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.UserRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.UserRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface GetGroupsOverviewUseCase {
    suspend fun getGroupsOverview(userId: String): Flow<ProblemOr<UsersGroupsOverview>>
}

class GetGroupsOverviewUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
    private val userRepository: UserRepository = if (USE_FAKE_DATA) UserRepositoryFake() else UserRepositoryImpl(),
    private val overviewCalculator: OverallStatusCalculator = if (USE_FAKE_DATA) OverallStatusCalculatorFake() else OverallStatusCalculatorImpl(),
    private val groupStatusCalculator: GroupStatusCalculator = if (USE_FAKE_DATA) GroupStatusCalculatorFake() else GroupStatusCalculatorImpl(),
) : GetGroupsOverviewUseCase {
    override suspend fun getGroupsOverview(userId: String): Flow<ProblemOr<UsersGroupsOverview>> = flow {
        var sums: GlobalOwedSumsOverview? = null
        groupRepository.getGroupList(userId).collect { groupResults ->
            val previewsOrProblem = groupResults.flatMap { groups ->
                groupStatusCalculator.computeGroupPreviews(groups)
            }
            emit(previewsOrProblem.map { previews ->
                UsersGroupsOverview(
                    userHasPrimaryCurrency = null,
                    sumsOverview = sums,
                    usersGroups = previews,
                )
            })
            previewsOrProblem.map { previews ->
                userRepository.getUserInfo(userId).collect { userInfoResult ->
                    val sumsResult = userInfoResult.map { userInfo ->
                        userInfo.primaryCurrency?.let { currency ->
                            overviewCalculator.calculateOverallUserStatusFromPreview(previews, currency)
                        }
                    }
                    sums = sumsResult.fold(
                        { problem ->
                            //todo do we want to swallow the error here, or propagate it ?
                            Log.e(APP_TAG, "Cannot get user info [$userId]: $problem", problem.cause)
                            null
                        },
                        {
                            it?.fold(
                                { problem ->
                                    Log.e(APP_TAG, "Cannot calculate global sums overview: [$problem]", problem.cause)
                                    null
                                },
                                { globalOwedSumsOverview -> globalOwedSumsOverview }
                            )
                        }
                    )
                    emit(UsersGroupsOverview(
                        userHasPrimaryCurrency = userInfoResult.fold({ null }, { it.primaryCurrency != null }),
                        sumsOverview = sums,
                        usersGroups = previews,
                    ).right())
                }
            }
        }
    }
}
