package cz.cvut.fit.molinlen.android.debttracker

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import cz.cvut.fit.molinlen.android.debttracker.navigation.DebtTrackerNavGraph
import cz.cvut.fit.molinlen.android.debttracker.ui.components.basicTextSelectionColors
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.DebtTrackerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()

            DebtTrackerTheme {
                CompositionLocalProvider(
                    LocalTextSelectionColors provides basicTextSelectionColors
                ) {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background,
                    ) {
                        DebtTrackerNavGraph(
                            navController = navController,
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DebtTrackerTheme {
        Greeting("Android")
    }
}
