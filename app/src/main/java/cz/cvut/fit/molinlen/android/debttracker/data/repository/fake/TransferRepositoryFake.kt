package cz.cvut.fit.molinlen.android.debttracker.data.repository.fake

import arrow.core.right
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.amountFromDouble
import cz.cvut.fit.molinlen.android.debttracker.data.repository.TransferRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.john
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.FakeGroupParticipants.user
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import java.time.OffsetDateTime
import java.time.ZoneOffset

class TransferRepositoryFake : TransferRepository {

    override suspend fun createTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<String>> = flowOf(transferDetail.transactionId.right())

    override suspend fun updateTransfer(
        userId: String,
        groupId: String,
        transfer: TransferDetail,
    ): Flow<ProblemOr<Unit>> = flowOf(Unit.right())

    override suspend fun deleteTransfer(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<Unit>> = flowOf(Unit.right())

    override suspend fun getTransferDetail(
        userId: String,
        groupId: String,
        transferId: String,
    ): Flow<ProblemOr<TransferDetail>> = flowOf(transferDetail.right())

    private val transferDetail = TransferDetail(
        transactionId = cz.cvut.fit.molinlen.android.debttracker.data.computation.fake.randomId(),
        from = user,
        to = john,
        amount = amountFromDouble(450.0),
        comments = listOf(
            CommentDetail(
                writerUserId = "9IePFt8v0eNqtioh1GyGK2RJj129",
                content = "Hi",
                time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                edited = false,
            ),
            CommentDetail(
                writerUserId = "4IePFt8v0eNqtioh1GyGK2RJj123",
                content = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam rhoncus aliquam metus. Phasellus rhoncus.",
                time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                edited = true,
            ),
            CommentDetail(
                writerUserId = "8IePFt8v0eNqtioh1GyGK2RJj128",
                content = "Lipsum",
                time = OffsetDateTime.of(2022, 3, 18, 18, 20, 53, 6, ZoneOffset.ofHours(1)),
                edited = true,
            ),
        ),
        time = OffsetDateTime.of(2022, 3, 9, 14, 0, 0, 0, ZoneOffset.ofHours(1)),
        note = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam rhoncus aliquam metus. Phasellus rhoncus. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Fusce consectetuer risus a nunc. Duis viverra diam non justo. Et harum quidem rerum facilis est et expedita distinctio. Duis condimentum augue id magna semper rutrum.",
    )
}
