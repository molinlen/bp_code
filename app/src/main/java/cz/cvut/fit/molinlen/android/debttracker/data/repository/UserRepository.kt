package cz.cvut.fit.molinlen.android.debttracker.data.repository

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserBasic
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserUpdateData
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    suspend fun getUserInfo(
        userId: String,
    ): Flow<ProblemOr<UserBasic>>

    suspend fun updateUserInfo(
        userId: String,
        data: UserUpdateData,
    ): Flow<ProblemOr<Unit>>
}
