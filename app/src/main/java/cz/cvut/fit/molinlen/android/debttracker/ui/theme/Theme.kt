package cz.cvut.fit.molinlen.android.debttracker.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val LightColorPalette = lightColors(
    primary = md_theme_light_primaryContainer,
    onPrimary = md_theme_light_onPrimaryContainer,
    primaryVariant = md_theme_light_primary,
    secondary = md_theme_light_secondaryContainer,
    onSecondary = md_theme_light_onSecondaryContainer,
    secondaryVariant = md_theme_light_secondary,
    background = md_theme_light_background,
    onBackground = md_theme_light_onBackground,
    surface = md_theme_light_background,
    onSurface = md_theme_light_onBackground,
    error = md_theme_light_errorContainer,
    onError = md_theme_light_onErrorContainer,
)

private val DarkColorPalette = darkColors(
    primary = md_theme_dark_primaryContainer,
    onPrimary = md_theme_dark_onPrimaryContainer,
    primaryVariant = md_theme_dark_primary,
    secondary = md_theme_dark_secondaryContainer,
    onSecondary = md_theme_dark_onSecondaryContainer,
    secondaryVariant = md_theme_dark_secondary,
    background = md_theme_dark_background,
    onBackground = md_theme_dark_onBackground,
    surface = md_theme_dark_background,
    onSurface = md_theme_dark_onBackground,
    error = md_theme_dark_errorContainer,
    onError = md_theme_dark_onErrorContainer,
)

@Composable
fun DebtTrackerTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

@Suppress("unused")
object AccentColors {
    private val isLight: Boolean
        @Composable
        get() = MaterialTheme.colors.isLight

    val surfaceAccentMild: Color
        @Composable
        get() = if (isLight) light_surface_accent_mild else dark_surface_accent_mild
    val surfaceAccentMedium: Color
        @Composable
        get() = if (isLight) light_surface_accent_medium else dark_surface_accent_medium
    val surfaceAccentStrong: Color
        @Composable
        get() = if (isLight) light_surface_accent_strong else dark_surface_accent_strong

    val redContent: Color
        @Composable
        get() = if (isLight) light_red_content else dark_red_content

    val greenContent: Color
        @Composable
        get() = if (isLight) light_green_content else dark_green_content
}

