package cz.cvut.fit.molinlen.android.debttracker.feature.registration

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.validation.RegistrationInputProblem
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import java.util.*

@Composable
fun RegistrationScreen(
    email: String? = null,
    password: String? = null,
    navigateAfterCreateAccount: () -> Unit,
) {
    val viewModel: RegistrationViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.updateEmailAndPassword(email = email ?: "", password = password ?: "")
    }

    RegistrationScreenImpl(
        viewState = viewModel.viewState.collectAsState().value,
        minPasswordLength = viewModel.minPasswordLength,
        updateEmail = { viewModel.updateEmail(it) },
        updateFirstPassword = { viewModel.updatePassword(it) },
        updateRepeatedPassword = { viewModel.updateRepeatedPassword(it) },
        updateCheckboxChecked = { checked -> viewModel.updateCheckboxChecked(checked) },
        onCreateAccount = { viewModel.register(navigateAfterCreateAccount) }
    )
}

private val emailProblems = EnumSet.of(
    RegistrationInputProblem.INVALID_EMAIL,
    RegistrationInputProblem.EMAIL_ALREADY_REGISTERED,
)

@Composable
fun RegistrationScreenImpl(
    viewState: RegistrationViewState,
    minPasswordLength: Int,
    updateEmail: (String) -> Unit,
    updateFirstPassword: (String) -> Unit,
    updateRepeatedPassword: (String) -> Unit,
    updateCheckboxChecked: (Boolean) -> Unit,
    onCreateAccount: () -> Unit,
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            ScreenHeader(
                headingText = stringResource(R.string.registration),
                backgroundColor = ScreenHeaderColors.editScreenColor,
            )
        },
    ) { paddingValues ->
        if (viewState.processing) {
            LoadingOverlay()
        }
        Column(
            modifier = Modifier
                .screenContentModifier(paddingValues)
                .verticalScroll(rememberScrollState())
                .clearFocusOnTap()
        ) {
            val extraVerticalPaddingModifier = Modifier.padding(vertical = 4.dp)
            ContentTopSpacer(10.dp)
            BasicTextField(
                label = stringResource(R.string.email),
                value = viewState.email,
                onValueChange = updateEmail,
                supportingText = stringResource(R.string.field_cant_be_blank),
                isError = viewState.inputProblems.any { it in emailProblems },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                modifier = extraVerticalPaddingModifier,
            )
            PasswordTextField(
                label = stringResource(R.string.password),
                value = viewState.password,
                onValueChange = updateFirstPassword,
                supportingText = stringResource(R.string.min_password_length_1)
                        + minPasswordLength + stringResource(R.string.min_password_length_2),
                isError = viewState.inputProblems.any { it == RegistrationInputProblem.PASSWORD_TOO_SHORT },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                modifier = extraVerticalPaddingModifier,
            )
            PasswordTextField(
                label = stringResource(R.string.password_repeat),
                value = viewState.repeatedPassword,
                onValueChange = updateRepeatedPassword,
                supportingText = stringResource(R.string.passwords_must_match),
                isError = viewState.inputProblems.any { it == RegistrationInputProblem.PASSWORDS_NOT_MATCH },
                modifier = extraVerticalPaddingModifier,
            )
            BasicLeftCheckboxWithText(
                checked = viewState.checkboxChecked,
                onCheckedChange = updateCheckboxChecked,
                text = { Text(text = stringResource(R.string.account_creation_checkbox_text)) },
                needsToBeChecked = true,
                modifier = Modifier.padding(vertical = 15.dp),
            )
            CreateAccountButton(
                createAccount = onCreateAccount,
                enabled = viewState.inputProblems.isEmpty() || viewState.registrationResult?.isRight() == true,
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(vertical = 10.dp),
                previousError = viewState.registrationResult?.fold({ it }, { null }),
            )
        }
    }
}

@Composable
fun CreateAccountButton(
    createAccount: () -> Unit,
    enabled: Boolean,
    modifier: Modifier,
    previousError: Problem?,
) {
    BasicRectangleButton(
        text = stringResource(R.string.create_account),
        onClick = createAccount,
        modifier = modifier,
        enabled = enabled,
    )
    previousError?.let {
        ErrorCard(
            text = buildErrorMessage(
                errorMessage = stringResource(R.string.try_again_prompt),
                errorDetails = it.description,
                errorCause = it.cause,
            ),
        )
    }
}
