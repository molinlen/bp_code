package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.*
import kotlinx.coroutines.flow.Flow

class FireStoreDbImpl(
    private val userDb: UserDb = FireStoreUserDbImpl(),
    private val groupDb: GroupDb = FireStoreGroupDbImpl(),
    private val purchaseDb: PurchaseDb = FireStorePurchaseDbImpl(),
    private val transferDb: TransferDb = FireStoreTransferDbImpl(),
) : Db {
    //users
    override fun getUserInfo(userId: String): Flow<ProblemOr<UserEntityData>> =
        userDb.getUserInfo(userId = userId)

    override fun getUsersGroupParticipants(userId: String): Flow<ProblemOr<List<UsersGroupEntityData>>> =
        userDb.getUsersGroupParticipants(userId = userId)

    override fun getUsersGroupParticipant(userId: String, groupId: String): Flow<ProblemOr<UsersGroupEntityData>> =
        userDb.getUsersGroupParticipant(userId = userId, groupId = groupId)

    override fun linkGroupSetParticipant(
        userId: String, groupId: String, userParticipantId: String?,
    ): Flow<ProblemOr<Unit>> =
        userDb.linkGroupSetParticipant(userId = userId, groupId = groupId, userParticipantId = userParticipantId)

    override fun unlinkUserFromGroup(userId: String, groupId: String): Flow<ProblemOr<Unit>> =
        userDb.unlinkUserFromGroup(userId = userId, groupId = groupId)

    override fun updateUserInfo(userId: String, userInfo: UserEntityUpdateData): Flow<ProblemOr<Unit>> =
        userDb.updateUserInfo(userId = userId, userInfo = userInfo)

    // groups
    override fun getGroupInfo(groupId: String): Flow<ProblemOr<GroupEntity>> =
        groupDb.getGroupInfo(groupId = groupId)

    override fun getGroupParticipants(groupId: String): Flow<ProblemOr<List<ParticipantEntity>>> =
        groupDb.getGroupParticipants(groupId = groupId)

    override fun createGroup(groupData: GroupEntityData): Flow<ProblemOr<String>> =
        groupDb.createGroup(groupData = groupData)

    override fun updateGroup(group: GroupEntity): Flow<ProblemOr<Unit>> =
        groupDb.updateGroup(group = group)

    override fun addParticipants(
        groupId: String,
        newParticipantsData: Set<ParticipantEntityData>,
    ): Flow<ProblemOr<Unit>> =
        groupDb.addParticipants(groupId = groupId, newParticipantsData = newParticipantsData)

    // transactions: purchases
    override fun getGroupPurchases(groupId: String): Flow<ProblemOr<List<PurchaseEntity>>> =
        purchaseDb.getGroupPurchases(groupId = groupId)

    override fun getPurchase(groupId: String, purchaseId: String): Flow<ProblemOr<PurchaseEntity>> =
        purchaseDb.getPurchase(groupId = groupId, purchaseId = purchaseId)

    override fun createPurchase(groupId: String, purchaseData: PurchaseEntityData): Flow<ProblemOr<String>> =
        purchaseDb.createPurchase(groupId = groupId, purchaseData = purchaseData)

    override fun updatePurchase(groupId: String, purchase: PurchaseEntity): Flow<ProblemOr<Unit>> =
        purchaseDb.updatePurchase(groupId = groupId, purchase = purchase)

    override fun deletePurchase(groupId: String, purchaseId: String): Flow<ProblemOr<Unit>> =
        purchaseDb.deletePurchase(groupId = groupId, purchaseId = purchaseId)

    // transactions: transfers
    override fun getGroupTransfers(groupId: String): Flow<ProblemOr<List<TransferEntity>>> =
        transferDb.getGroupTransfers(groupId = groupId)

    override fun getTransfer(groupId: String, transferId: String): Flow<ProblemOr<TransferEntity>> =
        transferDb.getTransfer(groupId = groupId, transferId = transferId)

    override fun createTransfer(groupId: String, transferData: TransferEntityData): Flow<ProblemOr<String>> =
        transferDb.createTransfer(groupId = groupId, transferData = transferData)

    override fun updateTransfer(groupId: String, transfer: TransferEntity): Flow<ProblemOr<Unit>> =
        transferDb.updateTransfer(groupId = groupId, transfer = transfer)

    override fun deleteTransfer(groupId: String, transferId: String): Flow<ProblemOr<Unit>> =
        transferDb.deleteTransfer(groupId = groupId, transferId = transferId)
}
