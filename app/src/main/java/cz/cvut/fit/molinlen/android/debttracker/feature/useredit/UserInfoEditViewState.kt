package cz.cvut.fit.molinlen.android.debttracker.feature.useredit

import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UserUpdateInputData

data class UserInfoEditViewState(
    val userData: UserUpdateInputData? = null,
    val saveError: Problem? = null,
    val processing: Boolean = false,
    val loading: Boolean = false,
    val error: Problem? = null,
) {
    constructor(errorMessage: String) : this(error = Problem(description = errorMessage))
}
