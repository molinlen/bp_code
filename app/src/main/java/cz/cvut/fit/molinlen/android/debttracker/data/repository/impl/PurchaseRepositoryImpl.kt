package cz.cvut.fit.molinlen.android.debttracker.data.repository.impl

import arrow.core.computations.either
import arrow.core.flatten
import arrow.core.left
import arrow.core.zip
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.PurchaseDetail
import cz.cvut.fit.molinlen.android.debttracker.data.repository.PurchaseRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.Db
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl.FireStoreDbImpl
import cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf

class PurchaseRepositoryImpl(
    private val db: Db = FireStoreDbImpl(),
) : PurchaseRepository {
    override suspend fun getPurchaseDetail(
        userId: String,
        groupId: String,
        purchaseId: String,
    ): Flow<ProblemOr<PurchaseDetail>> = flow {
        combine(
            db.getPurchase(groupId = groupId, purchaseId = purchaseId),
            db.getGroupParticipants(groupId)
        ) { purchaseEither, participantsEither ->
            val result = purchaseEither.zip(participantsEither) { purchaseEntity, participantEntities ->
                either.eager {
                    val participants = participantEntities.map { it.toModel().bind() }
                    purchaseEntity.toModelWithParticipants(participants.associatedByParticipantIds()).bind()
                }
            }.flatten()
            result
        }.collect { emit(it) }
    }

    override suspend fun createPurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<String>> =
        db.createPurchase(groupId, purchase.toEntityData())

    override suspend fun updatePurchase(
        userId: String,
        groupId: String,
        purchase: PurchaseDetail,
    ): Flow<ProblemOr<Unit>> {
        return if (purchase.transactionId.isNotBlank()) {
            db.updatePurchase(groupId, purchase.toEntity())
        } else {
            flowOf(Problem("Blank transactionId in updatePurchase").left())
        }
    }

    override suspend fun deletePurchase(userId: String, groupId: String, purchaseId: String): Flow<ProblemOr<Unit>> =
        db.deletePurchase(groupId = groupId, purchaseId = purchaseId)
}
