package cz.cvut.fit.molinlen.android.debttracker.feature.transactionedit

import androidx.compose.foundation.layout.Row
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.ui.components.DatePicker
import cz.cvut.fit.molinlen.android.debttracker.ui.components.DropDownPicker
import cz.cvut.fit.molinlen.android.debttracker.ui.components.ErrorCard
import cz.cvut.fit.molinlen.android.debttracker.ui.components.buildErrorMessage
import java.time.OffsetDateTime

@Composable
internal fun CurrencyPicker(
    currencyPickerExpanded: MutableState<Boolean>,
    pickedCurrency: CurrencyCode,
    onUpdateCurrency: (CurrencyCode) -> Unit,
    modifier: Modifier = Modifier,
    supportingText: String? = null,
    onlyShowSupportingTextForError: Boolean = true,
) {
    DropDownPicker(
        menuExpanded = currencyPickerExpanded,
        pickedText = pickedCurrency.name,
        contentDescription = stringResource(R.string.pick_currency),
        supportingText = supportingText,
        onlyShowSupportingTextForError = onlyShowSupportingTextForError,
        modifier = modifier,
    ) {
        CurrencyCode.values().map {
            DropdownMenuItem(
                onClick = {
                    onUpdateCurrency(it)
                    currencyPickerExpanded.value = false
                }
            ) {
                Text(it.name) //todo language-specific mapping
            }
        }
    }
}

@Composable
internal fun DatePickerRow(
    datePickerExpanded: MutableState<Boolean>,
    time: OffsetDateTime,
    modifier: Modifier,
    textModifier: Modifier,
    supportingText: String? = null,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier,
    ) {
        Text(text = stringResource(R.string.date_label), modifier = textModifier)
        DatePicker(datePickerExpanded = datePickerExpanded, time = time, supportingText = supportingText)
    }
}

@Composable
internal fun TransactionSaveErrorCard(problem: Problem) {
    ErrorCard(
        text = buildErrorMessage(
            errorMessage = stringResource(R.string.transaction_save_error_message),
            errorDetails = problem.description,
            errorCause = problem.cause,
        ),
    )
}
