package cz.cvut.fit.molinlen.android.debttracker.ui.components

import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.material.*
import androidx.compose.material.TextFieldDefaults.IconOpacity
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import java.time.LocalDate
import java.time.OffsetDateTime

val basicTextSelectionColors
    @Composable
    get() = TextSelectionColors(
        handleColor = MaterialTheme.colors.secondaryVariant,
        backgroundColor = MaterialTheme.colors.secondary,
    )

@Composable
fun BasicSelectionContainer(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) = SelectionContainer(modifier = modifier, content = content)

fun Modifier.clearFocusOnTap(): Modifier = composed {
    val focusManager = LocalFocusManager.current
    this.pointerInput(Unit) {
        detectTapGestures(
            onTap = {
                focusManager.clearFocus()
            },
        )
    }
}

@Composable
fun BasicTextField(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    supportingText: String? = null,
    isError: Boolean = false,
    onlyShowSupportingTextForError: Boolean = true,
    enabled: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
    keyboardActions: KeyboardActions = defaultKeyboardActions(),
    singleLine: Boolean = true,
) {
    Column(modifier = modifier) {
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            label = { Text(text = label) },
            modifier = Modifier.tweakForTextField(hasSupportingText = supportingText != null),
            colors = generalTextFieldColors(),
            isError = isError,
            enabled = enabled,
            singleLine = singleLine,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
        )
        supportingText?.let {
            TextFieldSupportingText(
                isError = isError,
                onlyShowSupportingTextForError = onlyShowSupportingTextForError,
                supportingText = it,
                modifier = Modifier.tweakForTextFieldSupportingText(),
            )
        }
    }
}

@Composable
fun PasswordTextField(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    supportingText: String? = null,
    isError: Boolean = false,
    onlyShowSupportingTextForError: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
    keyboardActions: KeyboardActions = defaultKeyboardActions(),
) {
    val showPassword = remember { mutableStateOf(false) }

    Column(modifier = modifier) {
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            label = { Text(text = label) },
            modifier = Modifier.tweakForTextField(hasSupportingText = supportingText != null),
            colors = generalTextFieldColors(),
            isError = isError,
            singleLine = true,
            visualTransformation = when {
                showPassword.value -> VisualTransformation.None
                else -> PasswordVisualTransformation()
            },
            keyboardOptions = keyboardOptions.copy(keyboardType = KeyboardType.Password),
            keyboardActions = keyboardActions,
            trailingIcon = {
                IconButton(onClick = { showPassword.value = showPassword.value.not() }) {
                    Icon(
                        painter = when {
                            showPassword.value -> painterResource(R.drawable.eye_crossed)
                            else -> painterResource(R.drawable.eye)
                        },
                        contentDescription = stringResource(R.string.show_password),
                    )
                }
            }
        )
        supportingText?.let {
            TextFieldSupportingText(
                isError = isError,
                onlyShowSupportingTextForError = onlyShowSupportingTextForError,
                supportingText = it,
                modifier = Modifier.tweakForTextFieldSupportingText(),
            )
        }
    }
}

@Composable
internal fun TextFieldSupportingText(
    isError: Boolean,
    onlyShowSupportingTextForError: Boolean,
    supportingText: String,
    modifier: Modifier,
) {
    CompositionLocalProvider(LocalContentAlpha provides if (isError) ContentAlpha.high else ContentAlpha.disabled) {
        Text(
            text = when {
                isError.not() && onlyShowSupportingTextForError -> ""
                else -> supportingText
            },
            style = MaterialTheme.typography.caption,
            color = if (isError) MaterialTheme.colors.onError else Color.Unspecified,
            modifier = modifier,
        )
    }
}

@Composable
fun defaultKeyboardActions(
    onConfirmAlso: () -> Unit = {},
): KeyboardActions {
    val focusManager = LocalFocusManager.current
    val clearAndAlso: KeyboardActionScope.() -> Unit = {
        focusManager.clearFocus()
        onConfirmAlso()
    }
    return KeyboardActions(
        onDone = clearAndAlso,
        onNext = {
            val moved = focusManager.moveFocus(FocusDirection.Down)
            if (moved.not()) {
                focusManager.clearFocus()
            }
            onConfirmAlso()
        },
        onGo = clearAndAlso,
        onSearch = clearAndAlso,
        onSend = clearAndAlso,
    )
}

@Composable
fun generalTextFieldColors(): TextFieldColors {
    val basicFocusedColor = MaterialTheme.colors.primaryVariant

    return TextFieldDefaults.outlinedTextFieldColors(
        cursorColor = basicFocusedColor,
        focusedLabelColor = basicFocusedColor,
        focusedBorderColor = basicFocusedColor,
        errorCursorColor = AccentColors.redContent,
        errorLabelColor = MaterialTheme.colors.onError,
        errorTrailingIconColor = MaterialTheme.colors.onSurface.copy(alpha = IconOpacity),
    )
}

val textFieldVerticalPadding = 1.dp

fun Modifier.tweakForTextField(hasSupportingText: Boolean): Modifier {
    return this
        .fillMaxWidth()
        .offset(y = (-4).dp)
        .alsoWhen {
            when {
                hasSupportingText -> it.padding(top = textFieldVerticalPadding)
                else -> it.padding(vertical = textFieldVerticalPadding)
            }
        }
}

private fun Modifier.tweakForTextFieldSupportingText(): Modifier {
    return this
        .offset(y = (-4).dp)
        .padding(bottom = textFieldVerticalPadding) // the negative offset creates enough bottom "padding" on its own
}

internal fun Modifier.tweakForGeneralSupportingText(): Modifier {
    return this.offset(y = (-6).dp)
}

fun Modifier.tweakForLabeledPickerRowLeadingText(): Modifier {
    return this.padding(bottom = 16.dp) // push leading text up to center against main row, "ignoring" support text
}


fun OffsetDateTime.toShortPrettyStringDate(
    leadingZeroes: Boolean = true,
    includeYear: Boolean = true,
): String {
    return this.toLocalDate().toShortPrettyString(leadingZeroes = leadingZeroes, includeYear = includeYear)
}

fun LocalDate.toShortPrettyString(
    leadingZeroes: Boolean = true,
    includeYear: Boolean = true,
): String {
    val dayPrefix = if (leadingZeroes && this.dayOfMonth < 10) "0" else ""
    val monthPrefix = if (leadingZeroes && this.month.value < 10) "0" else ""
    val yearValue = if (includeYear) this.year.toString().substring(2) else ""
    return "${dayPrefix}${this.dayOfMonth}.${monthPrefix}${this.month.value}.${yearValue}"
}

fun OffsetDateTime.toPrettyStringDate(
    leadingZeroes: Boolean = true,
    includeYear: Boolean = true,
): String {
    return this.toLocalDate().toPrettyString(leadingZeroes = leadingZeroes, includeYear = includeYear)
}

fun LocalDate.toPrettyString(
    leadingZeroes: Boolean = true,
    includeYear: Boolean = true,
): String {
    val dayPrefix = if (leadingZeroes && this.dayOfMonth < 10) "0" else ""
    val monthPrefix = if (leadingZeroes && this.month.value < 10) "0" else ""
    val yearValue = if (includeYear) this.year.toString() else ""
    return "${dayPrefix}${this.dayOfMonth}.${monthPrefix}${this.month.value}.${yearValue}"
}
