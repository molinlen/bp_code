package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import cz.cvut.fit.molinlen.android.debttracker.data.model.*

internal fun UsersGroupBasic.toDetail(participants: Set<ParticipantWithStatus>): UsersGroupDetail {
    return UsersGroupDetail(
        group = GroupDetail(
            groupId = group.groupId,
            name = group.name,
            defaultCurrency = group.defaultCurrency,
            transactions = group.transactions,
            participants = participants,
        ),
        userParticipantId = userParticipantId,
    )
}

internal fun UsersGroupBasic.toPreview(userStatus: Amount): UsersGroupPreview {
    return UsersGroupPreview(
        group = GroupPreview(
            groupId = group.groupId,
            name = group.name,
            defaultCurrency = group.defaultCurrency,
            currentOwnStatus = userStatus,
        ),
        userParticipantId = userParticipantId,
    )
}
