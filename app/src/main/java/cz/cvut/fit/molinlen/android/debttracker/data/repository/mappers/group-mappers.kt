package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import arrow.core.Either
import arrow.core.computations.either
import arrow.core.flatMap
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.*

fun Group.toEntityData(): GroupEntityData = GroupEntityData(
    name = name,
    defaultCurrency = defaultCurrency,
    participants = when (this) {
        is GroupBasic -> participants.toParticipantEntityDataMap()
        is GroupDetail -> participants.toParticipantEntityDataMap()
        else -> emptyMap()
    },
)

fun Collection<Participant>.toParticipantEntityDataMap(): Map<String, ParticipantEntityData> =
    this.map { it.toEntity() }.associate { it.participantId to it.participant }

fun GroupInfoBasic.toEntity(): GroupEntity = GroupEntity(
    groupId = groupId,
    group = toEntityData(),
    transfers = emptyList(),
    purchases = emptyList(),
)

fun GroupEntity.toModel(): ProblemOr<GroupBasic> {
    val mappedParticipants = either.eager {
        group?.participants
            ?.map { ParticipantEntity(participantId = it.key, participant = it.value).toModel().bind() }
            ?.associatedByParticipantIds()
            ?: emptyMap()
    }
    return mappedParticipants.flatMap { participants ->
        either.eager { transfers.map { it.toModelWithParticipants(participants).bind() } }
            .flatMap { transfers ->
                either.eager { purchases.map { it.toModelWithParticipants(participants).bind() } }
                    .flatMap { purchases ->
                        Either.catch {
                            GroupBasic(
                                groupId = groupId!!,
                                name = group?.name!!,
                                defaultCurrency = group?.defaultCurrency!!,
                                transactions = mergeSortedTransactions(transfers, purchases),
                                participants = participants.values.toSet(),
                            )
                        }.mapConversionThrowable("GroupEntity [$groupId]")
                    }
            }
    }
}

fun GroupEntityData.toUpdateData() = GroupEntityInfoUpdateData(
    name = name,
    defaultCurrency = defaultCurrency,
)

// inspired by https://stackoverflow.com/questions/65002540/kotlin-merging-pre-sorted-sequences-into-a-sorted-sequence
// but not written generically as suggested by the answers - so far only this specific case of wanting to merge pre-sorted
// collections into a sorted one was discovered, and this function may perform a teeny-tiny bit better than more generic ones
fun mergeSortedTransactions(one: List<Transaction>, two: List<Transaction>): List<Transaction> {
    val a = MutableIteratorValuePair(one.iterator())
    val b = MutableIteratorValuePair(two.iterator())
    val x = sequence {
        while (a.v !== null || b.v != null) {
            val mostRecent = mostRecentTransaction(a.v, b.v)!!
            yield(mostRecent)
            when (mostRecent.transactionId) {
                a.v?.transactionId -> a.v = a.it.nextOrNull()
                b.v?.transactionId -> b.v = b.it.nextOrNull()
            }
        }
    }
    return x.toList()
}

fun <T> Iterator<T>.nextOrNull() = if (this.hasNext()) this.next() else null

fun mostRecentTransaction(a: Transaction?, b: Transaction?): Transaction? {
    return when {
        a == null -> b
        b == null -> a
        a.time > b.time -> a
        else -> b // behavior for equal transaction time does not really matter here, as long as it is consistent
    }
}

data class MutableIteratorValuePair<V>(
    var it: Iterator<V>,
    var v: V?,
) {
    constructor(iterator: Iterator<V>) : this(iterator, iterator.nextOrNull())
}
