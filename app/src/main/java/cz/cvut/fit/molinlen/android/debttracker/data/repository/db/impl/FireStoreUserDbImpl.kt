package cz.cvut.fit.molinlen.android.debttracker.data.repository.db.impl

import android.util.Log
import arrow.core.left
import arrow.core.right
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import cz.cvut.fit.molinlen.android.debttracker.APP_TAG
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.UserDb
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.sendProblemAndLogWarn
import cz.cvut.fit.molinlen.android.debttracker.data.repository.db.tryToDeserialize
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.GroupEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UserEntityData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UserEntityUpdateData
import cz.cvut.fit.molinlen.android.debttracker.data.repository.entity.UsersGroupEntityData
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class FireStoreUserDbImpl : UserDb {
    private val db = Firebase.firestore

    override fun getUserInfo(userId: String): Flow<ProblemOr<UserEntityData>> = callbackFlow {
        db.collection(FireStoreCollections.USERS)
            .document(userId)
            .addSnapshotListener { result, e ->
                e
                    ?.let { exception ->
                        when (exception.code) {
                            FirebaseFirestoreException.Code.NOT_FOUND -> trySend(UserEntityData().right()) // user did not create data entity yet -> sending empty object
                            else -> sendProblemAndLogWarn(message = "Error getting user data", e = exception)
                        }
                    }
                    ?: tryToDeserialize("user data", null) {
                        result?.let { document ->
                            document.toObject(UserEntityData::class.java)
                                ?.let {
                                    trySend(it.right())
                                } ?: trySend(UserEntityData().right()) // user did not create data entity yet -> sending empty object
                        }
                    }
            }
        awaitClose { }
    }

    override fun getUsersGroupParticipants(userId: String): Flow<ProblemOr<List<UsersGroupEntityData>>> = callbackFlow {
        db.collection(FireStoreCollections.USERS)
            .document(userId)
            .collection(FireStoreCollections.UserSubCollections.GROUPS)
            .addSnapshotListener { result, e ->
                tryToDeserialize("user's groups", e) {
                    result?.documents?.let { documents ->
                        val usersGroups = documents.map { document ->
                            document.toObject(UsersGroupEntityData::class.java)
                                ?: run {
                                    Log.e(APP_TAG, "Error mapping user [$userId] groups for [${document.id}]"); null
                                }
                        }
                        trySend(usersGroups.filterNotNull().right())
                    }
                }
            }
        awaitClose { }
    }

    override fun getUsersGroupParticipant(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupEntityData>> = callbackFlow {
        db.collection(FireStoreCollections.USERS)
            .document(userId)
            .collection(FireStoreCollections.UserSubCollections.GROUPS)
            .document(groupId)
//            .whereEqualTo(UsersGroupEntityData::groupId.name, groupId)
            .addSnapshotListener { result, e ->
                tryToDeserialize("user's group participant", e) {
                    result?.let { document ->
                        val usersGroup = document.toObject(UsersGroupEntityData::class.java)
                            ?: run {
                                Log.e(APP_TAG, "Error mapping user [$userId] group for [${document.id}]"); null
                            }
                        trySend(usersGroup?.right() ?: Problem("No matching user's group found").left())
                    }
                }
            }
        awaitClose { }
    }

    override fun linkGroupSetParticipant(
        userId: String,
        groupId: String,
        userParticipantId: String?,
    ): Flow<ProblemOr<Unit>> = callbackFlow {
        try {
            db.runTransaction { transaction ->
                val group = transaction.get(
                    db.collection(FireStoreCollections.GROUPS)
                        .document(groupId)
                )
                if (group.id.endsWith(groupId)
                    && group.data?.get(GroupEntityData::defaultCurrency.name) in CurrencyCode.values().map { it.name }
                ) {
                    transaction.set(
                        db.collection(FireStoreCollections.USERS)
                            .document(userId)
                            .collection(FireStoreCollections.UserSubCollections.GROUPS)
                            .document(groupId),
                        UsersGroupEntityData(
                            userParticipantId = userParticipantId,
                            groupId = groupId,
                        )
                    )
                } else {
                    throw FirebaseFirestoreException("Group not found", FirebaseFirestoreException.Code.NOT_FOUND)
                }
            }
                .addOnSuccessListener {
                    trySend(Unit.right())
                }
                .addOnFailureListener { e ->
                    sendProblemAndLogWarn("Error creating linkGroupSetParticipant user [$userId] group [$groupId] participant [$userParticipantId]",
                        e)
                }
        } catch (e: Exception) {
            sendProblemAndLogWarn("Error linkGroupSetParticipant user [$userId] group [$groupId]", e)
        }
        awaitClose { }
    }

    override fun unlinkUserFromGroup(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<Unit>> = callbackFlow {
        db.collection(FireStoreCollections.USERS)
            .document(userId)
            .collection(FireStoreCollections.UserSubCollections.GROUPS)
            .document(groupId)
            .delete()
            .addOnSuccessListener {
                trySend(Unit.right())
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error removing user [$userId] from group [$groupId]", e)
            }
        awaitClose { }
    }

    override fun updateUserInfo(userId: String, userInfo: UserEntityUpdateData): Flow<ProblemOr<Unit>> = callbackFlow {
        db.collection(FireStoreCollections.USERS)
            .document(userId)
            .set(userInfo, SetOptions.merge())
            .addOnSuccessListener {
                trySend(Unit.right())
            }
            .addOnFailureListener { e ->
                sendProblemAndLogWarn("Error updateUserInfo user [$userId] info [$userInfo]", e)
            }
        awaitClose { }
    }
}
