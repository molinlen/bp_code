package cz.cvut.fit.molinlen.android.debttracker.feature.transactiondetail

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.Comment
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors

@Composable
fun TransactionMenuItems(
    onEditClick: () -> Unit,
    onDeleteClick: () -> Unit,
) {
//    val commentActionLabel = if (hasUsersComment) "Edit comment" else "Add comment"
//    MenuItem(label = commentActionLabel, imageVector = Icons.Default.MailOutline) { //should be comment icon
//        onCommentClick()
//    }
    MenuItem(
        label = stringResource(R.string.edit),
        imageVector = Icons.Default.Edit,
        onClick = onEditClick,
    )
    MenuItem(
        label = stringResource(R.string.delete),
        imageVector = Icons.Default.Delete,
        contentColorBase = AccentColors.redContent,
        onClick = onDeleteClick,
    )
}

@Composable
fun TransactionDeleteDialog(visible: MutableState<Boolean>, onDeleteConfirm: () -> Unit) {
    DestructiveActionConfirmationDialog(
        visible = visible,
        questionText = stringResource(R.string.transaction_deletion_dialog_question),
        confirmButtonText = stringResource(R.string.yes_delete),
        onConfirmClick = onDeleteConfirm,
    )
}

@Composable
fun OptionalCommentsColumn(
    comments: List<Comment>,
    userId: String?,
) {
    if (comments.isNotEmpty()) {
        Text(text = stringResource(R.string.comments_section), style = MaterialTheme.typography.h6)
        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 10.dp, vertical = 5.dp)
        ) {
            comments.map { comment ->
                Card(
                    modifier = Modifier.padding(vertical = 5.dp),
                    backgroundColor = when (comment.writerUserId) {
                        userId -> MaterialTheme.colors.secondary
                        else -> AccentColors.surfaceAccentMedium
                    },
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 10.dp, vertical = 10.dp),
                    ) {
                        //todo readable user identification
                        Text(
                            text = stringResource(R.string.by_label) + " " + comment.writerUserId,
                            fontStyle = FontStyle.Italic,
                        )
                        Text(text = comment.content)
                        Text(
                            text = comment.time.toPrettyStringDate()
                                    + if (comment.edited) stringResource(R.string.edited_slug) else "",
                            style = MaterialTheme.typography.caption,
                        )
                    }
                }
            }
        }
    }
}
