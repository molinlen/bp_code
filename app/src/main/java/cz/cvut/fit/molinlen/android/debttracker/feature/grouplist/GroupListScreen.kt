package cz.cvut.fit.molinlen.android.debttracker.feature.grouplist

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cz.cvut.fit.molinlen.android.debttracker.R
import cz.cvut.fit.molinlen.android.debttracker.data.model.GlobalOwedSumsOverview
import cz.cvut.fit.molinlen.android.debttracker.data.model.GroupPreview
import cz.cvut.fit.molinlen.android.debttracker.data.model.Problem
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupPreview
import cz.cvut.fit.molinlen.android.debttracker.ui.components.*
import cz.cvut.fit.molinlen.android.debttracker.ui.theme.AccentColors
import java.math.BigDecimal

@Composable
fun GroupListScreen(
    navigateAfterLogout: () -> Unit,
    navigateAfterAccountClick: () -> Unit,
    navigateAfterGroupClick: (String) -> Unit,
    navigateAfterCreateGroupClick: () -> Unit,
) {
    val viewModel: GroupListViewModel = viewModel()
    LaunchedEffect(Unit) {
        viewModel.loadData()
    }

    GroupListScreenImpl(
        canJoinSuchGroup = { viewModel.canJoinSpecifiedGroup(it) },
        onJoinGroupClick = { viewModel.joinGroup(it, navigateAfterGroupClick) },
        clearGroupJoinError = { viewModel.clearGroupJoinError() },
        onLogoutClick = { viewModel.logout { navigateAfterLogout() } },
        navigateAfterAccountClick = navigateAfterAccountClick,
        navigateAfterGroupClick = navigateAfterGroupClick,
        navigateAfterCreateGroupClick = navigateAfterCreateGroupClick,
        viewState = viewModel.viewState.collectAsState().value,
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun GroupListScreenImpl(
    canJoinSuchGroup: (inputGroupId: String) -> Boolean,
    onJoinGroupClick: (inputGroupId: String) -> Unit,
    clearGroupJoinError: () -> Unit,
    onLogoutClick: () -> Unit,
    navigateAfterAccountClick: () -> Unit,
    navigateAfterGroupClick: (String) -> Unit,
    navigateAfterCreateGroupClick: () -> Unit,
    viewState: GroupListViewState,
) {
    val menuScaffoldState = rememberBottomSheetMenuState()
    val joinGroupExpanded = remember { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(menuScaffoldState.currentValue) {
        if (menuScaffoldState.isVisible.not() && joinGroupExpanded.value) {
            clearGroupJoinError()
            joinGroupExpanded.value = false
        }
    }

    BottomSheetMenu(
        sheetState = menuScaffoldState,
        coroutineScope = coroutineScope,
        menuContent = {
            GroupListMenu(
                joinGroupExpanded = joinGroupExpanded,
                lastJoinGroupError = viewState.groupJoinError,
                canJoinSuchGroup = canJoinSuchGroup,
                onJoinGroupClick = onJoinGroupClick,
                onLogout = onLogoutClick,
                onAccountClick = navigateAfterAccountClick,
                onCreateGroupClick = navigateAfterCreateGroupClick,
            )
        }
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            topBar = {
                GroupListScreenHeader(onMenuClick = { toggleMenuState(coroutineScope, menuScaffoldState) })
            },
        ) { paddingValues ->
            if (viewState.processing) {
                LoadingOverlay()
            }
            when {
                viewState.loading -> LoadingScreen()
                viewState.error != null -> ErrorScreen(error = viewState.error)
                else -> GroupListScreenContent(
                    groups = viewState.groups,
                    overview = viewState.overview,
                    paddingValues = paddingValues,
                    userHasPrimaryCurrency = viewState.userHasPrimaryCurrency,
                    onGroupClick = navigateAfterGroupClick,
                )
            }
        }
    }
}

@Composable
private fun GroupListMenu(
    joinGroupExpanded: MutableState<Boolean>,
    lastJoinGroupError: Problem?,
    canJoinSuchGroup: (inputGroupId: String) -> Boolean,
    onJoinGroupClick: (inputGroupId: String) -> Unit,
    onLogout: () -> Unit,
    onAccountClick: () -> Unit,
    onCreateGroupClick: () -> Unit,
) {
    if (joinGroupExpanded.value) {
        val groupIdToJoin = remember { mutableStateOf("") }
        BottomSheetMenuContentColumn(
            modifier = Modifier
                .padding(horizontal = screenContentPadding)
                .clearFocusOnTap()
        ) {
            BasicTextField(
                label = stringResource(R.string.group_id),
                value = groupIdToJoin.value,
                onValueChange = { groupIdToJoin.value = it },
                modifier = Modifier.padding(vertical = 10.dp),
                keyboardActions = defaultKeyboardActions(onConfirmAlso = {
                    if (canJoinSuchGroup(groupIdToJoin.value)) {
                        onJoinGroupClick(groupIdToJoin.value)
                    }
                })
            )
            lastJoinGroupError?.let {
                ErrorCard(
                    text = buildErrorMessage(
                        errorMessage = stringResource(R.string.try_again_check_group_id_prompt),
                        errorDetails = it.description,
                        errorCause = it.cause,
                    ),
                )
            }
            BasicRectangleTextButton(
                text = stringResource(R.string.join),
                onClick = { onJoinGroupClick(groupIdToJoin.value) },
                enabled = canJoinSuchGroup(groupIdToJoin.value),
            )
        }
    } else {
        BottomSheetMenuContentColumn {
            MenuItem(label = stringResource(R.string.account), imageVector = Icons.Default.AccountCircle) {
                onAccountClick()
            }
            MenuItem(label = stringResource(R.string.log_out), imageVector = Icons.Default.ExitToApp) {
                onLogout()
            }
            MenuItem(label = stringResource(R.string.join_a_group), imageVector = Icons.Default.AddCircle) {
                joinGroupExpanded.value = true
            }
            MenuItem(label = stringResource(R.string.create_a_group), imageVector = Icons.Default.Add) {
                onCreateGroupClick()
            }
        }
    }
}

@Composable
private fun GroupListScreenContent(
    groups: List<UsersGroupPreview>?,
    overview: GlobalOwedSumsOverview?,
    paddingValues: PaddingValues,
    userHasPrimaryCurrency: Boolean?,
    onGroupClick: (String) -> Unit,
) {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(120.dp),
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
            .padding(horizontal = 10.dp),
    ) {
        item(span = { GridItemSpan(maxLineSpan) }) {
            GroupListSectionHeader(text = overview?.let { stringResource(R.string.your_status) } ?: "")
        }
        item(span = { GridItemSpan(maxLineSpan) }) {
            SumsCard(
                overview = overview,
                showUserCurrencyPrompt = userHasPrimaryCurrency == false && overview == null && groups?.isNotEmpty() == true,
            )
        }
        groups
            ?.let { usersGroups ->
                item(span = { GridItemSpan(maxLineSpan) }) {
                    GroupListSectionHeader(text = if (usersGroups.isNotEmpty()) stringResource(R.string.your_groups) else "")
                }
                items(usersGroups) { usersGroup ->
                    GroupCard(
                        group = usersGroup.group,
                        onClick = { groupId -> onGroupClick(groupId) },
                    )
                }
                if (usersGroups.isEmpty()) {
                    item(span = { GridItemSpan(maxLineSpan) }) { NoGroupsPromptCard() }
                }
            }
    }
}

@Composable
private fun SumsCard(
    overview: GlobalOwedSumsOverview?,
    showUserCurrencyPrompt: Boolean,
) {
    Box(
        contentAlignment = Alignment.TopCenter,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            overview
                ?.let { overview ->
                    OverviewSumsTexts(overview)
                }
                ?: OverviewTextColumn( // so that content does not "jump" on overview load
                    text1 = "", text2 = "", text3 = "", modifier = Modifier.fillMaxWidth()
                )
        }
        if (showUserCurrencyPrompt) { // overview won't load because of undefined currency -> prompt its definition
            Text(
                text = stringResource(R.string.no_user_primary_currency_overview_prompt),
                fontStyle = FontStyle.Italic,
                textAlign = TextAlign.Center,
            )
        }
    }
}

@Composable
private fun OverviewSumsTexts(
    overview: GlobalOwedSumsOverview,
) {
    val text3Color = when {
        overview.balance.value > BigDecimal.ZERO -> AccentColors.greenContent
        overview.balance.value < BigDecimal.ZERO -> AccentColors.redContent
        else -> MaterialTheme.colors.onBackground
    }
    OverviewTextColumn(
        text1 = stringResource(R.string.overview_collectable),
        text2 = stringResource(R.string.overview_owed),
        text3 = stringResource(R.string.overview_balance),
        modifier = Modifier.fillMaxWidth(2 / 5f)
    )
    OverviewTextColumn(
        text1 = overview.sumCollectable.mapAbsToString(),
        text2 = overview.sumOwed.mapAbsToString(),
        text3 = overview.balance.mapToStringWithSign(),
        text3Color = text3Color,
        horizontalAlignment = Alignment.End,
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
private fun OverviewTextColumn(
    text1: String,
    text2: String,
    text3: String,
    text3Color: Color = MaterialTheme.colors.onBackground,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    modifier: Modifier,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = horizontalAlignment,
    ) {
        Text(
            text = text1,
            style = MaterialTheme.typography.h6,
            fontWeight = FontWeight.Normal,
        )
        Text(
            text = text2,
            style = MaterialTheme.typography.h6,
            fontWeight = FontWeight.Normal,
        )
        Text(
            text = text3,
            style = MaterialTheme.typography.h6,
            color = text3Color,
        )
    }
}

@Composable
private fun GroupCard(
    group: GroupPreview,
    onClick: (groupId: String) -> Unit,
) {
    Card(
        modifier = Modifier
            .padding(3.dp),
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .height(120.dp)
                .background(MaterialTheme.colors.primary)
                .clickable { onClick(group.groupId) }
                .padding(10.dp),
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = group.name,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.h6,
                    fontWeight = FontWeight.Normal,
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis,
                )
                Text(
                    text = group.currentOwnStatus.mapToStringWithSign(),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.caption,
                )
            }
        }
    }
}

@Composable
private fun NoGroupsPromptCard() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 100.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = stringResource(R.string.no_user_groups_overview_prompt),
            fontStyle = FontStyle.Italic,
            textAlign = TextAlign.Center,
        )
    }
}

@Composable
private fun GroupListSectionHeader(
    text: String,
) {
    Text(text = text, style = MaterialTheme.typography.h5, modifier = Modifier.padding(top = 8.dp, bottom = 3.dp))
}

@Composable
private fun GroupListScreenHeader(
    onMenuClick: () -> Unit,
) {
    ScreenHeader(
        headingText = stringResource(R.string.overview),
        rightIcons = listOf {
            HeaderMenuIcon(onClick = onMenuClick)
        },
    )
}
