package cz.cvut.fit.molinlen.android.debttracker.data.usecase

import cz.cvut.fit.molinlen.android.debttracker.data.model.ProblemOr
import cz.cvut.fit.molinlen.android.debttracker.data.model.UsersGroupParticipants
import cz.cvut.fit.molinlen.android.debttracker.data.repository.GroupRepository
import cz.cvut.fit.molinlen.android.debttracker.data.repository.fake.GroupRepositoryFake
import cz.cvut.fit.molinlen.android.debttracker.data.repository.impl.GroupRepositoryImpl
import kotlinx.coroutines.flow.Flow

interface GetGroupParticipantsUseCase {
    suspend fun getUsersGroupParticipants(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupParticipants>>
}

class GetGroupParticipantsUseCaseImpl(
    private val groupRepository: GroupRepository = if (USE_FAKE_DATA) GroupRepositoryFake() else GroupRepositoryImpl(),
) : GetGroupParticipantsUseCase {
    override suspend fun getUsersGroupParticipants(
        userId: String,
        groupId: String,
    ): Flow<ProblemOr<UsersGroupParticipants>> {
        return groupRepository.getUsersGroupParticipants(userId = userId, groupId = groupId)
    }
}
