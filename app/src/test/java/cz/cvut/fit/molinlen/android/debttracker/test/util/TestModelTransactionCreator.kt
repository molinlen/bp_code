package cz.cvut.fit.molinlen.android.debttracker.test.util

import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.OffsetDateTime
import java.util.UUID

object TestModelTransactionCreator {

    fun randomId(): String = UUID.randomUUID().toString()

    fun createTransfer(
        transactionId: String = randomId(),
        from: Participant = ParticipantBasic(participantId = randomId(), nickname = "FPart"),
        to: Participant = ParticipantBasic(participantId = randomId(), nickname = "TPart"),
        amount: Amount = createAmount(BigDecimal.valueOf(217.3), CurrencyCode.CZK),
        time: OffsetDateTime = OffsetDateTime.now(),
        note: String? = null,
    ): TransferDetail {
        return TransferDetail(
            transactionId = transactionId,
            from = from,
            to = to,
            amount = amount,
            time = time,
            note = note,
            comments = emptyList(),
        )
    }

    fun createSplitting(
        splittingType: SplittingType = SplittingType.EXPLICIT,
        totalAmount: BigDecimal = BigDecimal.valueOf(1267.37),
        participants: List<Participant> = listOf(
            ParticipantBasic(participantId = randomId(), nickname = "TPart1"),
            ParticipantBasic(participantId = randomId(), nickname = "TPart2"),
            ParticipantBasic(participantId = randomId(), nickname = "TPart3"),
        ),
    ): TransactionSplittingStrategy {
        return when (splittingType) {
            SplittingType.EXPLICIT -> {
                val eachAmount = totalAmount.divide(maxOf(1, participants.size).toBigDecimal(), RoundingMode.DOWN)
                TransactionSplittingExplicit(
                    sharers = participants.map { TransactionShareExplicit(participant = it, shareValue = eachAmount) }
                )
            }
            SplittingType.PORTION -> TransactionSplittingPortion(
                sharers = participants.map { TransactionSharePortion(participant = it, shareValue = 7) }
            )
        }
    }

    fun createPayer(
        participant: Participant = createParticipant(nickname = "Payer"),
        amount: Amount = createAmount(BigDecimal.valueOf(217.37), CurrencyCode.CZK),
    ): Payer {
        return Payer(participant = participant, amount = amount)
    }

    fun createPurchase(
        transactionId: String = randomId(),
        payers: List<Payer> = listOf(
            createPayer(amount = createAmount(BigDecimal.valueOf(500L), CurrencyCode.CZK)),
            createPayer(amount = createAmount(BigDecimal.valueOf(271.77), CurrencyCode.CZK)),
        ),
        splitting: TransactionSplittingStrategy = createSplitting(totalAmount = payers.sumOf { it.amount.value }),
        time: OffsetDateTime = OffsetDateTime.now(),
        name: String? = null,
        note: String? = null,
    ): PurchaseDetail {
        return PurchaseDetail(
            transactionId = transactionId,
            payers = payers,
            splitting = splitting,
            comments = emptyList(),
            time = time,
            name = name,
            note = note,
        )
    }

    fun createParticipant(
        participantId: String = randomId(),
        nickname: String = "AParticipant",
    ): ParticipantBasic {
        return ParticipantBasic(participantId = participantId, nickname = nickname)
    }

    fun createGroup(
        groupId: String = randomId(),
        name: String = "A Group",
        defaultCurrency: CurrencyCode = CurrencyCode.CZK,
        transactions: List<Transaction> = emptyList(),
        participants: Set<ParticipantWithStatus> = emptySet(),
    ): GroupDetail {
        return GroupDetail(
            groupId = groupId,
            name = name,
            defaultCurrency = defaultCurrency,
            transactions = transactions,
            participants = participants,
        )
    }

    fun createGroupBasic(
        groupId: String = randomId(),
        name: String = "A Group",
        defaultCurrency: CurrencyCode = CurrencyCode.CZK,
        transactions: List<Transaction> = emptyList(),
        participants: Set<Participant> = emptySet(),
    ): GroupBasic {
        return GroupBasic(
            groupId = groupId,
            name = name,
            defaultCurrency = defaultCurrency,
            transactions = transactions,
            participants = participants,
        )
    }

    fun createUsersGroupPreview(
        userParticipantId: String? = null,
        groupId: String = randomId(),
        name: String = "A Group",
        defaultCurrency: CurrencyCode = CurrencyCode.CZK,
        ownStatus: Amount = amountZero(defaultCurrency),
    ): UsersGroupPreview {
        return UsersGroupPreview(
            userParticipantId = userParticipantId,
            group = GroupPreview(
                groupId = groupId,
                name = name,
                defaultCurrency = defaultCurrency,
                currentOwnStatus = ownStatus,
            )
        )
    }

    fun createUsersGroupBasic(
        userParticipantId: String? = null,
        group: GroupBasic = createGroupBasic(),
    ): UsersGroupBasic {
        return UsersGroupBasic(userParticipantId = userParticipantId, group = group)
    }
}
