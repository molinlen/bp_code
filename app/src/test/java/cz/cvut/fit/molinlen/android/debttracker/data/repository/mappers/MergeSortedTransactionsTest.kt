package cz.cvut.fit.molinlen.android.debttracker.data.repository.mappers

import cz.cvut.fit.molinlen.android.debttracker.data.model.Purchase
import cz.cvut.fit.molinlen.android.debttracker.data.model.Transaction
import cz.cvut.fit.molinlen.android.debttracker.data.model.Transfer
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createPurchase
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createTransfer
import org.junit.Assert.*
import org.junit.Test
import java.time.OffsetDateTime

internal class MergeSortedTransactionsTest {

    @Test
    fun noTransactions() {
        assertTrue(mergeSortedTransactions(emptyList(), emptyList()).isEmpty())
    }

    @Test
    fun onlyFewTransactions() {
        val sample1 = createTransfer()
        assertEquals(listOf(sample1), mergeSortedTransactions(listOf(sample1), emptyList()))
        assertEquals(listOf(sample1), mergeSortedTransactions(emptyList(), listOf(sample1)))

        val sample2 = createPurchase(time = sample1.time.minusMinutes(1))
        assertEquals(listOf(sample2), mergeSortedTransactions(listOf(sample2), emptyList()))
        assertEquals(listOf(sample2), mergeSortedTransactions(emptyList(), listOf(sample2)))

        assertEquals(listOf(sample1, sample2), mergeSortedTransactions(listOf(sample1), listOf(sample2)))
        assertEquals(listOf(sample1, sample2), mergeSortedTransactions(listOf(sample2), listOf(sample1)))
        assertEquals(listOf(sample1, sample2), mergeSortedTransactions(listOf(sample1, sample2), emptyList()))
        assertEquals(listOf(sample1, sample2), mergeSortedTransactions(emptyList(), listOf(sample1, sample2)))
    }

    @Test
    fun standardMerge1() {
        val baseTime = OffsetDateTime.now()
        val transactionsSorted = listOf(
            createPurchase(time = baseTime.minusMinutes(1)),
            createTransfer(time = baseTime.minusMinutes(2)),
            createPurchase(time = baseTime.minusMinutes(3)),
            createTransfer(time = baseTime.minusMinutes(4)),
            createPurchase(time = baseTime.minusMinutes(5)),
            createTransfer(time = baseTime.minusMinutes(6)),
        )
        testForSortedList(transactionsSorted)
    }

    @Test
    fun standardMerge2() {
        val baseTime = OffsetDateTime.now()
        val transactionsSorted = listOf(
            createPurchase(time = baseTime.minusHours(1)),
            createTransfer(time = baseTime.minusHours(2)),
            createTransfer(time = baseTime.minusHours(3)),
            createTransfer(time = baseTime.minusHours(4)),
            createTransfer(time = baseTime.minusHours(5)),
            createPurchase(time = baseTime.minusHours(6)),
        )
        testForSortedList(transactionsSorted)
    }

    @Test
    fun standardMerge3() {
        val baseTime = OffsetDateTime.now()
        val transactionsSorted = listOf(
            createTransfer(time = baseTime.minusHours(1)),
            createPurchase(time = baseTime.minusHours(2)),
            createPurchase(time = baseTime.minusHours(3)),
            createPurchase(time = baseTime.minusHours(4)),
            createPurchase(time = baseTime.minusHours(5)),
            createTransfer(time = baseTime.minusHours(6)),
        )
        testForSortedList(transactionsSorted)
    }

    @Test
    fun standardMerge4() {
        val baseTime = OffsetDateTime.now()
        val transactionsSorted = listOf(
            createPurchase(time = baseTime.minusMinutes(1)),
            createTransfer(time = baseTime.minusMinutes(2)),
            createTransfer(time = baseTime.minusMinutes(3)),
            createPurchase(time = baseTime.minusMinutes(4)),
            createPurchase(time = baseTime.minusMinutes(5)),
            createTransfer(time = baseTime.minusMinutes(6)),
            createPurchase(time = baseTime.minusMinutes(8)),
            createTransfer(time = baseTime.minusMinutes(9)),
            createPurchase(time = baseTime.minusMinutes(10)),
            createPurchase(time = baseTime.minusMinutes(11)),
            createTransfer(time = baseTime.minusMinutes(12)),
            createPurchase(time = baseTime.minusMinutes(13)),
            createTransfer(time = baseTime.minusMinutes(14)),
        )
        testForSortedList(transactionsSorted)
    }

    private fun testForSortedList(transactionsSorted: List<Transaction>) {
        val listA = transactionsSorted.filterIsInstance<Transfer>()
        val listB = transactionsSorted.filterIsInstance<Purchase>()
        assertEquals(transactionsSorted, mergeSortedTransactions(listA, listB))
        assertEquals(transactionsSorted, mergeSortedTransactions(listB, listA))
    }
}
