package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertor
import cz.cvut.fit.molinlen.android.debttracker.data.model.*
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode.*
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createGroupBasic
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createParticipant
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createPayer
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createPurchase
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createTransfer
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createUsersGroupBasic
import cz.cvut.fit.molinlen.android.debttracker.test.util.assertRightAnd
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

internal class GroupStatusCalculatorImplTest {
    private fun mockCurrencyConversion(value: BigDecimal, from: CurrencyCode, to: CurrencyCode): BigDecimal {
        val result = when {
            from == to -> value
            from == CZK && to == USD -> value.times(0.047.toBigDecimal()).scaleFor(to)
            from == CZK && to == EUR -> value.times(0.043.toBigDecimal()).scaleFor(to)
            from == EUR && to == CZK -> value.times(23.44.toBigDecimal()).scaleFor(to)
            from == EUR && to == USD -> value.times(1.10.toBigDecimal()).scaleFor(to)
            from == USD && to == CZK -> value.times(21.33.toBigDecimal()).scaleFor(to)
            from == USD && to == EUR -> value.times(0.91.toBigDecimal()).scaleFor(to)
            else -> null
        }
        return result!!
    }

    private val currencyConvertorMock: CurrencyConvertor = mockk()

    private val impl = GroupStatusCalculatorImpl(
        currencyConvertor = currencyConvertorMock,
    )

    private val aParticipant = createParticipant(nickname = "A Nickname")
    private val bParticipant = createParticipant(nickname = "B Nickname")
    private val cParticipant = createParticipant(nickname = "C Nickname")
    private val dParticipant = createParticipant(nickname = "D Nickname")
    private val eParticipant = createParticipant(nickname = "E Nickname")
    private val participants = setOf(aParticipant, bParticipant, cParticipant, dParticipant, eParticipant)

    private val groupCurrency = CZK

    private val groupBase = createGroupBasic(
        defaultCurrency = groupCurrency,
        participants = participants
    )

    @Before
    fun setup() {
        every { currencyConvertorMock.convert(any(), any(), any()) } answers { call ->
            mockCurrencyConversion(
                value = call.invocation.args[0]!! as BigDecimal,
                from = call.invocation.args[1]!! as CurrencyCode,
                to = call.invocation.args[2]!! as CurrencyCode,
            )
        }
    }

    @Test
    fun groupStatus_oneTransferSameCurrency() {
        val groupWTransfer = groupBase.copy(
            transactions = listOf(createTransfer(
                from = aParticipant,
                to = bParticipant,
                amount = createAmount(499.99.toBigDecimal(), groupCurrency)
            ))
        )
        impl.computeGroupStatus(createUsersGroupBasic(group = groupWTransfer)).assertRightAnd { result ->
            assertEquals(groupBase, result.group.toBasic(transactions = emptyList()))
            assertEquals(null, result.userParticipantId)
            assertNotNull(
                result.group.participants.singleOrNull {
                    it == aParticipant.withStatus(createAmount(499.99.toBigDecimal(), groupCurrency))
                }
            )
            assertNotNull(
                result.group.participants.singleOrNull {
                    it == bParticipant.withStatus(createAmount((-499.99).toBigDecimal(), groupCurrency))
                }
            )
            result.group.participants
                .filter { it.participantId !in setOf(aParticipant.participantId, bParticipant.participantId) }
                .forEach { assertEquals(amountZero(groupCurrency), it.currentStatus) }
        }
    }

    @Test
    fun groupStatus_onePurchaseExplicitSameCurrency() {
        val groupWPurchaseExplicit = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(
                            participant = aParticipant, amount = createAmount(500.00.toBigDecimal(), groupCurrency)),
                        createPayer(
                            participant = bParticipant, amount = createAmount(200.00.toBigDecimal(), groupCurrency)),
                    ),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(participant = bParticipant, shareValue = 300.00.toBigDecimal()),
                            TransactionShareExplicit(participant = cParticipant, shareValue = 200.01.toBigDecimal()),
                            TransactionShareExplicit(participant = dParticipant, shareValue = 199.99.toBigDecimal()),
                        ),
                    )
                )
            )
        )
        impl.computeGroupStatus(createUsersGroupBasic(group = groupWPurchaseExplicit)).assertRightAnd { result ->
            assertEquals(groupBase, result.group.toBasic(transactions = emptyList()))
            assertEquals(null, result.userParticipantId)
            result.group.participants.let { participants ->
                assertNotNull(
                    participants.singleOrNull {
                        it == aParticipant.withStatus(createAmount(500.00.toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == bParticipant.withStatus(createAmount((-100.00).toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == cParticipant.withStatus(createAmount((-200.01).toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == dParticipant.withStatus(createAmount((-199.99).toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == eParticipant.withStatus(amountZero(groupCurrency))
                    }
                )
            }
        }
    }

    @Test
    fun groupStatus_onePurchasePortionSameCurrency() {
        val groupCurrency = EUR
        val groupWPurchaseExplicit = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(
                            participant = aParticipant, amount = createAmount(99.99.toBigDecimal(), groupCurrency)),
                        createPayer(
                            participant = bParticipant, amount = createAmount(900.01.toBigDecimal(), groupCurrency)),
                    ),
                    splitting = TransactionSplittingPortion(
                        sharers = listOf(
                            TransactionSharePortion(bParticipant, 250),
                            TransactionSharePortion(cParticipant, 250),
                            TransactionSharePortion(dParticipant, 1000),
                        ),
                    ),
                )
            ),
            defaultCurrency = groupCurrency,
        )
        impl.computeGroupStatus(
            createUsersGroupBasic(group = groupWPurchaseExplicit, userParticipantId = aParticipant.participantId),
        ).assertRightAnd { result ->
            assertEquals(groupBase.copy(defaultCurrency = EUR), result.group.toBasic(transactions = emptyList()))
            assertEquals(aParticipant.participantId, result.userParticipantId)
            result.group.participants.let { participants ->
                assertNotNull(
                    participants.singleOrNull {
                        it == aParticipant.withStatus(createAmount(99.99.toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == bParticipant.withStatus(createAmount(733.35.toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == cParticipant.withStatus(createAmount((-166.66).toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == dParticipant.withStatus(createAmount((-666.66).toBigDecimal(), groupCurrency))
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == eParticipant.withStatus(amountZero(groupCurrency))
                    }
                )
            }
        }
    }

    @Test
    fun groupStatus_oneTransferDifferentCurrency() {
        val transactionCurrency = USD
        val amount = createAmount(94.13.toBigDecimal(), transactionCurrency)
        val groupWTransfer = groupBase.copy(
            transactions = listOf(createTransfer(
                from = aParticipant,
                to = bParticipant,
                amount = amount
            ))
        )
        impl.computeGroupStatus(createUsersGroupBasic(group = groupWTransfer)).assertRightAnd { result ->
            assertEquals(groupBase, result.group.toBasic(transactions = emptyList()))
            assertEquals(null, result.userParticipantId)
            assertNotNull(
                result.group.participants.singleOrNull {
                    it == aParticipant.withStatus(
                        createAmount(
                            mockCurrencyConversion(amount.value, transactionCurrency, groupCurrency),
                            groupCurrency,
                        )
                    )
                }
            )
            assertNotNull(
                result.group.participants.singleOrNull {
                    it == bParticipant.withStatus(
                        createAmount(
                            mockCurrencyConversion(-amount.value, transactionCurrency, groupCurrency),
                            groupCurrency,
                        )
                    )
                }
            )
            result.group.participants
                .filter { it.participantId !in setOf(aParticipant.participantId, bParticipant.participantId) }
                .forEach { assertEquals(amountZero(groupCurrency), it.currentStatus) }
        }
    }

    @Test
    fun groupStatus_onePurchaseExplicitDifferentCurrency() {
        val transactionCurrency = EUR
        val fullAmount = createAmount(537.81.toBigDecimal(), transactionCurrency)
        val groupWPurchaseExplicit = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(participant = aParticipant, amount = fullAmount),
                    ),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(participant = bParticipant, shareValue = 300.00.toBigDecimal()),
                            TransactionShareExplicit(participant = cParticipant, shareValue = 200.01.toBigDecimal()),
                            TransactionShareExplicit(participant = dParticipant, shareValue = 37.80.toBigDecimal()),
                        ),
                    )
                )
            )
        )
        impl.computeGroupStatus(
            createUsersGroupBasic(group = groupWPurchaseExplicit, userParticipantId = cParticipant.participantId),
        ).assertRightAnd { result ->
            assertEquals(groupBase, result.group.toBasic(transactions = emptyList()))
            assertEquals(cParticipant.participantId, result.userParticipantId)
            result.group.participants.let { participants ->
                assertNotNull(
                    participants.singleOrNull {
                        it == aParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion(fullAmount.value, transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == bParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-300.00).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == cParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-200.01).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == dParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-37.80).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == eParticipant.withStatus(amountZero(groupCurrency))
                    }
                )
            }
        }
    }

    @Test
    fun groupStatus_onePurchasePortionDifferentCurrency() {
        val transactionCurrency = USD
        val fullAmount = createAmount(437.81.toBigDecimal(), transactionCurrency)
        val groupWPurchasePortion = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(participant = aParticipant, amount = fullAmount),
                    ),
                    splitting = TransactionSplittingPortion(
                        sharers = listOf(
                            TransactionSharePortion(bParticipant, 250),
                            TransactionSharePortion(cParticipant, 250),
                            TransactionSharePortion(dParticipant, 1000),
                        ),
                    ),
                )
            )
        )
        impl.computeGroupStatus(createUsersGroupBasic(group = groupWPurchasePortion)).assertRightAnd { result ->
            assertEquals(groupBase, result.group.toBasic(transactions = emptyList()))
            assertEquals(null, result.userParticipantId)
            result.group.participants.let { participants ->
                assertNotNull(
                    participants.singleOrNull {
                        it == aParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion(fullAmount.value, transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == bParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == cParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == dParticipant.withStatus(
                            createAmount(
                                mockCurrencyConversion((-291.87).toBigDecimal(), transactionCurrency, groupCurrency),
                                groupCurrency,
                            )
                        )
                    }
                )
                assertNotNull(
                    participants.singleOrNull {
                        it == eParticipant.withStatus(amountZero(groupCurrency))
                    }
                )
            }
        }
    }

    @Test
    fun groupPreview_onePurchaseExplicitSameCurrency() {
        val groupWPurchaseExplicit = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(
                            participant = aParticipant, amount = createAmount(500.00.toBigDecimal(), groupCurrency)),
                        createPayer(
                            participant = bParticipant, amount = createAmount(200.00.toBigDecimal(), groupCurrency)),
                    ),
                    splitting = TransactionSplittingExplicit(
                        sharers = listOf(
                            TransactionShareExplicit(participant = bParticipant, shareValue = 300.00.toBigDecimal()),
                            TransactionShareExplicit(participant = cParticipant, shareValue = 200.01.toBigDecimal()),
                            TransactionShareExplicit(participant = dParticipant, shareValue = 199.99.toBigDecimal()),
                        ),
                    )
                )
            )
        )
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = null, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(null, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = aParticipant.participantId, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(createAmount(500.00.toBigDecimal(), groupCurrency)),
                result.group,
            )
            assertEquals(aParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = bParticipant.participantId, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(createAmount((-100.00).toBigDecimal(), groupCurrency)),
                result.group,
            )
            assertEquals(bParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = cParticipant.participantId, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(createAmount((-200.01).toBigDecimal(), groupCurrency)),
                result.group,
            )
            assertEquals(cParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = dParticipant.participantId, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(createAmount((-199.99).toBigDecimal(), groupCurrency)),
                result.group,
            )
            assertEquals(dParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = eParticipant.participantId, group = groupWPurchaseExplicit),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(eParticipant.participantId, result.userParticipantId)
        }
    }

    @Test
    fun groupPreview_oneTransferDifferentCurrency() {
        val transactionCurrency = USD
        val amount = createAmount(174.61.toBigDecimal(), transactionCurrency)
        val groupWTransfer = groupBase.copy(
            transactions = listOf(createTransfer(
                from = aParticipant,
                to = bParticipant,
                amount = amount
            ))
        )
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = null, group = groupWTransfer),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(null, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = aParticipant.participantId, group = groupWTransfer),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(amount.value, transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(aParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = bParticipant.participantId, group = groupWTransfer),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(-amount.value, transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(bParticipant.participantId, result.userParticipantId)
        }
        groupBase.participants
            .filter { it.participantId !in setOf(aParticipant.participantId, bParticipant.participantId) }
            .forEach {
                impl.computeGroupPreview(
                    createUsersGroupBasic(userParticipantId = it.participantId, group = groupWTransfer),
                ).assertRightAnd { result ->
                    assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
                    assertEquals(it.participantId, result.userParticipantId)
                }
            }
    }

    @Test
    fun groupPreview_onePurchaseExplicitDifferentCurrency() {
        val transactionCurrency = EUR
        val fullAmount = createAmount(437.81.toBigDecimal(), transactionCurrency)
        val groupWPurchasePortion = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(participant = aParticipant, amount = fullAmount),
                    ),
                    splitting = TransactionSplittingPortion(
                        sharers = listOf(
                            TransactionSharePortion(bParticipant, 250),
                            TransactionSharePortion(cParticipant, 250),
                            TransactionSharePortion(dParticipant, 1000),
                        ),
                    ),
                )
            )
        )
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = null, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(null, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = aParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(fullAmount.value, transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(aParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = bParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(bParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = cParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(cParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = dParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-291.87).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(dParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = eParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(eParticipant.participantId, result.userParticipantId)
        }
    }

    @Test
    fun groupPreview_onePurchasePortionDifferentCurrency() {
        val transactionCurrency = EUR
        val fullAmount = createAmount(437.81.toBigDecimal(), transactionCurrency)
        val groupWPurchasePortion = groupBase.copy(
            transactions = listOf(
                createPurchase(
                    payers = listOf(
                        createPayer(participant = aParticipant, amount = fullAmount),
                    ),
                    splitting = TransactionSplittingPortion(
                        sharers = listOf(
                            TransactionSharePortion(bParticipant, 250),
                            TransactionSharePortion(cParticipant, 250),
                            TransactionSharePortion(dParticipant, 1000),
                        ),
                    ),
                )
            )
        )
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = null, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(null, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = aParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(fullAmount.value, transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(aParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = bParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(bParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = cParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-72.96).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(cParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = dParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion((-291.87).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(dParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = eParticipant.participantId, group = groupWPurchasePortion),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(eParticipant.participantId, result.userParticipantId)
        }
    }

    @Test
    fun groupPreview_fewTransactionsDifferentCurrency() {
        val transactionCurrency = USD
        val groupWTransactions = groupBase.copy(
            transactions = listOf(
                createTransfer(
                    from = aParticipant,
                    to = bParticipant,
                    amount = createAmount(174.61.toBigDecimal(), transactionCurrency)
                ),
                createTransfer(
                    from = bParticipant,
                    to = aParticipant,
                    amount = createAmount(307.12.toBigDecimal(), transactionCurrency)
                ),
                createTransfer(
                    from = bParticipant,
                    to = aParticipant,
                    amount = createAmount(174.61.toBigDecimal(), transactionCurrency)
                ),
                createPurchase(
                    payers = listOf(
                        createPayer(
                            participant = aParticipant,
                            amount = createAmount(599.11.toBigDecimal(), transactionCurrency)),
                    ),
                    splitting = TransactionSplittingPortion(
                        sharers = listOf(
                            TransactionSharePortion(aParticipant, 1),
                            TransactionSharePortion(bParticipant, 10),
                        ),
                    ),
                )
            )
        )
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = null, group = groupWTransactions),
        ).assertRightAnd { result ->
            assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
            assertEquals(null, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = aParticipant.participantId, group = groupWTransactions),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(
                            599.11.toBigDecimal(), transactionCurrency, groupCurrency,
                        ) + mockCurrencyConversion(
                            (-307.12).toBigDecimal(), transactionCurrency, groupCurrency,
                        ) + mockCurrencyConversion((-54.46).toBigDecimal(), transactionCurrency, groupCurrency),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(aParticipant.participantId, result.userParticipantId)
        }
        impl.computeGroupPreview(
            createUsersGroupBasic(userParticipantId = bParticipant.participantId, group = groupWTransactions),
        ).assertRightAnd { result ->
            assertEquals(
                groupBase.toPreviewWithStatus(
                    createAmount(
                        mockCurrencyConversion(
                            307.12.toBigDecimal(), transactionCurrency, groupCurrency,
                        ) + mockCurrencyConversion(
                            -(599.11).toBigDecimal()
                                .times(10.toBigDecimal()).divide(11.toBigDecimal(), RoundingMode.DOWN),
                            transactionCurrency,
                            groupCurrency,
                        ),
                        groupCurrency,
                    )),
                result.group,
            )
            assertEquals(bParticipant.participantId, result.userParticipantId)
        }
        groupBase.participants
            .filter { it.participantId !in setOf(aParticipant.participantId, bParticipant.participantId) }
            .forEach {
                impl.computeGroupPreview(
                    createUsersGroupBasic(userParticipantId = it.participantId, group = groupWTransactions),
                ).assertRightAnd { result ->
                    assertEquals(groupBase.toPreviewWithStatus(amountZero(groupCurrency)), result.group)
                    assertEquals(it.participantId, result.userParticipantId)
                }
            }
    }

    private fun GroupDetail.toBasic(transactions: List<Transaction>): GroupBasic {
        return GroupBasic(
            groupId = groupId,
            name = name,
            defaultCurrency = defaultCurrency,
            transactions = transactions,
            participants = participants.map {
                ParticipantBasic(
                    participantId = it.participantId,
                    nickname = it.nickname)
            }.toSet()
        )
    }

    private fun ParticipantBasic.withStatus(status: Amount): ParticipantWithStatus {
        return ParticipantWithStatus(
            participantId = participantId,
            nickname = nickname,
            currentStatus = status,
        )
    }

    private fun Group.toPreviewWithStatus(status: Amount): GroupPreview {
        return GroupPreview(
            groupId = groupId,
            name = name,
            defaultCurrency = defaultCurrency,
            currentOwnStatus = status,
        )
    }
}
