package cz.cvut.fit.molinlen.android.debttracker.test.util

import arrow.core.Either
import org.junit.Assert.assertTrue

fun <IL, IR> Either<IL, IR>.assertRightAnd(forRight: (IR) -> Unit) {
    return this.fold(
        { assertTrue("Expected Either.Right, got Either.Left", false) },
        { right -> forRight(right) }
    )
}

fun <IL, IR> Either<IL, IR>.assertLeft() {
    return this.fold(
        {},
        { assertTrue("Expected Either.Left, got Either.Right", false) }
    )
}
