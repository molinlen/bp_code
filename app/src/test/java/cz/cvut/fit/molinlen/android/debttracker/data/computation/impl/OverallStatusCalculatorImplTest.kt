package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import cz.cvut.fit.molinlen.android.debttracker.data.CurrencyConvertor
import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.GlobalOwedSumsOverview
import cz.cvut.fit.molinlen.android.debttracker.data.model.amountZero
import cz.cvut.fit.molinlen.android.debttracker.data.model.createAmount
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createUsersGroupPreview
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.randomId
import cz.cvut.fit.molinlen.android.debttracker.test.util.assertRightAnd
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

internal class OverallStatusCalculatorImplTest {
    private val impl = OverallStatusCalculatorImpl()

    @Test
    fun noOwnTransactions() = runBlocking {
        val currency = CurrencyCode.CZK
        val expected = GlobalOwedSumsOverview(
            sumOwed = amountZero(currency = currency),
            sumCollectable = amountZero(currency = currency),
        )

        impl.calculateOverallUserStatusFromPreview(
            usersGroups = emptyList(),
            currency = currency,
        ).assertRightAnd { calculated ->
            assertEquals(expected, calculated)
        }
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(createUsersGroupPreview(userParticipantId = null)),
            currency = currency,
        ).assertRightAnd { calculated ->
            assertEquals(expected, calculated)
        }
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(
                createUsersGroupPreview(userParticipantId = null),
                createUsersGroupPreview(userParticipantId = null),
                createUsersGroupPreview(userParticipantId = null),
            ),
            currency = currency,
        ).assertRightAnd { calculated ->
            assertEquals(expected, calculated)
        }
    }

    @Test
    fun fewOwnTransactionsOneCurrency() = runBlocking {
        val currency1 = CurrencyCode.CZK
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(
                createUsersGroupPreview(
                    userParticipantId = randomId(), ownStatus = createAmount(213.11.toBigDecimal(), currency1)),
                createUsersGroupPreview(
                    userParticipantId = randomId(), ownStatus = createAmount((-1832.97).toBigDecimal(), currency1)),
                createUsersGroupPreview(userParticipantId = null),
            ),
            currency = currency1,
        ).assertRightAnd { calculated ->
            val expected = GlobalOwedSumsOverview(
                sumOwed = createAmount(1832.97.toBigDecimal(), currency1),
                sumCollectable = createAmount(213.11.toBigDecimal(), currency1),
            )
            assertEquals(expected, calculated)
        }
        val currency2 = CurrencyCode.EUR
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(
                createUsersGroupPreview(
                    userParticipantId = randomId(), ownStatus = createAmount((-2832.97).toBigDecimal(), currency2)),
                createUsersGroupPreview(
                    userParticipantId = randomId(), ownStatus = createAmount(1213.11.toBigDecimal(), currency2)),
            ),
            currency = currency2,
        ).assertRightAnd { calculated ->
            val expected = GlobalOwedSumsOverview(
                sumOwed = createAmount(2832.97.toBigDecimal(), currency2),
                sumCollectable = createAmount(1213.11.toBigDecimal(), currency2),
            )
            assertEquals(expected, calculated)
        }
    }

    @Test
    fun fewOwnTransactionsCurrencyConversion() = runBlocking {
        val currencyConvertorMock: CurrencyConvertor = mockk()
        val impl = OverallStatusCalculatorImpl(
            currencyConvertor = currencyConvertorMock,
        )

        val usersCurrency1 = CurrencyCode.CZK
        val aAmount1 = createAmount(50.toBigDecimal(), CurrencyCode.EUR)
        coEvery {
            currencyConvertorMock.convert(
                value = aAmount1.value,
                from = aAmount1.currency,
                to = usersCurrency1,
            )
        } returns 100.00.toBigDecimal()
        val bAmount1 = createAmount((-30).toBigDecimal(), CurrencyCode.USD)
        coEvery {
            currencyConvertorMock.convert(
                value = bAmount1.value,
                from = bAmount1.currency,
                to = usersCurrency1,
            )
        } returns (-90.00).toBigDecimal()
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = aAmount1),
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = bAmount1),
                createUsersGroupPreview(userParticipantId = null),
            ),
            currency = usersCurrency1,
        ).assertRightAnd { calculated ->
            val expected = GlobalOwedSumsOverview(
                sumOwed = createAmount(90.00.toBigDecimal(), usersCurrency1),
                sumCollectable = createAmount(100.00.toBigDecimal(), usersCurrency1),
            )
            assertEquals(expected, calculated)
        }


        val usersCurrency2 = CurrencyCode.USD
        val aAmount2 = createAmount(30.toBigDecimal(), CurrencyCode.EUR)
        coEvery {
            currencyConvertorMock.convert(
                value = aAmount2.value,
                from = aAmount2.currency,
                to = usersCurrency2,
            )
        } returns 35.00.toBigDecimal()
        val bAmount2 = createAmount((-2000).toBigDecimal(), CurrencyCode.CZK)
        coEvery {
            currencyConvertorMock.convert(
                value = bAmount2.value,
                from = bAmount2.currency,
                to = usersCurrency2,
            )
        } returns (-95.00).toBigDecimal()
        val cAmount2 = createAmount((-25).toBigDecimal(), CurrencyCode.EUR)
        coEvery {
            currencyConvertorMock.convert(
                value = cAmount2.value,
                from = cAmount2.currency,
                to = usersCurrency2,
            )
        } returns (-29.50).toBigDecimal()
        val dAmount2 = createAmount(300.toBigDecimal(), CurrencyCode.CZK)
        coEvery {
            currencyConvertorMock.convert(
                value = dAmount2.value,
                from = dAmount2.currency,
                to = usersCurrency2,
            )
        } returns 14.50.toBigDecimal()
        impl.calculateOverallUserStatusFromPreview(
            usersGroups = listOf(
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = aAmount2),
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = bAmount2),
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = cAmount2),
                createUsersGroupPreview(userParticipantId = randomId(), ownStatus = dAmount2),
            ),
            currency = usersCurrency2,
        ).assertRightAnd { calculated ->
            val expected = GlobalOwedSumsOverview(
                sumOwed = createAmount(124.50.toBigDecimal(), usersCurrency2),
                sumCollectable = createAmount(49.50.toBigDecimal(), usersCurrency2),
            )
            assertEquals(expected, calculated)
        }
    }
}
