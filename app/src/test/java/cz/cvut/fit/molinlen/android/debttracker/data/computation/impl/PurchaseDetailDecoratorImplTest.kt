package cz.cvut.fit.molinlen.android.debttracker.data.computation.impl

import cz.cvut.fit.molinlen.android.debttracker.data.model.CurrencyCode
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransactionSharePortion
import cz.cvut.fit.molinlen.android.debttracker.data.model.TransactionSplittingPortion
import cz.cvut.fit.molinlen.android.debttracker.data.model.createAmount
import cz.cvut.fit.molinlen.android.debttracker.test.util.TestModelTransactionCreator.createParticipant
import cz.cvut.fit.molinlen.android.debttracker.test.util.assertLeft
import cz.cvut.fit.molinlen.android.debttracker.test.util.assertRightAnd
import org.junit.Assert.assertEquals
import org.junit.Test

internal class PurchaseDetailDecoratorImplTest {

//    private val amountSplitter = AmountSplitterImpl
    private val impl = PurchaseDetailDecoratorImpl()

    @Test
    fun testFail() {
        val result = impl.decoratePortionSplitting(
            splittingPortion = TransactionSplittingPortion(sharers = emptyList()),
            totalAmount = createAmount(500.toBigDecimal(), CurrencyCode.CZK),
        )
        result.assertLeft()
    }

    @Test
    fun decoratePortionSimple() {
        val fullAmount = createAmount(500.toBigDecimal(), CurrencyCode.CZK)
        val share = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val undecorated = TransactionSplittingPortion(sharers = listOf(share))

        val result = impl.decoratePortionSplitting(splittingPortion = undecorated, totalAmount = fullAmount)

        result.assertRightAnd { decorated ->
            assertEquals(undecorated.sharers.firstOrNull()?.participant, decorated.sharers.firstOrNull()?.participant)
            assertEquals(undecorated.sharers.firstOrNull()?.shareValue, decorated.sharers.firstOrNull()?.shareValue)
            assertEquals(fullAmount.value, decorated.sharers.firstOrNull()?.shareValueComputed)
        }
    }

    @Test
    fun decoratePortionTwo() {
        val fullAmount = createAmount(20.1.toBigDecimal(), CurrencyCode.CZK)
        val share1 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val share2 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val undecorated = TransactionSplittingPortion(sharers = listOf(share1, share2))

        val result = impl.decoratePortionSplitting(splittingPortion = undecorated, totalAmount = fullAmount)

        result.assertRightAnd { decorated ->
            assertEquals(2, decorated.sharers.size)
            assertEquals(undecorated.sharers[0].participant, decorated.sharers[0].participant)
            assertEquals(undecorated.sharers[0].shareValue, decorated.sharers[0].shareValue)
            assertEquals(10.05.toBigDecimal(), decorated.sharers[0].shareValueComputed)
            assertEquals(undecorated.sharers[1].participant, decorated.sharers[1].participant)
            assertEquals(undecorated.sharers[1].shareValue, decorated.sharers[1].shareValue)
            assertEquals(10.05.toBigDecimal(), decorated.sharers[1].shareValueComputed)
        }
    }

    @Test
    fun decoratePortionRounded() {
        val fullAmount = createAmount(10.toBigDecimal(), CurrencyCode.CZK)
        val share1 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val share2 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val share3 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val undecorated = TransactionSplittingPortion(sharers = listOf(share1, share2, share3))

        val result = impl.decoratePortionSplitting(splittingPortion = undecorated, totalAmount = fullAmount)

        result.assertRightAnd { decorated ->
            assertEquals(3, decorated.sharers.size)
            assertEquals(undecorated.sharers[0].participant, decorated.sharers[0].participant)
            assertEquals(undecorated.sharers[0].shareValue, decorated.sharers[0].shareValue)
            assertEquals(3.33.toBigDecimal(), decorated.sharers[0].shareValueComputed)
            assertEquals(undecorated.sharers[1].participant, decorated.sharers[1].participant)
            assertEquals(undecorated.sharers[1].shareValue, decorated.sharers[1].shareValue)
            assertEquals(3.33.toBigDecimal(), decorated.sharers[1].shareValueComputed)
            assertEquals(undecorated.sharers[2].participant, decorated.sharers[2].participant)
            assertEquals(undecorated.sharers[2].shareValue, decorated.sharers[2].shareValue)
            assertEquals(3.33.toBigDecimal(), decorated.sharers[2].shareValueComputed)
        }
    }

    @Test
    fun decoratePortionStandard() {
        val fullAmount = createAmount(12.toBigDecimal(), CurrencyCode.CZK)
        val share1 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 1,
        )
        val share2 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 3,
        )
        val share3 = TransactionSharePortion(
            participant = createParticipant(),
            shareValue = 2,
        )
        val undecorated = TransactionSplittingPortion(sharers = listOf(share1, share2, share3))

        val result = impl.decoratePortionSplitting(splittingPortion = undecorated, totalAmount = fullAmount)

        result.assertRightAnd { decorated ->
            assertEquals(3, decorated.sharers.size)
            assertEquals(undecorated.sharers[0].participant, decorated.sharers[0].participant)
            assertEquals(undecorated.sharers[0].shareValue, decorated.sharers[0].shareValue)
            assertEquals("2.00".toBigDecimal(), decorated.sharers[0].shareValueComputed)
            assertEquals(undecorated.sharers[1].participant, decorated.sharers[1].participant)
            assertEquals(undecorated.sharers[1].shareValue, decorated.sharers[1].shareValue)
            assertEquals("6.00".toBigDecimal(), decorated.sharers[1].shareValueComputed)
            assertEquals(undecorated.sharers[2].participant, decorated.sharers[2].participant)
            assertEquals(undecorated.sharers[2].shareValue, decorated.sharers[2].shareValue)
            assertEquals("4.00".toBigDecimal(), decorated.sharers[2].shareValueComputed)
        }
    }
}
