# DebtTracker app

This repo contains the DebtTracker Android app: a tool to help users manage their groups' common finances.

This Kotlin app uses [Firebase services](https://firebase.google.com/) and [Jetpack Compose](https://developer.android.com/jetpack/compose).

As of now, the app is a prototype.



## Running the app locally

Compiling the app should not be a problem with a reasonably up-to-date version of [Android studio IDE](https://developer.android.com/studio).

Building and running the app requires an `./app/google-services.json` file with configuration for and already setup Firebase project. Without the configuration, the build will not complete. Can't access such file? For debugging or demonstration purposes, you can configure your own Firebase project.

For the release build variant, a signing config file is necessary.

### Configuring Firebase

1. Create a new project from [Firebase console](https://console.firebase.google.com).
2. In the overview of the created project, start adding an Android app: you should see the option next to something along the lines of "Get started by adding Firebase to your app", "Add an app to get started".
3. Register the app: package `cz.cvut.fit.molinlen.android.debttracker` (for app release build variant) or `cz.cvut.fit.molinlen.android.debttracker.debug` (for debug build variant). (As of now, the debug signing certificate does not need to be filled out.)
4. Download your `google-services.json` file.
5. Click through the rest of the Firebase console wizard to "Continue to console" button, which should take you to project overview.
6. Want to run both debug and release builds? In project overview, click "Add app" and repeat for the other variant (you will only use the `google-services.json` file from this step, the previous one can be deleted).
7. Put the `google-services.json` file into the `app` folder (make sure the file is named so).
8. The app should run now, but **that is not the end**. It will still not function properly.
9. In Firebase project overview, go to "Authentication" -> "Get started" -> "Sign-in providers: Native providers" and pick "Email/Password". Enable the option and save.
10. In Firebase project overview, go to "Cloud Firestore" -> "Create database" -> "Start in test mode" (if you're lazy and data isn't going to be sensitive, otherwise production mode), pick a location and click "Enable".
11. Go to Firestore console (e.g. click "Firestore Database" in "Project shortcuts") and its "Rules" tab: set something reasonable for your needs (current project setting should be [here](./misc/firestore_rules_v1.txt))

... and that is it, the app should run smoothly now.

### Release signing config

A release build demands a signing config file as referenced from [the app-level build.gradle file](./app/build.gradle) by the `signingConfigs` section.

Already have a config file to sign Android apps that you want to use? Feel free to change the settings in your local copy of the [build.gradle file](./app/build.gradle) to work with your signing file.

Don't have such a file and don't know how to create it? Google offers a [handy guide](https://developer.android.com/studio/publish/app-signing#sign-apk).

An alternative quick-and-dirty solution for local release build testing may be to change the release gradle setting from `signingConfig signingConfigs.release` to `signingConfig signingConfigs.debug`. This little hack is not considered a proper way to conduct signing and may stop working in the future.

If you change the signing configuration, before installing (the release version of) the app on any device that has it already installed with another signature, you will need to uninstall the previous version from that device.

(The author only worked with this project on a Linux machine, so if the signing file path ended up not being Windows-friendly, I am sorry, I tried, feel free to correct it.)




